# to compile resources do:
# pyrcc5 app/resources.qrc -o app/resources.py
# after that modify import in resources.py to pyqt6:
# from PyQt6 import QtCore
#
# to make executable do:
#
# pyinstaller --onefile time-wizard-level-editor.pyw
#
# the following line must be below
# import resources

import app.resources
import sys

from PyQt6.QtWidgets import QApplication
from app.mainWindow import MainWindow

sys._excepthook = sys.excepthook


def customExceptionHook(exceptionType, value, traceback):
    print(exceptionType, value, traceback)
    sys._excepthook(exceptionType, value, traceback)
    sys.exit(1)


if __name__ == "__main__":
    sys.excepthook = customExceptionHook
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()

    sys.exit(app.exec())
