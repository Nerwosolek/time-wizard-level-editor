from PyQt6.QtCore import Qt, pyqtSlot, QObject, pyqtSignal
from PyQt6.QtWidgets import QWidget, QGridLayout, QLabel, QSlider

from data.ranges import Ranges


class TimeControlPanel(QWidget):

    fps = 50
    timeChanged = pyqtSignal()

    def __init__(self):
        super().__init__()

        layout = QGridLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setAlignment(Qt.AlignmentFlag.AlignTop)

        self.levelTimeLabel = QLabel('0:00.00')
        self.levelTimeSlider = QSlider(Qt.Orientation.Horizontal)
        self.levelTimeSlider.setMaximum(self.secondsToFrames(Ranges.seconds.max // 2))
        self.levelTimeSlider.setTickPosition(QSlider.TickPosition.TicksBelow)
        self.levelTimeSlider.setTickInterval(self.secondsToFrames(5))
        self.levelTimeSlider.valueChanged.connect(self.levelTimeSliderValueChanged)

        self.globalTimeLabel = QLabel('0:00.00')
        self.globalTimeSlider = QSlider(Qt.Orientation.Horizontal)
        self.globalTimeSlider.setMaximum(self.secondsToFrames(Ranges.seconds.max // 2))
        self.globalTimeSlider.setTickPosition(QSlider.TickPosition.TicksBelow)
        self.globalTimeSlider.setTickInterval(self.secondsToFrames(5))
        self.globalTimeSlider.valueChanged.connect(self.globalTimeSliderValueChanged)

        layout.addWidget(self.levelTimeLabel, 0, 0, 1, 1)
        layout.addWidget(self.levelTimeSlider, 0, 1, 1, 1)
        layout.addWidget(self.globalTimeLabel, 1, 0, 1, 1)
        layout.addWidget(self.globalTimeSlider, 1, 1, 1, 1)

        self.setLayout(layout)

    def secondsToFrames(self, seconds: int):
        return seconds * self.fps

    def framesToTime(self, frames: int):
        part = (frames % self.fps) * 2
        tmp = frames // self.fps
        seconds = tmp % 60
        minutes = tmp // 60
        return minutes, seconds, part

    @pyqtSlot()
    def levelTimeSliderValueChanged(self):
        minutes, seconds, part = self.framesToTime(self.levelTimeSlider.value())
        self.levelTimeLabel.setText(f'{minutes}:{seconds:02d}.{part:02d}')
        self.timeChanged.emit()

    @pyqtSlot()
    def globalTimeSliderValueChanged(self):
        minutes, seconds, part = self.framesToTime(self.globalTimeSlider.value())
        self.globalTimeLabel.setText(f'{minutes}:{seconds:02d}.{part:02d}')
        self.timeChanged.emit()
