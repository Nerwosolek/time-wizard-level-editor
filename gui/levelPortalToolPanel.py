from abc import abstractmethod

from PyQt6.QtCore import Qt, pyqtSlot, QPoint, QSize
from PyQt6.QtGui import QUndoStack
from PyQt6.QtWidgets import QLabel, QGridLayout, QSpinBox

from commands.levelPortalCommands import LevelPortalInPosChangeCommand, LevelPortalOutPosChangeCommand, \
    LevelPortalInSizeChangeCommand, LevelPortalOutSizeChangeCommand
from data.language import Language
from data.selectionManager import SelectionManager
from gui.headerWidget import HeaderWidget, QHLine
from gui.iconProvider import IconProvider
from gui.toolPanelWidget import ToolPanelWidget
from data.levelInfo import LevelPortalInfo
from data.ranges import Ranges


class LevelPortalToolPanel(ToolPanelWidget):

    @abstractmethod
    def headerLabel(self):
        pass

    @abstractmethod
    def headerIcon(self):
        pass

    @abstractmethod
    def createColumnChangeCommand(self, value):
        pass

    @abstractmethod
    def createRowChangeCommand(self, value):
        pass

    @abstractmethod
    def createHeightChangeCommand(self, value):
        pass

    def __init__(self, data: LevelPortalInfo, history: QUndoStack, selectionManager: SelectionManager,
                 iconProvider: IconProvider, lang: Language):
        super().__init__(history, selectionManager, lang)
        self.data = data

        layout = QGridLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setAlignment(Qt.AlignmentFlag.AlignTop)
        self.header = HeaderWidget(iconProvider, None, self.headerIcon())

        layout.addWidget(self.header, 0, 0, 1, 2)
        layout.addWidget(QHLine(), 1, 0, 1, 2)

        self.columnLabel = QLabel(None)
        layout.addWidget(self.columnLabel, 2, 0, 1, 1)
        self.columnSpinBox = QSpinBox()
        self.columnSpinBox.setRange(Ranges.column.min, Ranges.column.max)
        self.columnSpinBox.valueChanged[int].connect(self.columnChanged)
        layout.addWidget(self.columnSpinBox, 2, 1, 1, 1)

        self.rowLabel = QLabel(None)
        layout.addWidget(self.rowLabel, 3, 0, 1, 1)
        self.rowSpinBox = QSpinBox()
        self.rowSpinBox.setRange(Ranges.row.min, Ranges.row.max)
        self.rowSpinBox.valueChanged[int].connect(self.rowChanged)
        layout.addWidget(self.rowSpinBox, 3, 1, 1, 1)

        self.heightLabel = QLabel(None)
        layout.addWidget(self.heightLabel, 4, 0, 1, 1)
        self.heightSpinBox = QSpinBox()
        self.heightSpinBox.setRange(Ranges.height.min, Ranges.height.max)
        self.heightSpinBox.valueChanged[int].connect(self.heightChanged)
        layout.addWidget(self.heightSpinBox, 4, 1, 1, 1)

        self.setLayout(layout)
        self.updateControls()

    def updateLanguage(self):
        self.header.setText(self.headerLabel())
        self.columnLabel.setText(self.lang.column())
        self.rowLabel.setText(self.lang.row())
        self.heightLabel.setText(self.lang.height())

    def reinitControls(self):
        self.updateControls()

    def internalUpdateControls(self):
        self.columnSpinBox.setValue(self.data.column)
        self.rowSpinBox.setValue(self.data.row)
        self.heightSpinBox.setValue(self.data.height)

    @pyqtSlot(int)
    def columnChanged(self, value):
        if self.updating:
            return

        if self.data.column != value:
            command = self.createColumnChangeCommand(value)
            self.history.push(command)

    @pyqtSlot(int)
    def rowChanged(self, value):
        if self.updating:
            return

        if self.data.row != value:
            command = self.createRowChangeCommand(value)
            self.history.push(command)

    @pyqtSlot(int)
    def heightChanged(self, value):
        if self.updating:
            return

        if self.data.height != value:
            command = self.createHeightChangeCommand(value)
            self.history.push(command)


class LevelPortalInToolPanel(LevelPortalToolPanel):
    def headerLabel(self):
        return self.lang.levelPortalIn()

    def headerIcon(self):
        return ':/icons/view-portal-in'

    def createColumnChangeCommand(self, value):
        return LevelPortalInPosChangeCommand(self.data, self.data.getPos(), QPoint(value, self.data.row), self.lang)

    def createRowChangeCommand(self, value):
        return LevelPortalInPosChangeCommand(self.data, self.data.getPos(), QPoint(self.data.column, value), self.lang)

    def createHeightChangeCommand(self, value):
        size = self.data.getSize()
        return LevelPortalInSizeChangeCommand(self.data, size, QSize(size.width(), value), self.lang)

    def showTab(self):
        self.selectionManager.setSelected(self.data)


class LevelPortalOutToolPanel(LevelPortalToolPanel):
    def headerLabel(self):
        return self.lang.levelPortalOut()

    def headerIcon(self):
        return ':/icons/view-portal-out'

    def createColumnChangeCommand(self, value):
        return LevelPortalOutPosChangeCommand(self.data, self.data.getPos(), QPoint(value, self.data.row), self.lang)

    def createRowChangeCommand(self, value):
        return LevelPortalOutPosChangeCommand(self.data, self.data.getPos(), QPoint(self.data.column, value), self.lang)

    def createHeightChangeCommand(self, value):
        size = self.data.getSize()
        return LevelPortalOutSizeChangeCommand(self.data, size, QSize(size.width(), value), self.lang)

    def showTab(self):
        self.selectionManager.setSelected(self.data)
