from abc import abstractmethod

from PyQt6.QtGui import QUndoStack
from PyQt6.QtWidgets import QWidget

from data.language import Language
from data.selectionManager import SelectionManager


class ToolPanelWidget(QWidget):

    def __init__(self, history: QUndoStack, selectionManager: SelectionManager, lang: Language):
        self.history = history
        self.selectionManager = selectionManager
        self.lang = lang
        self.updating = False
        super().__init__()

    @abstractmethod
    def updateLanguage(self):
        pass

    def updateControls(self):
        self.updating = True
        self.internalUpdateControls()
        self.updating = False

    @abstractmethod
    def showTab(self):
        pass

    @abstractmethod
    def reinitControls(self):
        pass

    @abstractmethod
    def internalUpdateControls(self):
        pass
