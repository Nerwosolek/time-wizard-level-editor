from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QDialog, QListWidget, QListWidgetItem, QVBoxLayout, QStyledItemDelegate

from data.language import Language
from data.levelValidator import LevelValidator
from data.selectionManager import SelectionManager


class ProblemNameDelegate(QStyledItemDelegate):

    def __init__(self, parent, lang: Language):
        super().__init__(parent)
        self.lang = lang

    def initStyleOption(self, option, index):
        super().initStyleOption(option, index)
        problem = option.widget.item(index.row()).data(Qt.ItemDataRole.UserRole)
        if problem.item is None:
            option.text = f'{index.row()}. {self.lang.problemKindEnum(problem.kind)}'
        else:
            option.text = (f'{index.row()}. {self.lang.problemKindEnum(problem.kind)} - '
                           f'{problem.item.getItemName(self.lang)}')


class LevelProblemsWindow(QDialog):

    def __init__(self, parent, validator: LevelValidator, selectionManager: SelectionManager, lang: Language):
        super().__init__(parent)

        self.validator = validator
        self.selectionManager = selectionManager
        self.lang = lang
        self.resize(600, 300)
        self.listView = QListWidget()
        self.listView.itemClicked.connect(self.selectionChanged)
        self.listView.currentRowChanged.connect(self.selectionChanged)
        layout = QVBoxLayout()
        layout.setAlignment(Qt.AlignmentFlag.AlignTop)
        layout.addWidget(self.listView)
        self.setLayout(layout)

        delegate = ProblemNameDelegate(self.listView, self.lang)
        self.listView.setItemDelegate(delegate)

    def updateLanguage(self):
        self.setWindowTitle(self.lang.problemList())

    def update(self):
        self.listView.clear()
        for problem in self.validator.problems:
            item = QListWidgetItem()
            item.setText('')
            item.setData(Qt.ItemDataRole.UserRole, problem)
            self.listView.addItem(item)

    def selectionChanged(self):
        selected = self.listView.selectedItems()
        if len(selected) == 0:
            return

        item = selected[0].data(Qt.ItemDataRole.UserRole).item
        self.selectionManager.setSelected(item)
