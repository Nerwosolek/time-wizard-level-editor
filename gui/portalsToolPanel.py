from PyQt6.QtCore import pyqtSlot, QPoint, QSize
from PyQt6.QtGui import QUndoStack
from PyQt6.QtWidgets import QLabel, QGridLayout, QSpinBox, QComboBox

from commands.portalsCommands import PortalKindChangedCommand, PortalPosChangedCommand, PortalSizeChangedCommand
from data.enums import PortalEntryKind
from data.language import Language
from data.selectionManager import SelectionManager
from gui.iconProvider import IconProvider
from gui.itemListToolPanel import ItemListToolPanel
from data.levelInfo import PortalInfo
from data.ranges import Ranges


class PortalsToolPanel(ItemListToolPanel):

    def __init__(self, data: list, history: QUndoStack, selectionManager: SelectionManager, iconProvider: IconProvider,
                 lang: Language):
        self.kindLabel = None
        self.kindComboBox = None
        self.columnLabel = None
        self.columnSpinBox = None
        self.rowLabel = None
        self.rowSpinBox = None
        self.heightLabel = None
        self.heightSpinBox = None
        super().__init__(data, history, selectionManager, iconProvider, lang)

    def headerLabel(self):
        return self.lang.portals()

    def headerIcon(self):
        return ':/icons/view-portals'

    def initControls(self, layout: QGridLayout):
        self.kindLabel = QLabel(None)
        layout.addWidget(self.kindLabel, 4, 0, 1, 1)
        self.kindComboBox = QComboBox()
        for x in PortalEntryKind:
            self.kindComboBox.addItem(x.name, x)
        self.kindComboBox.currentIndexChanged[int].connect(self.kindChanged)
        layout.addWidget(self.kindComboBox, 4, 1, 1, 1)

        self.columnLabel = QLabel(None)
        layout.addWidget(self.columnLabel, 5, 0, 1, 1)
        self.columnSpinBox = QSpinBox()
        self.columnSpinBox.setRange(Ranges.column.min, Ranges.column.max)
        self.columnSpinBox.valueChanged[int].connect(self.columnChanged)
        layout.addWidget(self.columnSpinBox, 5, 1, 1, 1)

        self.rowLabel = QLabel(None)
        layout.addWidget(self.rowLabel, 6, 0, 1, 1)
        self.rowSpinBox = QSpinBox()
        self.rowSpinBox.setRange(Ranges.row.min, Ranges.row.max)
        self.rowSpinBox.valueChanged[int].connect(self.rowChanged)
        layout.addWidget(self.rowSpinBox, 6, 1, 1, 1)

        self.heightLabel = QLabel('Height')
        layout.addWidget(self.heightLabel, 7, 0, 1, 1)
        self.heightSpinBox = QSpinBox()
        self.heightSpinBox.setRange(2, Ranges.height.max)
        self.heightSpinBox.valueChanged[int].connect(self.heightChanged)
        layout.addWidget(self.heightSpinBox, 7, 1, 1, 1)

    def internalUpdateLanguage(self):
        self.kindLabel.setText(self.lang.entry())
        self.columnLabel.setText(self.lang.column())
        self.rowLabel.setText(self.lang.row())
        self.heightLabel.setText(self.lang.height())
        for i in range(self.kindComboBox.count()):
            self.kindComboBox.setItemText(i, self.lang.portalEntryKindEnum(self.kindComboBox.itemData(i)))

    def internalUpdateControls(self):
        item = self.getCurrentItemData()
        if item is None:
            self.kindLabel.setEnabled(False)
            self.kindComboBox.setEnabled(False)
            self.columnLabel.setEnabled(False)
            self.columnSpinBox.setEnabled(False)
            self.rowLabel.setEnabled(False)
            self.rowSpinBox.setEnabled(False)
            self.heightLabel.setEnabled(False)
            self.heightSpinBox.setEnabled(False)
        else:
            self.kindLabel.setEnabled(True)
            self.kindComboBox.setEnabled(True)
            self.columnLabel.setEnabled(True)
            self.columnSpinBox.setEnabled(True)
            self.rowLabel.setEnabled(True)
            self.rowSpinBox.setEnabled(True)
            self.heightLabel.setEnabled(True)
            self.heightSpinBox.setEnabled(True)
            self.kindComboBox.setCurrentIndex(list(PortalEntryKind).index(item.kind))
            self.columnSpinBox.setValue(item.column)
            self.rowSpinBox.setValue(item.row)
            self.heightSpinBox.setValue(item.height)

    def createNewItem(self):
        return PortalInfo(
            PortalEntryKind(self.kindComboBox.itemData(self.kindComboBox.currentIndex())),
            self.columnSpinBox.value(),
            self.rowSpinBox.value(),
            self.heightSpinBox.value())

    @pyqtSlot(int)
    def kindChanged(self):
        item = self.getCurrItem()
        if item is None:
            return

        value = PortalEntryKind(self.kindComboBox.itemData(self.kindComboBox.currentIndex()))
        if item.kind != value:
            command = PortalKindChangedCommand(item, item.kind, value, self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def columnChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.column != value:
            command = PortalPosChangedCommand(item, item.getPos(), QPoint(value, item.row), self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def rowChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.row != value:
            command = PortalPosChangedCommand(item, item.getPos(), QPoint(item.column, value), self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def heightChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.height != value:
            size = item.getSize()
            command = PortalSizeChangedCommand(item, size, QSize(size.width(), value), self.lang)
            self.history.push(command)
