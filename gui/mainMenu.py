from PyQt6.QtWidgets import QMenuBar, QMainWindow

from app.actions import Actions
from data.language import Language


class MainMenu(QMenuBar):

    def __init__(self, owner: QMainWindow, actions: Actions):
        super().__init__()

        self.fileMenu = self.addMenu('')
        self.fileMenu.addAction(actions.newAction)
        self.fileMenu.addAction(actions.openAction)
        self.fileMenu.addAction(actions.saveAction)
        self.fileMenu.addAction(actions.saveAsAction)
        self.fileMenu.addAction(actions.importAction)
        self.fileMenu.addAction(actions.exportAction)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(actions.exitAction)

        self.gameMenu = self.addMenu('')
        self.gameMenu.addAction(actions.testAction)

        self.editMenu = self.addMenu('')
        self.editMenu.addAction(actions.undoAction)
        self.editMenu.addAction(actions.redoAction)
        self.editMenu.addSeparator()
        self.editMenu.addAction(actions.cutAction)
        self.editMenu.addAction(actions.copyAction)
        self.editMenu.addAction(actions.pasteAction)

        self.viewMenu = self.addMenu('')
        self.viewMenu.addAction(actions.zoomOutAction)
        self.viewMenu.addAction(actions.zoomInAction)
        self.viewMenu.addSeparator()
        self.viewMenu.addAction(actions.generalViewAction)
        self.viewMenu.addAction(actions.levelPortalInViewAction)
        self.viewMenu.addAction(actions.levelPortalOutViewAction)
        self.viewMenu.addAction(actions.boardItemsViewAction)
        self.viewMenu.addAction(actions.movingPlatformsViewAction)
        self.viewMenu.addAction(actions.enemiesViewAction)
        self.viewMenu.addAction(actions.lasersViewAction)
        self.viewMenu.addAction(actions.buttonsViewAction)
        self.viewMenu.addAction(actions.forceFieldsViewAction)
        self.viewMenu.addAction(actions.disappearingPlatformsViewAction)
        self.viewMenu.addAction(actions.hourglassesViewAction)
        self.viewMenu.addAction(actions.portalsViewAction)

        self.statesMenu = self.addMenu('')
        for action in actions.stateActions:
            self.statesMenu.addAction(action)

        self.languageMenu = self.addMenu('')
        self.languageMenu.addAction(actions.languageEnAction)
        self.languageMenu.addAction(actions.languagePlAction)
        self.languageMenu.addAction(actions.languageEsAction)

        self.helpMenu = self.addMenu('')
        self.helpMenu.addAction(actions.aboutAction)

        owner.setMenuBar(self)

    def updateLanguage(self, lang: Language):
        self.fileMenu.setTitle(lang.file())
        self.gameMenu.setTitle(lang.test())
        self.editMenu.setTitle(lang.edit())
        self.viewMenu.setTitle(lang.view())
        self.statesMenu.setTitle(lang.states())
        self.languageMenu.setTitle(lang.language())
        self.helpMenu.setTitle(lang.help())
