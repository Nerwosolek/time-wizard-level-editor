from PyQt6.QtCore import pyqtSlot, QPoint
from PyQt6.QtGui import QUndoStack
from PyQt6.QtWidgets import QLabel, QGridLayout, QSpinBox, QComboBox, QCheckBox

from commands.buttonsCommands import ButtonKindChangedCommand, ButtonStateChangedCommand, ButtonPosChangedCommand
from commands.commandFactory import CommandFactory
from data.enums import ButtonKind
from data.language import Language
from data.levelInfo import ButtonInfo
from data.ranges import Ranges
from data.selectionManager import SelectionManager
from gui.iconProvider import IconProvider
from gui.itemListToolPanel import ItemListToolPanel


class ButtonsToolPanel(ItemListToolPanel):

    def __init__(self, data: list, history: QUndoStack, selectionManager: SelectionManager,
                 commandFactory: CommandFactory, iconProvider: IconProvider, lang: Language):
        self.kindLabel = None
        self.kindComboBox = None
        self.timeResistLabel = None
        self.timeResistCheckBox = None
        self.columnLabel = None
        self.columnSpinBox = None
        self.rowLabel = None
        self.rowSpinBox = None
        self.stateLabel = None
        self.stateSpinBox = None
        self.commandFactory = commandFactory
        super().__init__(data, history, selectionManager, iconProvider, lang)

    def headerLabel(self):
        return self.lang.buttons()

    def headerIcon(self):
        return ':/icons/view-buttons'

    def initControls(self, layout: QGridLayout):
        self.kindLabel = QLabel(None)
        layout.addWidget(self.kindLabel, 4, 0, 1, 1)
        self.kindComboBox = QComboBox()
        for x in ButtonKind:
            self.kindComboBox.addItem(x.name, x)
        self.kindComboBox.currentIndexChanged[int].connect(self.kindChanged)
        layout.addWidget(self.kindComboBox, 4, 1, 1, 1)

        self.timeResistLabel = QLabel(None)
        layout.addWidget(self.timeResistLabel, 5, 0, 1, 1)
        self.timeResistCheckBox = QCheckBox('')
        self.timeResistCheckBox.stateChanged[int].connect(self.timeResistChanged)
        layout.addWidget(self.timeResistCheckBox, 5, 1, 1, 1)

        self.columnLabel = QLabel(None)
        layout.addWidget(self.columnLabel, 6, 0, 1, 1)
        self.columnSpinBox = QSpinBox()
        self.columnSpinBox.setRange(Ranges.column.min, Ranges.column.max)
        self.columnSpinBox.valueChanged[int].connect(self.columnChanged)
        layout.addWidget(self.columnSpinBox, 6, 1, 1, 1)

        self.rowLabel = QLabel(None)
        layout.addWidget(self.rowLabel, 7, 0, 1, 1)
        self.rowSpinBox = QSpinBox()
        self.rowSpinBox.setRange(Ranges.row.min, Ranges.row.max)
        self.rowSpinBox.valueChanged[int].connect(self.rowChanged)
        layout.addWidget(self.rowSpinBox, 7, 1, 1, 1)

        self.stateLabel = QLabel(None)
        layout.addWidget(self.stateLabel, 8, 0, 1, 1)
        self.stateSpinBox = QSpinBox()
        self.stateSpinBox.setRange(Ranges.state.min, Ranges.state.max)
        self.stateSpinBox.valueChanged[int].connect(self.stateChanged)
        layout.addWidget(self.stateSpinBox, 8, 1, 1, 1)

    def internalUpdateLanguage(self):
        self.kindLabel.setText(self.lang.kind())
        self.timeResistLabel.setText(self.lang.timeResist())
        self.columnLabel.setText(self.lang.column())
        self.rowLabel.setText(self.lang.row())
        self.stateLabel.setText(self.lang.state())
        for i in range(self.kindComboBox.count()):
            self.kindComboBox.setItemText(i, self.lang.buttonKindEnum(self.kindComboBox.itemData(i)))

    def internalUpdateControls(self):
        item = self.getCurrentItemData()
        if item is None:
            self.kindLabel.setEnabled(False)
            self.kindComboBox.setEnabled(False)
            self.timeResistLabel.setEnabled(False)
            self.timeResistCheckBox.setEnabled(False)
            self.columnLabel.setEnabled(False)
            self.columnSpinBox.setEnabled(False)
            self.rowLabel.setEnabled(False)
            self.rowSpinBox.setEnabled(False)
            self.stateLabel.setEnabled(False)
            self.stateSpinBox.setEnabled(False)
        else:
            self.kindLabel.setEnabled(True)
            self.kindComboBox.setEnabled(True)
            self.timeResistLabel.setEnabled(True)
            self.timeResistCheckBox.setEnabled(True)
            self.columnLabel.setEnabled(True)
            self.columnSpinBox.setEnabled(True)
            self.rowLabel.setEnabled(True)
            self.rowSpinBox.setEnabled(True)
            self.stateLabel.setEnabled(True)
            self.stateSpinBox.setEnabled(True)
            self.kindComboBox.setCurrentIndex(list(ButtonKind).index(item.kind))
            self.timeResistCheckBox.setChecked(item.timeResist)
            self.columnSpinBox.setValue(item.column)
            self.rowSpinBox.setValue(item.row)
            self.stateSpinBox.setValue(item.state)

    def createNewItem(self):
        return ButtonInfo(
            ButtonKind(self.kindComboBox.itemData(self.kindComboBox.currentIndex())),
            self.timeResistCheckBox.isChecked(),
            self.columnSpinBox.value(),
            self.rowSpinBox.value(),
            self.stateSpinBox.value())

    @pyqtSlot(int)
    def kindChanged(self):
        item = self.getCurrItem()
        if item is None:
            return

        value = ButtonKind(self.kindComboBox.itemData(self.kindComboBox.currentIndex()))
        if item.kind != value:
            command = ButtonKindChangedCommand(item, item.kind, value, self.lang)
            self.history.push(command)

    @pyqtSlot()
    def timeResistChanged(self):
        item = self.getCurrItem()
        if item is None:
            return

        value = self.timeResistCheckBox.isChecked()
        if item.kind != value:
            command = self.commandFactory.createGroupTimeResistChangedCommand(item.state, value)
            if not (command is None):
                self.history.push(command)

    @pyqtSlot(int)
    def columnChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.column != value:
            command = ButtonPosChangedCommand(item, item.getPos(), QPoint(value, item.row), self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def rowChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.row != value:
            command = ButtonPosChangedCommand(item, item.getPos(), QPoint(item.column, value), self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def stateChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.state != value:
            command = self.commandFactory.createGroupTimeResistChangedCommand(value, item.timeResist)
            if not (command is None):
                self.history.push(command)
            command = ButtonStateChangedCommand(item, item.state, value, self.lang)
            self.history.push(command)
