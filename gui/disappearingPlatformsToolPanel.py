from PyQt6.QtCore import pyqtSlot, QPoint, QSize
from PyQt6.QtGui import QUndoStack
from PyQt6.QtWidgets import QLabel, QGridLayout, QSpinBox, QComboBox, QCheckBox

from commands.commandFactory import CommandFactory
from commands.disappearingPlatformsCommands import DisappearingPlatformActivationMethodChangedCommand, \
    DisappearingPlatformInvertedChangedCommand, DisappearingPlatformTimeResistChangedCommand, \
    DisappearingPlatformStateChangedCommand, DisappearingPlatformMaskChangedCommand, \
    DisappearingPlatformPosChangedCommand, DisappearingPlatformSizeChangedCommand
from data.enums import ActivationMethodKind
from data.language import Language
from data.levelInfo import DisappearingPlatformInfo
from data.ranges import Ranges
from data.selectionManager import SelectionManager
from gui.iconProvider import IconProvider
from gui.itemListToolPanel import ItemListToolPanel, BinarySpinBox


class DisappearingPlatformsToolPanel(ItemListToolPanel):

    def __init__(self, data: list, history: QUndoStack, selectionManager: SelectionManager,
                 commandFactory: CommandFactory, iconProvider: IconProvider, lang: Language):
        self.methodLabel = None
        self.methodComboBox = None
        self.invertedLabel = None
        self.invertedCheckBox = None
        self.timeResistLabel = None
        self.timeResistCheckBox = None
        self.columnLabel = None
        self.columnSpinBox = None
        self.rowLabel = None
        self.rowSpinBox = None
        self.widthLabel = None
        self.widthSpinBox = None
        self.maskLabel = None
        self.maskSpinBox = None
        self.stateLabel = None
        self.stateSpinBox = None
        self.commandFactory = commandFactory
        super().__init__(data, history, selectionManager, iconProvider, lang)

    def headerLabel(self):
        return self.lang.disappearingPlatforms()

    def headerIcon(self):
        return ':/icons/view-cloud-platforms'

    def initControls(self, layout: QGridLayout):
        self.methodLabel = QLabel(None)
        layout.addWidget(self.methodLabel, 4, 0, 1, 1)
        self.methodComboBox = QComboBox()
        for x in ActivationMethodKind:
            self.methodComboBox.addItem(x.name, x)
        self.methodComboBox.currentIndexChanged[int].connect(self.methodChanged)
        layout.addWidget(self.methodComboBox, 4, 1, 1, 1)

        self.invertedLabel = QLabel(None)
        layout.addWidget(self.invertedLabel, 5, 0, 1, 1)
        self.invertedCheckBox = QCheckBox('')
        self.invertedCheckBox.stateChanged[int].connect(self.invertedChanged)
        layout.addWidget(self.invertedCheckBox, 5, 1, 1, 1)

        self.timeResistLabel = QLabel(None)
        layout.addWidget(self.timeResistLabel, 6, 0, 1, 1)
        self.timeResistCheckBox = QCheckBox('')
        self.timeResistCheckBox.stateChanged[int].connect(self.timeResistChanged)
        layout.addWidget(self.timeResistCheckBox, 6, 1, 1, 1)

        self.columnLabel = QLabel(None)
        layout.addWidget(self.columnLabel, 7, 0, 1, 1)
        self.columnSpinBox = QSpinBox()
        self.columnSpinBox.setRange(Ranges.column.min, Ranges.column.max)
        self.columnSpinBox.valueChanged[int].connect(self.columnChanged)
        layout.addWidget(self.columnSpinBox, 7, 1, 1, 1)

        self.rowLabel = QLabel(None)
        layout.addWidget(self.rowLabel, 8, 0, 1, 1)
        self.rowSpinBox = QSpinBox()
        self.rowSpinBox.setRange(Ranges.row.min, Ranges.row.max)
        self.rowSpinBox.valueChanged[int].connect(self.rowChanged)
        layout.addWidget(self.rowSpinBox, 8, 1, 1, 1)

        self.widthLabel = QLabel(None)
        layout.addWidget(self.widthLabel, 9, 0, 1, 1)
        self.widthSpinBox = QSpinBox()
        self.widthSpinBox.setRange(2, Ranges.width.max)
        self.widthSpinBox.valueChanged[int].connect(self.lengthChanged)
        layout.addWidget(self.widthSpinBox, 9, 1, 1, 1)

        self.maskLabel = QLabel(None)
        self.maskSpinBox = BinarySpinBox()
        self.maskSpinBox.setRange(Ranges.mask.min, Ranges.mask.max)
        self.maskSpinBox.setDisplayIntegerBase(2)
        self.maskSpinBox.valueChanged[int].connect(self.maskChanged)

        self.stateLabel = QLabel(None)
        self.stateSpinBox = QSpinBox()
        self.stateSpinBox.setRange(Ranges.state.min, Ranges.state.max)
        self.stateSpinBox.valueChanged[int].connect(self.stateChanged)

    def internalUpdateLanguage(self):
        self.methodLabel.setText(self.lang.activationMethod())
        self.invertedLabel.setText(self.lang.invertedActivation())
        self.timeResistLabel.setText(self.lang.timeResist())
        self.columnLabel.setText(self.lang.column())
        self.rowLabel.setText(self.lang.row())
        self.widthLabel.setText(self.lang.width())
        self.maskLabel.setText(self.lang.timePattern())
        self.stateLabel.setText(self.lang.state())
        for i in range(self.methodComboBox.count()):
            self.methodComboBox.setItemText(i, self.lang.activationMethodKindEnum(self.methodComboBox.itemData(i)))

    def internalUpdateControls(self):
        item = self.getCurrentItemData()
        if item is None:
            self.methodLabel.setEnabled(False)
            self.methodComboBox.setEnabled(False)
            self.invertedLabel.setEnabled(False)
            self.invertedCheckBox.setEnabled(False)
            self.timeResistLabel.setEnabled(False)
            self.timeResistCheckBox.setEnabled(False)
            self.columnLabel.setEnabled(False)
            self.columnSpinBox.setEnabled(False)
            self.rowLabel.setEnabled(False)
            self.rowSpinBox.setEnabled(False)
            self.widthLabel.setEnabled(False)
            self.widthSpinBox.setEnabled(False)
            self.maskLabel.setEnabled(False)
            self.maskSpinBox.setEnabled(False)
            self.stateLabel.setEnabled(False)
            self.stateSpinBox.setEnabled(False)
        else:
            self.methodLabel.setEnabled(True)
            self.methodComboBox.setEnabled(True)
            self.invertedLabel.setEnabled(True)
            self.invertedCheckBox.setEnabled(True)
            self.timeResistLabel.setEnabled(True)
            self.timeResistCheckBox.setEnabled(True)
            self.columnLabel.setEnabled(True)
            self.columnSpinBox.setEnabled(True)
            self.rowLabel.setEnabled(True)
            self.rowSpinBox.setEnabled(True)
            self.widthLabel.setEnabled(True)
            self.widthSpinBox.setEnabled(True)
            self.maskLabel.setEnabled(True)
            self.maskSpinBox.setEnabled(True)
            self.stateLabel.setEnabled(True)
            self.stateSpinBox.setEnabled(True)
            self.methodComboBox.setCurrentIndex(list(ActivationMethodKind).index(item.methodKind))
            self.invertedCheckBox.setChecked(item.invertedActivation)
            self.timeResistCheckBox.setChecked(item.timeResist)
            self.columnSpinBox.setValue(item.column)
            self.rowSpinBox.setValue(item.row)
            self.widthSpinBox.setValue(item.width)
            self.maskSpinBox.setValue(item.mask)
            self.stateSpinBox.setValue(item.state)

        self.layout.takeAt(self.layout.indexOf(self.maskLabel))
        self.layout.takeAt(self.layout.indexOf(self.maskSpinBox))
        self.layout.takeAt(self.layout.indexOf(self.stateLabel))
        self.layout.takeAt(self.layout.indexOf(self.stateSpinBox))
        if (ActivationMethodKind(self.methodComboBox.itemData(self.methodComboBox.currentIndex())) ==
                ActivationMethodKind.Time):
            self.layout.addWidget(self.maskLabel, 10, 0, 1, 1)
            self.layout.addWidget(self.maskSpinBox, 10, 1, 1, 1)
            self.maskLabel.show()
            self.maskSpinBox.show()
            self.stateLabel.hide()
            self.stateSpinBox.hide()
        else:
            self.layout.addWidget(self.stateLabel, 10, 0, 1, 1)
            self.layout.addWidget(self.stateSpinBox, 10, 1, 1, 1)
            self.maskLabel.hide()
            self.maskSpinBox.hide()
            self.stateLabel.show()
            self.stateSpinBox.show()

    def createNewItem(self):
        return DisappearingPlatformInfo(
            ActivationMethodKind(self.methodComboBox.itemData(self.methodComboBox.currentIndex())),
            self.invertedCheckBox.isChecked(),
            self.timeResistCheckBox.isChecked(),
            self.columnSpinBox.value(),
            self.rowSpinBox.value(),
            self.widthSpinBox.value(),
            self.maskSpinBox.value(),
            self.stateSpinBox.value())

    @pyqtSlot(int)
    def methodChanged(self):
        item = self.getCurrItem()
        if item is None:
            return

        value = ActivationMethodKind(self.methodComboBox.itemData(self.methodComboBox.currentIndex()))
        if item.methodKind != value:
            if value == ActivationMethodKind.State:
                command = self.commandFactory.createGroupTimeResistChangedCommand(item.state, item.timeResist)
                if not (command is None):
                    self.history.push(command)
            command = DisappearingPlatformActivationMethodChangedCommand(item, item.methodKind, value, self.lang)
            self.history.push(command)
            self.updateControls()

    @pyqtSlot()
    def invertedChanged(self):
        item = self.getCurrItem()
        if item is None:
            return

        value = self.invertedCheckBox.isChecked()
        if item.invertedActivation != value:
            command = DisappearingPlatformInvertedChangedCommand(item, item.invertedActivation, value, self.lang)
            self.history.push(command)

    @pyqtSlot()
    def timeResistChanged(self):
        item = self.getCurrItem()
        if item is None:
            return

        value = self.timeResistCheckBox.isChecked()
        if item.timeResist != value:
            if item.methodKind == ActivationMethodKind.Time:
                command = DisappearingPlatformTimeResistChangedCommand(item, item.timeResist, value, self.lang)
            else:
                command = self.commandFactory.createGroupTimeResistChangedCommand(item.state, value)
            if not (command is None):
                self.history.push(command)

    @pyqtSlot(int)
    def columnChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.column != value:
            command = DisappearingPlatformPosChangedCommand(item, item.getPos(), QPoint(value, item.row), self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def rowChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.row != value:
            command = DisappearingPlatformPosChangedCommand(item, item.getPos(), QPoint(item.column, value), self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def lengthChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.width != value:
            size = item.getSize()
            command = DisappearingPlatformSizeChangedCommand(item, size, QSize(value, size.height()), self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def maskChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.mask != value:
            command = DisappearingPlatformMaskChangedCommand(item, item.mask, value, self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def stateChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.state != value:
            command = self.commandFactory.createGroupTimeResistChangedCommand(value, item.timeResist)
            if not (command is None):
                self.history.push(command)
            command = DisappearingPlatformStateChangedCommand(item, item.state, value, self.lang)
            self.history.push(command)
