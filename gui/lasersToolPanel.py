from PyQt6.QtCore import pyqtSlot, QPoint, QSize
from PyQt6.QtGui import QUndoStack
from PyQt6.QtWidgets import QLabel, QGridLayout, QSpinBox, QComboBox, QCheckBox

from commands.lasersCommands import LaserKindChangedCommand, LaserTimeResistChangedCommand, \
    LaserMaskChangedCommand, LaserPosChangedCommand, LaserSizeChangedCommand
from data.enums import LaserKind
from data.language import Language
from data.selectionManager import SelectionManager
from gui.iconProvider import IconProvider
from gui.itemListToolPanel import ItemListToolPanel, BinarySpinBox
from data.levelInfo import LaserInfo
from data.ranges import Ranges


class LasersToolPanel(ItemListToolPanel):

    def __init__(self, data: list, history: QUndoStack, selectionManager: SelectionManager, iconProvider: IconProvider,
                 lang: Language):
        self.kindLabel = None
        self.kindComboBox = None
        self.timeResistLabel = None
        self.timeResistCheckBox = None
        self.columnLabel = None
        self.columnSpinBox = None
        self.widthLabel = None
        self.widthSpinBox = None
        self.rowLabel = None
        self.rowSpinBox = None
        self.maskLabel = None
        self.maskSpinBox = None
        super().__init__(data, history, selectionManager, iconProvider, lang)

    def headerLabel(self):
        return self.lang.lasers()

    def headerIcon(self):
        return ':/icons/view-lasers'

    def initControls(self, layout: QGridLayout):
        self.kindLabel = QLabel(None)
        layout.addWidget(self.kindLabel, 4, 0, 1, 1)
        self.kindComboBox = QComboBox()
        for x in LaserKind:
            self.kindComboBox.addItem(x.name, x)
        self.kindComboBox.currentIndexChanged[int].connect(self.kindChanged)
        layout.addWidget(self.kindComboBox, 4, 1, 1, 1)

        self.timeResistLabel = QLabel(None)
        layout.addWidget(self.timeResistLabel, 5, 0, 1, 1)
        self.timeResistCheckBox = QCheckBox('')
        self.timeResistCheckBox.stateChanged[int].connect(self.timeResistChanged)
        layout.addWidget(self.timeResistCheckBox, 5, 1, 1, 1)

        self.columnLabel = QLabel(None)
        layout.addWidget(self.columnLabel, 6, 0, 1, 1)
        self.columnSpinBox = QSpinBox()
        self.columnSpinBox.setRange(Ranges.column.min, Ranges.column.max)
        self.columnSpinBox.valueChanged[int].connect(self.columnChanged)
        layout.addWidget(self.columnSpinBox, 6, 1, 1, 1)

        self.rowLabel = QLabel(None)
        layout.addWidget(self.rowLabel, 7, 0, 1, 1)
        self.rowSpinBox = QSpinBox()
        self.rowSpinBox.setRange(Ranges.row.min, Ranges.row.max)
        self.rowSpinBox.valueChanged[int].connect(self.rowChanged)
        layout.addWidget(self.rowSpinBox, 7, 1, 1, 1)

        self.widthLabel = QLabel(None)
        layout.addWidget(self.widthLabel, 8, 0, 1, 1)
        self.widthSpinBox = QSpinBox()
        self.widthSpinBox.setRange(2, Ranges.width.max)
        self.widthSpinBox.valueChanged[int].connect(self.widthChanged)
        layout.addWidget(self.widthSpinBox, 8, 1, 1, 1)

        self.maskLabel = QLabel(None)
        layout.addWidget(self.maskLabel, 9, 0, 1, 1)
        self.maskSpinBox = BinarySpinBox()
        self.maskSpinBox.setRange(Ranges.mask.min, Ranges.mask.max)
        self.maskSpinBox.setDisplayIntegerBase(2)
        self.maskSpinBox.valueChanged[int].connect(self.maskChanged)
        layout.addWidget(self.maskSpinBox, 9, 1, 1, 1)

    def internalUpdateLanguage(self):
        self.kindLabel.setText(self.lang.kind())
        self.timeResistLabel.setText(self.lang.timeResist())
        self.columnLabel.setText(self.lang.column())
        self.rowLabel.setText(self.lang.row())
        self.widthLabel.setText(self.lang.width())
        self.maskLabel.setText(self.lang.timePattern())
        for i in range(self.kindComboBox.count()):
            self.kindComboBox.setItemText(i, self.lang.laserKindEnum(self.kindComboBox.itemData(i)))

    def internalUpdateControls(self):
        item = self.getCurrentItemData()
        if item is None:
            self.kindLabel.setEnabled(False)
            self.kindComboBox.setEnabled(False)
            self.timeResistLabel.setEnabled(False)
            self.timeResistCheckBox.setEnabled(False)
            self.columnLabel.setEnabled(False)
            self.columnSpinBox.setEnabled(False)
            self.rowLabel.setEnabled(False)
            self.rowSpinBox.setEnabled(False)
            self.widthLabel.setEnabled(False)
            self.widthSpinBox.setEnabled(False)
            self.maskLabel.setEnabled(False)
            self.maskSpinBox.setEnabled(False)
        else:
            self.kindLabel.setEnabled(True)
            self.kindComboBox.setEnabled(True)
            self.timeResistLabel.setEnabled(True)
            self.timeResistCheckBox.setEnabled(True)
            self.columnLabel.setEnabled(True)
            self.columnSpinBox.setEnabled(True)
            self.rowLabel.setEnabled(True)
            self.rowSpinBox.setEnabled(True)
            self.widthLabel.setEnabled(True)
            self.widthSpinBox.setEnabled(True)
            self.maskLabel.setEnabled(True)
            self.maskSpinBox.setEnabled(True)
            self.kindComboBox.setCurrentIndex(list(LaserKind).index(item.kind))
            self.timeResistCheckBox.setChecked(item.timeResist)
            self.columnSpinBox.setValue(item.column)
            self.rowSpinBox.setValue(item.row)
            self.widthSpinBox.setValue(item.width)
            self.maskSpinBox.setValue(item.mask)

    def createNewItem(self):
        return LaserInfo(
            LaserKind(self.kindComboBox.itemData(self.kindComboBox.currentIndex())),
            self.timeResistCheckBox.isChecked(),
            self.columnSpinBox.value(),
            self.rowSpinBox.value(),
            self.widthSpinBox.value(),
            self.maskSpinBox.value())

    @pyqtSlot(int)
    def kindChanged(self):
        item = self.getCurrItem()
        if item is None:
            return

        value = LaserKind(self.kindComboBox.itemData(self.kindComboBox.currentIndex()))
        if item.kind != value:
            command = LaserKindChangedCommand(item, item.kind, value, self.lang)
            self.history.push(command)

    @pyqtSlot()
    def timeResistChanged(self):
        item = self.getCurrItem()
        if item is None:
            return

        value = self.timeResistCheckBox.isChecked()
        if item.timeResist != value:
            command = LaserTimeResistChangedCommand(item, item.timeResist, value, self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def columnChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.column != value:
            command = LaserPosChangedCommand(item, item.getPos(), QPoint(value, item.row), self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def rowChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.row != value:
            command = LaserPosChangedCommand(item, item.getPos(), QPoint(item.column, value), self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def widthChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.width != value:
            size = item.getSize()
            command = LaserSizeChangedCommand(item, size, QSize(value, size.width()), self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def maskChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.mask != value:
            command = LaserMaskChangedCommand(item, item.mask, value, self.lang)
            self.history.push(command)
