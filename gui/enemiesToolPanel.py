from PyQt6.QtCore import pyqtSlot, QPoint
from PyQt6.QtGui import QUndoStack
from PyQt6.QtWidgets import QLabel, QGridLayout, QSpinBox, QComboBox, QCheckBox

from commands.enemiesCommands import EnemyKindChangedCommand, EnemyTimeResistChangedCommand, EnemyRangeChangedCommand, \
    EnemyMoveOffsetChangedCommand, EnemyPosChangedCommand
from data.enums import RangeKind, EnemyKind
from data.language import Language
from data.selectionManager import SelectionManager
from gui.iconProvider import IconProvider
from gui.itemListToolPanel import ItemListToolPanel
from data.levelInfo import EnemyInfo
from data.ranges import Ranges


class EnemiesToolPanel(ItemListToolPanel):

    def __init__(self, data: list, history: QUndoStack, selectionManager: SelectionManager, iconProvider: IconProvider,
                 lang: Language):
        self.kindLabel = None
        self.kindComboBox = None
        self.timeResistLabel = None
        self.timeResistCheckBox = None
        self.xPosLabel = None
        self.xPosSpinBox = None
        self.yPosLabel = None
        self.yPosSpinBox = None
        self.columnLabel = None
        self.columnSpinBox = None
        self.rowLabel = None
        self.rowSpinBox = None
        self.rangeLabel = None
        self.rangeComboBox = None
        self.offsetLabel = None
        self.offsetSpinBox = None
        super().__init__(data, history, selectionManager, iconProvider, lang)

    def headerLabel(self):
        return self.lang.enemies()

    def headerIcon(self):
        return ':/icons/view-enemies'

    def initControls(self, layout: QGridLayout):
        self.kindLabel = QLabel(None)
        layout.addWidget(self.kindLabel, 4, 0, 1, 1)
        self.kindComboBox = QComboBox()
        for x in EnemyKind:
            self.kindComboBox.addItem(x.name, x)
        self.kindComboBox.currentIndexChanged[int].connect(self.kindChanged)
        layout.addWidget(self.kindComboBox, 4, 1, 1, 1)

        self.timeResistLabel = QLabel(None)
        layout.addWidget(self.timeResistLabel, 5, 0, 1, 1)
        self.timeResistCheckBox = QCheckBox('')
        self.timeResistCheckBox.stateChanged[int].connect(self.timeResistChanged)
        layout.addWidget(self.timeResistCheckBox, 5, 1, 1, 1)

        self.xPosLabel = QLabel(None)
        layout.addWidget(self.xPosLabel, 6, 0, 1, 1)
        self.xPosSpinBox = QSpinBox()
        self.xPosSpinBox.setRange(Ranges.xPos.min, Ranges.xPos.max)
        self.xPosSpinBox.valueChanged[int].connect(self.xPosChanged)
        layout.addWidget(self.xPosSpinBox, 6, 1, 1, 1)

        self.yPosLabel = QLabel(None)
        layout.addWidget(self.yPosLabel, 7, 0, 1, 1)
        self.yPosSpinBox = QSpinBox()
        self.yPosSpinBox.setRange(Ranges.yPos.min, Ranges.yPos.max)
        self.yPosSpinBox.valueChanged[int].connect(self.yPosChanged)
        layout.addWidget(self.yPosSpinBox, 7, 1, 1, 1)

        self.rangeLabel = QLabel(None)
        layout.addWidget(self.rangeLabel, 8, 0, 1, 1)
        self.rangeComboBox = QComboBox()
        self.rangeComboBox.addItems([x for x in RangeKind])
        self.rangeComboBox.currentIndexChanged[int].connect(self.rangeChanged)
        layout.addWidget(self.rangeComboBox, 8, 1, 1, 1)

        self.offsetLabel = QLabel(None)
        layout.addWidget(self.offsetLabel, 9, 0, 1, 1)
        self.offsetSpinBox = QSpinBox()
        self.offsetSpinBox.setRange(Ranges.moveOffset.min, Ranges.moveOffset.max)
        self.offsetSpinBox.valueChanged[int].connect(self.offsetChanged)
        layout.addWidget(self.offsetSpinBox, 9, 1, 1, 1)

    def internalUpdateLanguage(self):
        self.kindLabel.setText(self.lang.kind())
        self.timeResistLabel.setText(self.lang.timeResist())
        self.xPosLabel.setText(self.lang.horizontalPosition())
        self.yPosLabel.setText(self.lang.verticalPosition())
        self.rangeLabel.setText(self.lang.range())
        self.offsetLabel.setText(self.lang.offset())
        for i in range(self.kindComboBox.count()):
            self.kindComboBox.setItemText(i, self.lang.enemyKindEnum(self.kindComboBox.itemData(i)))

    def internalUpdateControls(self):
        item = self.getCurrentItemData()
        if item is None:
            self.kindLabel.setEnabled(False)
            self.kindComboBox.setEnabled(False)
            self.timeResistLabel.setEnabled(False)
            self.timeResistCheckBox.setEnabled(False)
            self.xPosLabel.setEnabled(False)
            self.xPosSpinBox.setEnabled(False)
            self.yPosLabel.setEnabled(False)
            self.yPosSpinBox.setEnabled(False)
            self.rangeLabel.setEnabled(False)
            self.rangeComboBox.setEnabled(False)
            self.offsetLabel.setEnabled(False)
            self.offsetSpinBox.setEnabled(False)
        else:
            self.kindLabel.setEnabled(True)
            self.kindComboBox.setEnabled(True)
            self.timeResistLabel.setEnabled(True)
            self.timeResistCheckBox.setEnabled(True)
            self.xPosLabel.setEnabled(True)
            self.xPosSpinBox.setEnabled(True)
            self.yPosLabel.setEnabled(True)
            self.yPosSpinBox.setEnabled(True)
            self.rangeLabel.setEnabled(True)
            self.rangeComboBox.setEnabled(True)
            self.offsetLabel.setEnabled(True)
            self.offsetSpinBox.setEnabled(True)
            self.kindComboBox.setCurrentIndex(list(EnemyKind).index(item.kind))
            self.timeResistCheckBox.setChecked(item.timeResist)
            self.xPosSpinBox.setValue(item.xPos)
            self.rangeComboBox.setCurrentText(item.range)
            self.yPosSpinBox.setValue(item.yPos)
            self.offsetSpinBox.setValue(item.moveOffset)

    def createNewItem(self):
        return EnemyInfo(
            EnemyKind(self.kindComboBox.itemData(self.kindComboBox.currentIndex())),
            self.timeResistCheckBox.isChecked(),
            self.xPosSpinBox.value(),
            self.yPosSpinBox.value(),
            RangeKind(self.rangeComboBox.currentText()),
            self.offsetSpinBox.value())

    @pyqtSlot(int)
    def kindChanged(self):
        item = self.getCurrItem()
        if item is None:
            return

        value = EnemyKind(self.kindComboBox.itemData(self.kindComboBox.currentIndex()))
        if item.kind != value:
            command = EnemyKindChangedCommand(item, item.kind, value, self.lang)
            self.history.push(command)

    @pyqtSlot()
    def timeResistChanged(self):
        item = self.getCurrItem()
        if item is None:
            return

        value = self.timeResistCheckBox.isChecked()
        if item.timeResist != value:
            command = EnemyTimeResistChangedCommand(item, item.timeResist, value, self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def xPosChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.xPos != value:
            command = EnemyPosChangedCommand(item, item.getPos(), QPoint(value, item.yPos), self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def yPosChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.yPos != value:
            command = EnemyPosChangedCommand(item, item.getPos(), QPoint(item.xPos, value), self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def rangeChanged(self):
        item = self.getCurrItem()
        if item is None:
            return

        value = RangeKind(self.rangeComboBox.currentText())
        if item.range != value:
            command = EnemyRangeChangedCommand(item, item.range, value, self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def offsetChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.moveOffset != value:
            command = EnemyMoveOffsetChangedCommand(item, item.moveOffset, value, self.lang)
            self.history.push(command)
