from PyQt6.QtCore import pyqtSlot, QPoint
from PyQt6.QtGui import QUndoStack
from PyQt6.QtWidgets import QLabel, QGridLayout, QSpinBox, QCheckBox

from commands.hourglassesCommands import HourglassTimeResistChangedCommand, HourglassPosChangedCommand
from data.language import Language
from data.selectionManager import SelectionManager
from gui.iconProvider import IconProvider
from gui.itemListToolPanel import ItemListToolPanel
from data.levelInfo import HourglassInfo
from data.ranges import Ranges


class HourglassesToolPanel(ItemListToolPanel):

    def __init__(self, data: list, history: QUndoStack, selectionManager: SelectionManager, iconProvider: IconProvider,
                 lang: Language):
        self.timeResistLabel = None
        self.timeResistCheckBox = None
        self.columnLabel = None
        self.columnSpinBox = None
        self.rowLabel = None
        self.rowSpinBox = None
        super().__init__(data, history, selectionManager, iconProvider, lang)

    def headerLabel(self):
        return self.lang.hourglasses()

    def headerIcon(self):
        return ':/icons/view-hourglasses'

    def initControls(self, layout: QGridLayout):
        self.timeResistLabel = QLabel(None)
        layout.addWidget(self.timeResistLabel, 4, 0, 1, 1)
        self.timeResistCheckBox = QCheckBox('')
        self.timeResistCheckBox.stateChanged[int].connect(self.timeResistChanged)
        layout.addWidget(self.timeResistCheckBox, 4, 1, 1, 1)

        self.columnLabel = QLabel(None)
        layout.addWidget(self.columnLabel, 5, 0, 1, 1)
        self.columnSpinBox = QSpinBox()
        self.columnSpinBox.setRange(Ranges.column.min, Ranges.column.max)
        self.columnSpinBox.valueChanged[int].connect(self.columnChanged)
        layout.addWidget(self.columnSpinBox, 5, 1, 1, 1)

        self.rowLabel = QLabel(None)
        layout.addWidget(self.rowLabel, 6, 0, 1, 1)
        self.rowSpinBox = QSpinBox()
        self.rowSpinBox.setRange(Ranges.row.min, Ranges.row.max)
        self.rowSpinBox.valueChanged[int].connect(self.rowChanged)
        layout.addWidget(self.rowSpinBox, 6, 1, 1, 1)

    def internalUpdateLanguage(self):
        self.timeResistLabel.setText(self.lang.timeResist())
        self.columnLabel.setText(self.lang.column())
        self.rowLabel.setText(self.lang.row())

    def internalUpdateControls(self):
        item = self.getCurrentItemData()
        if item is None:
            self.timeResistLabel.setEnabled(False)
            self.timeResistCheckBox.setEnabled(False)
            self.columnLabel.setEnabled(False)
            self.columnSpinBox.setEnabled(False)
            self.rowLabel.setEnabled(False)
            self.rowSpinBox.setEnabled(False)
        else:
            self.timeResistLabel.setEnabled(True)
            self.timeResistCheckBox.setEnabled(True)
            self.columnLabel.setEnabled(True)
            self.columnSpinBox.setEnabled(True)
            self.rowLabel.setEnabled(True)
            self.rowSpinBox.setEnabled(True)
            self.timeResistCheckBox.setChecked(item.timeResist)
            self.columnSpinBox.setValue(item.column)
            self.rowSpinBox.setValue(item.row)

    def createNewItem(self):
        return HourglassInfo(
            self.timeResistCheckBox.isChecked(),
            self.columnSpinBox.value(),
            self.rowSpinBox.value())

    @pyqtSlot()
    def timeResistChanged(self):
        item = self.getCurrItem()
        if item is None:
            return

        value = self.timeResistCheckBox.isChecked()
        if item.timeResist != value:
            command = HourglassTimeResistChangedCommand(item, item.timeResist, value, self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def columnChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.column != value:
            command = HourglassPosChangedCommand(item, item.getPos(), QPoint(value, item.row), self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def rowChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.row != value:
            command = HourglassPosChangedCommand(item, item.getPos(), QPoint(item.column, value), self.lang)
            self.history.push(command)
