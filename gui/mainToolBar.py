from PyQt6.QtCore import QSize
from PyQt6.QtWidgets import QMainWindow, QToolBar

from app.actions import Actions


class MainToolBar:

    def __init__(self, owner: QMainWindow, actions: Actions):
        iconSize = QSize(24, 24)

        fileToolBar = owner.addToolBar('File')
        fileToolBar.setMovable(False)
        fileToolBar.addAction(actions.newAction)
        fileToolBar.addAction(actions.openAction)
        fileToolBar.addAction(actions.saveAction)
        fileToolBar.addAction(actions.saveAsAction)
        fileToolBar.addAction(actions.importAction)
        fileToolBar.addAction(actions.exportAction)
        fileToolBar.setIconSize(iconSize)

        gameToolBar = QToolBar('Game', owner)
        gameToolBar.setMovable(False)
        gameToolBar.addAction(actions.testAction)
        gameToolBar.setIconSize(iconSize)
        owner.addToolBar(gameToolBar)

        editToolBar = QToolBar('Edit', owner)
        editToolBar.setMovable(False)
        editToolBar.addAction(actions.undoAction)
        editToolBar.addAction(actions.redoAction)
        editToolBar.setIconSize(iconSize)
        owner.addToolBar(editToolBar)

        editToolBar2 = QToolBar('Edit2', owner)
        editToolBar2.setMovable(False)
        editToolBar2.addAction(actions.cutAction)
        editToolBar2.addAction(actions.copyAction)
        editToolBar2.addAction(actions.pasteAction)
        editToolBar2.setIconSize(iconSize)
        owner.addToolBar(editToolBar2)

        zoomToolBar = QToolBar('Zoom', owner)
        zoomToolBar.setMovable(False)
        owner.addToolBar(zoomToolBar)
        zoomToolBar.addAction(actions.zoomOutAction)
        zoomToolBar.addAction(actions.zoomInAction)
        zoomToolBar.setIconSize(iconSize)

        viewToolBar = QToolBar('View', owner)
        viewToolBar.setMovable(False)
        owner.addToolBar(viewToolBar)
        viewToolBar.addAction(actions.generalViewAction)
        viewToolBar.addAction(actions.levelPortalInViewAction)
        viewToolBar.addAction(actions.levelPortalOutViewAction)
        viewToolBar.addAction(actions.boardItemsViewAction)
        viewToolBar.addAction(actions.movingPlatformsViewAction)
        viewToolBar.addAction(actions.enemiesViewAction)
        viewToolBar.addAction(actions.lasersViewAction)
        viewToolBar.addAction(actions.buttonsViewAction)
        viewToolBar.addAction(actions.forceFieldsViewAction)
        viewToolBar.addAction(actions.disappearingPlatformsViewAction)
        viewToolBar.addAction(actions.hourglassesViewAction)
        viewToolBar.addAction(actions.portalsViewAction)
        viewToolBar.setIconSize(iconSize)

        statesToolBar = QToolBar('States', owner)
        statesToolBar.setMovable(False)
        owner.addToolBar(statesToolBar)
        statesToolBar.setIconSize(iconSize)
        for action in actions.stateActions:
            statesToolBar.addAction(action)
