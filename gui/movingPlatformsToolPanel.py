from PyQt6.QtCore import pyqtSlot, QPoint, QSize
from PyQt6.QtGui import QUndoStack
from PyQt6.QtWidgets import QLabel, QGridLayout, QSpinBox, QComboBox, QCheckBox

from commands.movingPlatformsCommands import MovingPlatformTimeResistChangedCommand, \
    MovingPlatformRangeChangedCommand, MovingPlatformMoveOffsetChangedCommand, MovingPlatformPosChangedCommand, \
    MovingPlatformSizeChangedCommand
from data.enums import RangeKind
from data.language import Language
from data.selectionManager import SelectionManager
from gui.iconProvider import IconProvider
from gui.itemListToolPanel import ItemListToolPanel
from data.levelInfo import MovingPlatformInfo
from data.ranges import Ranges


class MovingPlatformsToolPanel(ItemListToolPanel):

    def __init__(self, data: list, history: QUndoStack, selectionManager: SelectionManager, iconProvider: IconProvider,
                 lang: Language):
        self.timeResistLabel = None
        self.timeResistCheckBox = None
        self.xPosLabel = None
        self.xPosSpinBox = None
        self.rowLabel = None
        self.rowSpinBox = None
        self.widthLabel = None
        self.widthSpinBox = None
        self.rangeLabel = None
        self.rangeComboBox = None
        self.offsetLabel = None
        self.offsetSpinBox = None
        super().__init__(data, history, selectionManager, iconProvider, lang)

    def headerLabel(self):
        return self.lang.movingPlatforms()

    def headerIcon(self):
        return ':/icons/view-moving-platforms'

    def initControls(self, layout: QGridLayout):
        self.timeResistLabel = QLabel(None)
        layout.addWidget(self.timeResistLabel, 4, 0, 1, 1)
        self.timeResistCheckBox = QCheckBox('')
        self.timeResistCheckBox.stateChanged[int].connect(self.timeResistChanged)
        layout.addWidget(self.timeResistCheckBox, 4, 1, 1, 1)

        self.xPosLabel = QLabel(None)
        layout.addWidget(self.xPosLabel, 5, 0, 1, 1)
        self.xPosSpinBox = QSpinBox()
        self.xPosSpinBox.setRange(Ranges.xPos.min, Ranges.xPos.max)
        self.xPosSpinBox.valueChanged[int].connect(self.xPosChanged)
        layout.addWidget(self.xPosSpinBox, 5, 1, 1, 1)

        self.rowLabel = QLabel()
        layout.addWidget(self.rowLabel, 6, 0, 1, 1)
        self.rowSpinBox = QSpinBox()
        self.rowSpinBox.setRange(Ranges.row.min, Ranges.row.max)
        self.rowSpinBox.valueChanged[int].connect(self.rowChanged)
        layout.addWidget(self.rowSpinBox, 6, 1, 1, 1)

        self.widthLabel = QLabel(None)
        layout.addWidget(self.widthLabel, 7, 0, 1, 1)
        self.widthSpinBox = QSpinBox()
        self.widthSpinBox.setRange(2, Ranges.width.max)
        self.widthSpinBox.valueChanged[int].connect(self.widthChanged)
        layout.addWidget(self.widthSpinBox, 7, 1, 1, 1)

        self.rangeLabel = QLabel(None)
        layout.addWidget(self.rangeLabel, 8, 0, 1, 1)
        self.rangeComboBox = QComboBox()
        self.rangeComboBox.addItems([x for x in RangeKind])
        self.rangeComboBox.currentIndexChanged[int].connect(self.rangeChanged)
        layout.addWidget(self.rangeComboBox, 8, 1, 1, 1)

        self.offsetLabel = QLabel(None)
        layout.addWidget(self.offsetLabel, 9, 0, 1, 1)
        self.offsetSpinBox = QSpinBox()
        self.offsetSpinBox.setRange(Ranges.moveOffset.min, Ranges.moveOffset.max)
        self.offsetSpinBox.valueChanged[int].connect(self.offsetChanged)
        layout.addWidget(self.offsetSpinBox, 9, 1, 1, 1)

    def internalUpdateLanguage(self):
        self.timeResistLabel.setText(self.lang.timeResist())
        self.xPosLabel.setText(self.lang.horizontalPosition())
        self.rowLabel.setText(self.lang.row())
        self.widthLabel.setText(self.lang.width())
        self.rangeLabel.setText(self.lang.range())
        self.offsetLabel.setText(self.lang.offset())

    def internalUpdateControls(self):
        item = self.getCurrentItemData()
        if item is None:
            self.timeResistLabel.setEnabled(False)
            self.timeResistCheckBox.setEnabled(False)
            self.xPosLabel.setEnabled(False)
            self.xPosSpinBox.setEnabled(False)
            self.rowLabel.setEnabled(False)
            self.rowSpinBox.setEnabled(False)
            self.widthLabel.setEnabled(False)
            self.widthSpinBox.setEnabled(False)
            self.rangeLabel.setEnabled(False)
            self.rangeComboBox.setEnabled(False)
            self.offsetLabel.setEnabled(False)
            self.offsetSpinBox.setEnabled(False)
        else:
            self.timeResistLabel.setEnabled(True)
            self.timeResistCheckBox.setEnabled(True)
            self.xPosLabel.setEnabled(True)
            self.xPosSpinBox.setEnabled(True)
            self.rowLabel.setEnabled(True)
            self.rowSpinBox.setEnabled(True)
            self.widthLabel.setEnabled(True)
            self.widthSpinBox.setEnabled(True)
            self.rangeLabel.setEnabled(True)
            self.rangeComboBox.setEnabled(True)
            self.offsetLabel.setEnabled(True)
            self.offsetSpinBox.setEnabled(True)
            self.timeResistCheckBox.setChecked(item.timeResist)
            self.xPosSpinBox.setValue(item.xPos)
            self.rowSpinBox.setValue(item.row)
            self.widthSpinBox.setValue(item.width)
            self.rangeComboBox.setCurrentText(item.range)
            self.offsetSpinBox.setValue(item.moveOffset)

    def createNewItem(self):
        return MovingPlatformInfo(
            self.timeResistCheckBox.isChecked(),
            self.xPosSpinBox.value(),
            self.rowSpinBox.value(),
            self.widthSpinBox.value(),
            RangeKind(self.rangeComboBox.currentText()),
            self.offsetSpinBox.value())

    @pyqtSlot()
    def timeResistChanged(self):
        item = self.getCurrItem()
        if item is None:
            return

        value = self.timeResistCheckBox.isChecked()
        if item.timeResist != value:
            command = MovingPlatformTimeResistChangedCommand(item, item.timeResist, value, self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def xPosChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.xPos != value:
            command = MovingPlatformPosChangedCommand(item, item.getPos(), QPoint(value, item.row), self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def rowChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.row != value:
            command = MovingPlatformPosChangedCommand(item, item.getPos(), QPoint(item.xPos, value), self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def widthChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.width != value:
            size = item.getSize()
            command = MovingPlatformSizeChangedCommand(item, size, QSize(value, size.height()), self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def rangeChanged(self):
        item = self.getCurrItem()
        if item is None:
            return

        value = RangeKind(self.rangeComboBox.currentText())
        if item.range != value:
            command = MovingPlatformRangeChangedCommand(item, item.range, value, self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def offsetChanged(self, value):
        item = self.getCurrItem()
        if item is None:
            return

        if item.moveOffset != value:
            command = MovingPlatformMoveOffsetChangedCommand(item, item.moveOffset, value, self.lang)
            self.history.push(command)
