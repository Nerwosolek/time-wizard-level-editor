from PyQt6.QtGui import QAction
from PyQt6.QtWidgets import QMenu

from app.actions import Actions
from data.language import Language


class BoardContextMenu(QMenu):

    def __init__(self, parent, actions: Actions, lang: Language):
        super().__init__(parent)

        self.lang = lang

        self.areaMenu = self.addMenu('')
        self.areaMenu.addAction(actions.addVoidAction)
        self.areaMenu.addAction(actions.addAirAction)

        self.wallMenu = self.addMenu('')
        self.wallMenu.addAction(actions.addWall1x1Action)
        self.wallMenu.addAction(actions.addBottomWall1x2Action)
        self.wallMenu.addAction(actions.addTopWall1x2Action)
        self.wallMenu.addAction(actions.addHorizontalWallAction)
        self.wallMenu.addAction(actions.addVerticalWallAction)
        self.wallMenu.addAction(actions.addBottomWall)
        self.wallMenu.addAction(actions.addTopWall)
        self.wallMenu.addAction(actions.addLeftWall)
        self.wallMenu.addAction(actions.addRightWall)
        self.wallMenu.addAction(actions.addLeftTopWall)
        self.wallMenu.addAction(actions.addRightTopWall)
        self.wallMenu.addAction(actions.addTopWallWithCorners)

        self.platformMenu = self.addMenu('')
        self.platformMenu.addAction(actions.addLightPlatformAction)
        self.platformMenu.addAction(actions.addPlatformAction)

        self.addAction(actions.addLadderAction)

        self.spikesMenu = self.addMenu('')
        self.spikesMenu.addAction(actions.addBottomSpikesAction)
        self.spikesMenu.addAction(actions.addTopSpikesAction)
        self.spikesMenu.addAction(actions.addLeftSpikesAction)
        self.spikesMenu.addAction(actions.addRightSpikesAction)

        self.decorationMenu = self.addMenu('')
        self.decorationMenu.addAction(actions.addStructure)
        self.decorationMenu.addAction(actions.addNarrowStructure)
        self.decorationMenu.addAction(actions.addChain)
        self.decorationMenu.addAction(actions.addTile)
        self.decorationMenu.addAction(actions.addPattern4x4_1)
        self.decorationMenu.addAction(actions.addPattern4x4_2)
        self.decorationMenu.addAction(actions.addPattern4x4_3)
        self.decorationMenu.addAction(actions.addPattern3x3_1)
        self.decorationMenu.addAction(actions.addPattern3x3_2)
        self.decorationMenu.addAction(actions.addPattern3x3_3)

        separator = QAction(self)
        separator.setSeparator(True)
        self.addAction(separator)

        self.addAction(actions.addMovingPlatformAction)

        self.enemyMenu = self.addMenu('')
        self.enemyMenu.addAction(actions.addHorizontalEnemy)
        self.enemyMenu.addAction(actions.addVerticalEnemy)

        self.laserMenu = self.addMenu('')
        self.laserMenu.addAction(actions.addLeftToRightLaser)
        self.laserMenu.addAction(actions.addRightToLeftLaser)

        self.buttonMenu = self.addMenu('')
        self.buttonMenu.addAction(actions.addTurnsOnButton)
        self.buttonMenu.addAction(actions.addTurnsOffButton)

        self.forceFieldMenu = self.addMenu('')
        self.forceFieldMenu.addAction(actions.addTimeForceField)
        self.forceFieldMenu.addAction(actions.addStateForceField)

        self.disappearingPlatformsMenu = self.addMenu('')
        self.disappearingPlatformsMenu.addAction(actions.addTimeDisappearingPlatform)
        self.disappearingPlatformsMenu.addAction(actions.addStateDisappearingPlatform)

        self.addAction(actions.addHourglassAction)

        self.portalsMenu = self.addMenu('')
        self.portalsMenu.addAction(actions.addLeftSidePortal)
        self.portalsMenu.addAction(actions.addRightSidePortal)
        self.portalsMenu.addAction(actions.addBothSidesPortal)

    def updateLanguage(self):
        self.areaMenu.setTitle(self.lang.addArea())
        self.wallMenu.setTitle(self.lang.addWall())
        self.platformMenu.setTitle(self.lang.addPlatform())
        self.spikesMenu.setTitle(self.lang.addSpikes())
        self.decorationMenu.setTitle(self.lang.addDecoration())
        self.enemyMenu.setTitle(self.lang.addEnemy())
        self.laserMenu.setTitle(self.lang.addLaser())
        self.buttonMenu.setTitle(self.lang.addButton())
        self.forceFieldMenu.setTitle(self.lang.addForceField())
        self.disappearingPlatformsMenu.setTitle(self.lang.addDisappearingPlatform())
        self.portalsMenu.setTitle(self.lang.addPortal())
