from PyQt6 import QtCore
from PyQt6.QtCore import Qt, QPoint, QSize
from PyQt6.QtGui import QCursor, QKeyEvent
from PyQt6.QtWidgets import QScrollArea, QLabel

from app.actions import Actions
from app.history import History
from commands.commandFactory import CommandFactory
from commands.boardItemsCommands import BoardItemPosChangedCommand, BoardItemSizeChangedCommand
from commands.buttonsCommands import ButtonPosChangedCommand
from commands.disappearingPlatformsCommands import DisappearingPlatformPosChangedCommand, \
    DisappearingPlatformSizeChangedCommand
from commands.enemiesCommands import EnemyPosChangedCommand
from commands.forceFieldsCommands import ForceFieldPosChangedCommand, ForceFieldSizeChangedCommand
from commands.hourglassesCommands import HourglassPosChangedCommand
from commands.lasersCommands import LaserPosChangedCommand, LaserSizeChangedCommand
from commands.levelPortalCommands import LevelPortalInPosChangeCommand, LevelPortalOutPosChangeCommand, \
    LevelPortalInSizeChangeCommand, LevelPortalOutSizeChangeCommand
from commands.movingPlatformsCommands import MovingPlatformPosChangedCommand, MovingPlatformSizeChangedCommand
from commands.portalsCommands import PortalPosChangedCommand, PortalSizeChangedCommand
from data.enums import BoardItemKind, RangeKind, EnemyKind, LaserKind, ButtonKind, ActivationMethodKind, \
    PortalEntryKind, ResizeObjectMode
from data.language import Language
from data.levelInfo import LevelInfo, LevelPortalInInfo, PixelInfo, LevelPortalOutInfo, BoardItemInfo, \
    MovingPlatformInfo, EnemyInfo, LaserInfo, ButtonInfo, ForceFieldInfo, DisappearingPlatformInfo, HourglassInfo, \
    PortalInfo
from data.selectionManager import SelectionManager
from gui.boardContextMenu import BoardContextMenu
from image.fontProvider import ZoomKind
from image.imageGenerator import ImageGenerator


class LevelPanel(QScrollArea):

    def __init__(self, actions: Actions, levelInfo: LevelInfo, history: History, selectionManager: SelectionManager,
                 commandFactory: CommandFactory, lang: Language):
        super().__init__()
        self.actions = actions
        self.levelInfo = levelInfo
        self.history = history
        self.selectionManager = selectionManager
        self.commandFactory = commandFactory
        self.lang = lang
        self.hoverItem = None
        self.hoverObjectResizeMode = ResizeObjectMode.NoResize
        self.zoom = None
        self.imageGenerator = None
        self.startMousePos = None
        self.startObjectPos = None
        self.startObjectSize = None
        self.imageGenerators = {
            ZoomKind.Zoom2: ImageGenerator(levelInfo, selectionManager, ZoomKind.Zoom2),
            ZoomKind.Zoom3: ImageGenerator(levelInfo, selectionManager, ZoomKind.Zoom3),
            ZoomKind.Zoom4: ImageGenerator(levelInfo, selectionManager, ZoomKind.Zoom4)
        }

        self.board = QLabel()
        self.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAsNeeded)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAsNeeded)
        self.setWidgetResizable(False)
        self.setWidget(self.board)
        self.setZoom(ZoomKind.Zoom3)
        self.setMouseTracking(True)

        self.menu = BoardContextMenu(self.board, self.actions, self.lang)
        self.contextMenuPos = None
        self.connectActions()

        self.board.setFocusPolicy(Qt.FocusPolicy.StrongFocus)
        self.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.board.setMouseTracking(True)
        self.board.mousePressEvent = self.boardMousePressEvent
        self.board.mouseReleaseEvent = self.boardMouseReleaseEvent
        self.board.mouseMoveEvent = self.boardMouseMoveEvent
        self.board.leaveEvent = self.boardLeaveEvent
        self.board.contextMenuEvent = self.boardContextMenuEvent
        self.board.keyPressEvent = self.boardKeyPressEvent

    def connectActions(self):
        self.actions.addVoidAction.triggered.connect(self.addVoid)
        self.actions.addAirAction.triggered.connect(self.addAir)
        self.actions.addWall1x1Action.triggered.connect(self.addWall1x1)
        self.actions.addBottomWall1x2Action.triggered.connect(self.addBottomWall1x2)
        self.actions.addTopWall1x2Action.triggered.connect(self.addTopWall1x2)
        self.actions.addHorizontalWallAction.triggered.connect(self.addHorizontalWall)
        self.actions.addVerticalWallAction.triggered.connect(self.addVerticalWall)
        self.actions.addLightPlatformAction.triggered.connect(self.addLightPlatform)
        self.actions.addPlatformAction.triggered.connect(self.addPlatform)
        self.actions.addLadderAction.triggered.connect(self.addLadder)
        self.actions.addBottomSpikesAction.triggered.connect(self.addBottomSpikes)
        self.actions.addTopSpikesAction.triggered.connect(self.addTopSpikes)
        self.actions.addLeftSpikesAction.triggered.connect(self.addLeftSpikes)
        self.actions.addRightSpikesAction.triggered.connect(self.addRightSpikes)
        self.actions.addStructure.triggered.connect(self.addStructure)
        self.actions.addBottomWall.triggered.connect(self.addBottomWall)
        self.actions.addTopWall.triggered.connect(self.addTopWall)
        self.actions.addLeftWall.triggered.connect(self.addLeftWall)
        self.actions.addRightWall.triggered.connect(self.addRightWall)
        self.actions.addLeftTopWall.triggered.connect(self.addLeftTopWall)
        self.actions.addRightTopWall.triggered.connect(self.addRightTopWall)
        self.actions.addNarrowStructure.triggered.connect(self.addNarrowStructure)
        self.actions.addTopWallWithCorners.triggered.connect(self.addTopWallWithCorners)
        self.actions.addChain.triggered.connect(self.addChain)
        self.actions.addTile.triggered.connect(self.addTile)
        self.actions.addPattern4x4_1.triggered.connect(self.addPattern4x4_1)
        self.actions.addPattern4x4_2.triggered.connect(self.addPattern4x4_2)
        self.actions.addPattern4x4_3.triggered.connect(self.addPattern4x4_3)
        self.actions.addPattern3x3_1.triggered.connect(self.addPattern3x3_1)
        self.actions.addPattern3x3_2.triggered.connect(self.addPattern3x3_2)
        self.actions.addPattern3x3_3.triggered.connect(self.addPattern3x3_3)

        self.actions.addMovingPlatformAction.triggered.connect(self.addMovingPlatform)
        self.actions.addHorizontalEnemy.triggered.connect(self.addHorizontalEnemy)
        self.actions.addVerticalEnemy.triggered.connect(self.addVerticalEnemy)
        self.actions.addLeftToRightLaser.triggered.connect(self.addLeftToRightLaser)
        self.actions.addRightToLeftLaser.triggered.connect(self.addRightToLeftLaser)
        self.actions.addTurnsOnButton.triggered.connect(self.addTurnsOnButton)
        self.actions.addTurnsOffButton.triggered.connect(self.addTurnsOffButton)
        self.actions.addTimeForceField.triggered.connect(self.addTimeForceField)
        self.actions.addStateForceField.triggered.connect(self.addStateForceField)
        self.actions.addTimeDisappearingPlatform.triggered.connect(self.addTimeDisappearingPlatform)
        self.actions.addStateDisappearingPlatform.triggered.connect(self.addStateDisappearingPlatform)
        self.actions.addHourglassAction.triggered.connect(self.addHourglass)
        self.actions.addLeftSidePortal.triggered.connect(self.addLeftSidePortal)
        self.actions.addRightSidePortal.triggered.connect(self.addRightSidePortal)
        self.actions.addBothSidesPortal.triggered.connect(self.addBothSidesPortal)

    def updateLanguage(self):
        self.menu.updateLanguage()

    def boardMousePressEvent(self, event):
        if event.buttons() != Qt.MouseButton.LeftButton:
            return

        item = self.imageGenerator.getObjectAt(event.pos())
        self.selectionManager.setSelected(item)
        if item is None:
            self.startMousePos = None
            self.startObjectPos = None
            self.startObjectSize = None
        else:
            self.startMousePos = event.pos()
            self.startObjectPos = item.getRect().topLeft()
            self.startObjectSize = item.getSize()
        self.updateCursor()

    def boardMouseReleaseEvent(self, event):
        self.boardMouseHover(event.pos())

    def boardMouseMoveEvent(self, event):
        if event.buttons() == Qt.MouseButton.LeftButton:
            self.boardMouseDrag(event.pos())
        else:
            self.startMousePos = None
            if event.buttons() == Qt.MouseButton.NoButton:
                self.boardMouseHover(event.pos())

    def boardMouseDrag(self, pos: QPoint):
        if self.startMousePos is None:
            return

        item = self.selectionManager.selected
        delta = self.imageGenerator.imagePosToGamePos(pos - self.startMousePos)
        if self.hoverObjectResizeMode == ResizeObjectMode.ResizeOnRight:
            width = self.startObjectSize.width() + delta.x() // PixelInfo.pixelsInColumn
            size = item.getSize()
            self.setObjectSize(item, QSize(width, size.height()))
        elif self.hoverObjectResizeMode == ResizeObjectMode.ResizeOnBottom:
            height = self.startObjectSize.height() + delta.y() // PixelInfo.pixelsInRow
            size = item.getSize()
            self.setObjectSize(item, QSize(size.width(), height))
        elif self.hoverObjectResizeMode == ResizeObjectMode.ResizeOnRightBottom:
            width = self.startObjectSize.width() + delta.x() // PixelInfo.pixelsInColumn
            height = self.startObjectSize.height() + delta.y() // PixelInfo.pixelsInRow
            self.setObjectSize(item, QSize(width, height))
        else:
            objectPos = self.startObjectPos + delta
            self.setObjectPosition(item, objectPos)

    def setObjectPosition(self, item, objectPos):
        if issubclass(item.__class__, LevelPortalInInfo):
            self.portalInChangePos(item, objectPos)
        elif issubclass(item.__class__, LevelPortalOutInfo):
            self.portalOutChangePos(item, objectPos)
        elif issubclass(item.__class__, BoardItemInfo):
            self.boardItemChangePos(item, objectPos)
        elif issubclass(item.__class__, MovingPlatformInfo):
            self.movingPlatformChangePos(item, objectPos)
        elif issubclass(item.__class__, EnemyInfo):
            self.enemyChangePos(item, objectPos)
        elif issubclass(item.__class__, LaserInfo):
            self.laserChangePos(item, objectPos)
        elif issubclass(item.__class__, ButtonInfo):
            self.buttonChangePos(item, objectPos)
        elif issubclass(item.__class__, ForceFieldInfo):
            self.forceFieldChangePos(item, objectPos)
        elif issubclass(item.__class__, DisappearingPlatformInfo):
            self.disappearingPlatformChangePos(item, objectPos)
        elif issubclass(item.__class__, HourglassInfo):
            self.hourglassChangePos(item, objectPos)
        elif issubclass(item.__class__, PortalInfo):
            self.portalChangePos(item, objectPos)

    def setObjectSize(self, item, objectSize):
        if issubclass(item.__class__, LevelPortalInInfo):
            self.portalInChangeSize(item, objectSize)
        elif issubclass(item.__class__, LevelPortalOutInfo):
            self.portalOutChangeSize(item, objectSize)
        elif issubclass(item.__class__, BoardItemInfo):
            self.boardItemChangeSize(item, objectSize)
        elif issubclass(item.__class__, MovingPlatformInfo):
            self.movingPlatformChangeSize(item, objectSize)
        elif issubclass(item.__class__, LaserInfo):
            self.laserChangeSize(item, objectSize)
        elif issubclass(item.__class__, ForceFieldInfo):
            self.forceFieldChangeSize(item, objectSize)
        elif issubclass(item.__class__, DisappearingPlatformInfo):
            self.disappearingPlatformChangeSize(item, objectSize)
        elif issubclass(item.__class__, PortalInfo):
            self.portalChangeSize(item, objectSize)

    def boardMouseHover(self, pos: QPoint):
        item = self.imageGenerator.getObjectAt(pos)
        if self.hoverItem != item:
            self.updateHoverObject(item, pos)
        else:
            mode = self.imageGenerator.getResizeModeObjectAt(pos)
            if self.hoverObjectResizeMode != mode:
                self.hoverObjectResizeMode = mode
                self.updateCursor()

    def boardLeaveEvent(self, event):
        self.hoverObjectResizeMode = ResizeObjectMode.NoResize
        self.updateHoverObject(None, None)
        self.updateCursor()

    def updateHoverObject(self, item, pos):
        self.hoverItem = item
        for generator in self.imageGenerators.values():
            generator.hoverItem = item
        self.hoverObjectResizeMode = self.imageGenerator.getResizeModeObjectAt(pos)
        self.updateContent()
        self.updateCursor()

    def updateCursor(self):
        if (self.selectionManager.selected is None) or (self.selectionManager.selected != self.hoverItem):
            self.setCursor(QCursor(QtCore.Qt.CursorShape.ArrowCursor))
        else:
            if self.hoverObjectResizeMode == ResizeObjectMode.ResizeOnRight:
                self.setCursor(QCursor(QtCore.Qt.CursorShape.SizeHorCursor))
            elif self.hoverObjectResizeMode == ResizeObjectMode.ResizeOnBottom:
                self.setCursor(QCursor(QtCore.Qt.CursorShape.SizeVerCursor))
            elif self.hoverObjectResizeMode == ResizeObjectMode.ResizeOnRightBottom:
                self.setCursor(QCursor(QtCore.Qt.CursorShape.SizeFDiagCursor))
            else:
                self.setCursor(QCursor(QtCore.Qt.CursorShape.SizeAllCursor))

    def portalInChangePos(self, item: LevelPortalInInfo, pos: QPoint):
        oldValue = item.getPos()
        newValue = QPoint(PixelInfo.getCorrectedColumn(pos), PixelInfo.getCorrectedRow(pos))
        if oldValue != newValue:
            command = LevelPortalInPosChangeCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def portalOutChangePos(self, item: LevelPortalOutInfo, pos: QPoint):
        oldValue = item.getPos()
        newValue = QPoint(PixelInfo.getCorrectedColumn(pos), PixelInfo.getCorrectedRow(pos))
        if oldValue != newValue:
            command = LevelPortalOutPosChangeCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def boardItemChangePos(self, item: BoardItemInfo, pos: QPoint):
        oldValue = item.getPos()
        newValue = QPoint(PixelInfo.getCorrectedColumn(pos), PixelInfo.getCorrectedRow(pos))
        if oldValue != newValue:
            command = BoardItemPosChangedCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def movingPlatformChangePos(self, item: MovingPlatformInfo, pos: QPoint):
        oldValue = item.getPos()
        newValue = QPoint(PixelInfo.getCorrectXPos(pos), PixelInfo.getCorrectedRow(pos))
        if oldValue != newValue:
            command = MovingPlatformPosChangedCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def enemyChangePos(self, item: EnemyInfo, pos: QPoint):
        oldValue = item.getPos()
        newValue = QPoint(PixelInfo.getCorrectXPos(pos), PixelInfo.getCorrectYPos(pos))
        if oldValue != newValue:
            command = EnemyPosChangedCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def laserChangePos(self, item: LaserInfo, pos: QPoint):
        oldValue = item.getPos()
        newValue = QPoint(PixelInfo.getCorrectedColumn(pos), PixelInfo.getCorrectedRow(pos))
        if oldValue != newValue:
            command = LaserPosChangedCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def buttonChangePos(self, item: ButtonInfo, pos: QPoint):
        oldValue = item.getPos()
        newValue = QPoint(PixelInfo.getCorrectedColumn(pos), PixelInfo.getCorrectedRow(pos))
        if oldValue != newValue:
            command = ButtonPosChangedCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def forceFieldChangePos(self, item: ForceFieldInfo, pos: QPoint):
        oldValue = item.getPos()
        newValue = QPoint(PixelInfo.getCorrectedColumn(pos), PixelInfo.getCorrectedRow(pos))
        if oldValue != newValue:
            command = ForceFieldPosChangedCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def disappearingPlatformChangePos(self, item: DisappearingPlatformInfo, pos: QPoint):
        oldValue = item.getPos()
        newValue = QPoint(PixelInfo.getCorrectedColumn(pos), PixelInfo.getCorrectedRow(pos))
        if oldValue != newValue:
            command = DisappearingPlatformPosChangedCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def hourglassChangePos(self, item: HourglassInfo, pos: QPoint):
        oldValue = item.getPos()
        newValue = QPoint(PixelInfo.getCorrectedColumn(pos), PixelInfo.getCorrectedRow(pos))
        if oldValue != newValue:
            command = HourglassPosChangedCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def portalChangePos(self, item: PortalInfo, pos: QPoint):
        oldValue = item.getPos()
        newValue = QPoint(PixelInfo.getCorrectedColumn(pos), PixelInfo.getCorrectedRow(pos))
        if oldValue != newValue:
            command = PortalPosChangedCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def portalInChangeSize(self, item: LevelPortalInInfo, size: QSize):
        oldValue = item.getSize()
        newValue = item.getCorrectedSize(size)
        if oldValue != newValue:
            command = LevelPortalInSizeChangeCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def portalOutChangeSize(self, item: LevelPortalOutInfo, size: QSize):
        oldValue = item.getSize()
        newValue = item.getCorrectedSize(size)
        if oldValue != newValue:
            command = LevelPortalOutSizeChangeCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def boardItemChangeSize(self, item: BoardItemInfo, size: QSize):
        oldValue = item.getSize()
        newValue = item.getCorrectedSize(size)
        if oldValue != newValue:
            command = BoardItemSizeChangedCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def movingPlatformChangeSize(self, item: MovingPlatformInfo, size: QSize):
        oldValue = item.getSize()
        newValue = item.getCorrectedSize(size)
        if oldValue != newValue:
            command = MovingPlatformSizeChangedCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def laserChangeSize(self, item: LaserInfo, size: QSize):
        oldValue = item.getSize()
        newValue = item.getCorrectedSize(size)
        if oldValue != newValue:
            command = LaserSizeChangedCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def forceFieldChangeSize(self, item: ForceFieldInfo, size: QSize):
        oldValue = item.getSize()
        newValue = item.getCorrectedSize(size)
        if oldValue != newValue:
            command = ForceFieldSizeChangedCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def disappearingPlatformChangeSize(self, item: DisappearingPlatformInfo, size: QSize):
        oldValue = item.getSize()
        newValue = item.getCorrectedSize(size)
        if oldValue != newValue:
            command = DisappearingPlatformSizeChangedCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def portalChangeSize(self, item: PortalInfo, size: QSize):
        oldValue = item.getSize()
        newValue = item.getCorrectedSize(size)
        if oldValue != newValue:
            command = PortalSizeChangedCommand(item, oldValue, newValue, self.lang)
            self.history.push(command)

    def updateContent(self):
        self.imageGenerator.generate()
        self.board.setPixmap(self.imageGenerator.image)

    def updateTime(self, levelTime, globalTime):
        for generator in self.imageGenerators.values():
            generator.levelTime = levelTime
            generator.globalTime = globalTime
        self.updateContent()

    def setZoom(self, value: ZoomKind):
        self.zoom = value
        self.imageGenerator = self.imageGenerators[value]
        self.updateContent()
        self.board.setGeometry(0, 0, self.imageGenerator.image.width(), self.imageGenerator.image.height())

    def boardContextMenuEvent(self, event):
        self.contextMenuPos = QPoint(event.x(), event.y())
        self.menu.exec(self.board.mapToGlobal(self.contextMenuPos))

    def boardKeyPressEvent(self, event: QKeyEvent):
        if event.modifiers() == QtCore.Qt.KeyboardModifier.NoModifier:
            if event.key() == QtCore.Qt.Key.Key_Delete:
                self.deleteSelected()
            elif event.key() == QtCore.Qt.Key.Key_Left:
                self.shiftSelectedObjectLeft()
            elif event.key() == QtCore.Qt.Key.Key_Right:
                self.shiftSelectedObjectRight()
            elif event.key() == QtCore.Qt.Key.Key_Up:
                self.shiftSelectedObjectUp()
            elif event.key() == QtCore.Qt.Key.Key_Down:
                self.shiftSelectedObjectDown()
            elif event.key() == QtCore.Qt.Key.Key_Home:
                self.moveSelectedObjectTop()
            elif event.key() == QtCore.Qt.Key.Key_PageUp:
                self.moveSelectedObjectUp()
            elif event.key() == QtCore.Qt.Key.Key_PageDown:
                self.moveSelectedObjectDown()
            elif event.key() == QtCore.Qt.Key.Key_End:
                self.moveSelectedObjectBottom()
        elif event.modifiers() == QtCore.Qt.KeyboardModifier.ShiftModifier:
            if event.key() == QtCore.Qt.Key.Key_Left:
                self.shrinkSelectedObjectHorizontally()
            elif event.key() == QtCore.Qt.Key.Key_Right:
                self.expandSelectedObjectHorizontally()
            elif event.key() == QtCore.Qt.Key.Key_Up:
                self.shrinkSelectedObjectVertically()
            elif event.key() == QtCore.Qt.Key.Key_Down:
                self.expandSelectedObjectVertically()

    def deleteSelected(self):
        command = self.commandFactory.createRemoveItem(self.selectionManager.selected)
        if not (command is None):
            self.history.push(command)

    def shiftSelectedObjectLeft(self):
        item = self.selectionManager.selected
        if not (item is None):
            pos = item.getRect().topLeft() + QPoint(-item.horizontalShift(), 0)
            self.setObjectPosition(item, pos)

    def shiftSelectedObjectRight(self):
        item = self.selectionManager.selected
        if not (item is None):
            pos = item.getRect().topLeft() + QPoint(item.horizontalShift(), 0)
            self.setObjectPosition(item, pos)

    def shiftSelectedObjectUp(self):
        item = self.selectionManager.selected
        if not (item is None):
            pos = item.getRect().topLeft() + QPoint(0, -item.verticalShift())
            self.setObjectPosition(item, pos)

    def shiftSelectedObjectDown(self):
        item = self.selectionManager.selected
        if not (item is None):
            pos = item.getRect().topLeft() + QPoint(0, item.verticalShift())
            self.setObjectPosition(item, pos)

    def shrinkSelectedObjectHorizontally(self):
        item = self.selectionManager.selected
        if not (item is None):
            size = item.getSize() + QSize(-1, 0)
            self.setObjectSize(item, size)

    def expandSelectedObjectHorizontally(self):
        item = self.selectionManager.selected
        if not (item is None):
            size = item.getSize() + QSize(1, 0)
            self.setObjectSize(item, size)

    def shrinkSelectedObjectVertically(self):
        item = self.selectionManager.selected
        if not (item is None):
            size = item.getSize() + QSize(0, -1)
            self.setObjectSize(item, size)

    def expandSelectedObjectVertically(self):
        item = self.selectionManager.selected
        if not (item is None):
            size = item.getSize() + QSize(0, 1)
            self.setObjectSize(item, size)

    def moveSelectedObjectTop(self):
        command = self.commandFactory.createMoveItemTop(self.selectionManager.selected)
        if not (command is None):
            self.history.push(command)

    def moveSelectedObjectUp(self):
        command = self.commandFactory.createMoveItemUp(self.selectionManager.selected)
        if not (command is None):
            self.history.push(command)

    def moveSelectedObjectDown(self):
        command = self.commandFactory.createMoveItemDown(self.selectionManager.selected)
        if not (command is None):
            self.history.push(command)

    def moveSelectedObjectBottom(self):
        command = self.commandFactory.createMoveItemBottom(self.selectionManager.selected)
        if not (command is None):
            self.history.push(command)

    def addBoardItem(self, kind: BoardItemKind):
        if self.contextMenuPos is None:
            return

        pos = self.imageGenerator.imagePosToGamePos(self.contextMenuPos)
        pos = QPoint(PixelInfo.getCorrectedColumn(pos), PixelInfo.getCorrectedRow(pos))
        info = BoardItemInfo(kind,pos.x(), pos.y(), 1, 1)
        command = self.commandFactory.createAddItem(info)
        self.history.push(command)
        self.selectionManager.setSelected(info)

    def addVoid(self):
        self.addBoardItem(BoardItemKind.Void)

    def addAir(self):
        self.addBoardItem(BoardItemKind.Air)

    def addWall1x1(self):
        self.addBoardItem(BoardItemKind.Wall1x1)

    def addBottomWall1x2(self):
        self.addBoardItem(BoardItemKind.BottomWall1x2)

    def addTopWall1x2(self):
        self.addBoardItem(BoardItemKind.TopWall1x2)

    def addHorizontalWall(self):
        self.addBoardItem(BoardItemKind.HorizontalWall)

    def addVerticalWall(self):
        self.addBoardItem(BoardItemKind.VerticalWall)

    def addLightPlatform(self):
        self.addBoardItem(BoardItemKind.LightPlatform)

    def addPlatform(self):
        self.addBoardItem(BoardItemKind.Platform)

    def addLadder(self):
        self.addBoardItem(BoardItemKind.Ladder)

    def addBottomSpikes(self):
        self.addBoardItem(BoardItemKind.BottomSpikes)

    def addTopSpikes(self):
        self.addBoardItem(BoardItemKind.TopSpikes)

    def addLeftSpikes(self):
        self.addBoardItem(BoardItemKind.LeftSpikes)

    def addRightSpikes(self):
        self.addBoardItem(BoardItemKind.RightSpikes)

    def addStructure(self):
        self.addBoardItem(BoardItemKind.Structure)

    def addBottomWall(self):
        self.addBoardItem(BoardItemKind.BottomWall)

    def addTopWall(self):
        self.addBoardItem(BoardItemKind.TopWall)

    def addLeftWall(self):
        self.addBoardItem(BoardItemKind.LeftWall)

    def addRightWall(self):
        self.addBoardItem(BoardItemKind.RightWall)

    def addLeftTopWall(self):
        self.addBoardItem(BoardItemKind.LeftTopWall)

    def addRightTopWall(self):
        self.addBoardItem(BoardItemKind.RightTopWall)

    def addNarrowStructure(self):
        self.addBoardItem(BoardItemKind.NarrowStructure)

    def addTopWallWithCorners(self):
        self.addBoardItem(BoardItemKind.TopWallWithCorners)

    def addChain(self):
        self.addBoardItem(BoardItemKind.Chain)

    def addTile(self):
        self.addBoardItem(BoardItemKind.Tile)

    def addPattern4x4_1(self):
        self.addBoardItem(BoardItemKind.Pattern4x4_1)

    def addPattern4x4_2(self):
        self.addBoardItem(BoardItemKind.Pattern4x4_2)

    def addPattern4x4_3(self):
        self.addBoardItem(BoardItemKind.Pattern4x4_3)

    def addPattern3x3_1(self):
        self.addBoardItem(BoardItemKind.Pattern3x3_1)

    def addPattern3x3_2(self):
        self.addBoardItem(BoardItemKind.Pattern3x3_2)

    def addPattern3x3_3(self):
        self.addBoardItem(BoardItemKind.Pattern3x3_3)

    def addMovingPlatform(self):
        if self.contextMenuPos is None:
            return

        pos = self.imageGenerator.imagePosToGamePos(self.contextMenuPos)
        pos = QPoint(PixelInfo.getCorrectXPos(pos), PixelInfo.getCorrectedRow(pos))
        info = MovingPlatformInfo(False, pos.x(), pos.y(), 2, RangeKind.Range1, 0)
        command = self.commandFactory.createAddItem(info)
        self.history.push(command)
        self.selectionManager.setSelected(info)

    def addEnemy(self, kind: EnemyKind):
        if self.contextMenuPos is None:
            return

        pos = self.imageGenerator.imagePosToGamePos(self.contextMenuPos)
        pos = QPoint(PixelInfo.getCorrectXPos(pos), PixelInfo.getCorrectYPos(pos))
        info = EnemyInfo(kind, False, pos.x(), pos.y(), RangeKind.Range1, 0)
        command = self.commandFactory.createAddItem(info)
        self.history.push(command)
        self.selectionManager.setSelected(info)

    def addHorizontalEnemy(self):
        self.addEnemy(EnemyKind.Horizontal)

    def addVerticalEnemy(self):
        self.addEnemy(EnemyKind.Vertical)

    def addLaser(self, kind: LaserKind):
        if self.contextMenuPos is None:
            return

        pos = self.imageGenerator.imagePosToGamePos(self.contextMenuPos)
        pos = QPoint(PixelInfo.getCorrectedColumn(pos), PixelInfo.getCorrectedRow(pos))
        info = LaserInfo(kind, False, pos.x(), pos.y(), 2, 0)
        command = self.commandFactory.createAddItem(info)
        self.history.push(command)
        self.selectionManager.setSelected(info)

    def addLeftToRightLaser(self):
        self.addLaser(LaserKind.LeftToRight)

    def addRightToLeftLaser(self):
        self.addLaser(LaserKind.RightToLeft)

    def addButton(self, kind: ButtonKind):
        if self.contextMenuPos is None:
            return

        pos = self.imageGenerator.imagePosToGamePos(self.contextMenuPos)
        pos = QPoint(PixelInfo.getCorrectedColumn(pos), PixelInfo.getCorrectedRow(pos))
        info = ButtonInfo(kind, False, pos.x(), pos.y(), 0)
        command = self.commandFactory.createAddItem(info)
        self.history.push(command)
        self.selectionManager.setSelected(info)

    def addTurnsOnButton(self):
        self.addButton(ButtonKind.TurnsOn)

    def addTurnsOffButton(self):
        self.addButton(ButtonKind.TurnsOff)

    def addForceField(self, kind: ActivationMethodKind):
        if self.contextMenuPos is None:
            return

        pos = self.imageGenerator.imagePosToGamePos(self.contextMenuPos)
        pos = QPoint(PixelInfo.getCorrectedColumn(pos), PixelInfo.getCorrectedRow(pos))
        info = ForceFieldInfo(kind, False, False, pos.x(), pos.y(), 2, 0, 0)
        command = self.commandFactory.createAddItem(info)
        self.history.push(command)
        self.selectionManager.setSelected(info)

    def addTimeForceField(self):
        self.addForceField(ActivationMethodKind.Time)

    def addStateForceField(self):
        self.addForceField(ActivationMethodKind.State)

    def addDisappearingPlatform(self, kind: ActivationMethodKind):
        if self.contextMenuPos is None:
            return

        pos = self.imageGenerator.imagePosToGamePos(self.contextMenuPos)
        pos = QPoint(PixelInfo.getCorrectedColumn(pos), PixelInfo.getCorrectedRow(pos))
        info = DisappearingPlatformInfo(kind, False, False, pos.x(), pos.y(), 2, 0, 0)
        command = self.commandFactory.createAddItem(info)
        self.history.push(command)
        self.selectionManager.setSelected(info)

    def addTimeDisappearingPlatform(self):
        self.addDisappearingPlatform(ActivationMethodKind.Time)

    def addStateDisappearingPlatform(self):
        self.addDisappearingPlatform(ActivationMethodKind.State)

    def addPortal(self, kind: PortalEntryKind):
        if self.contextMenuPos is None:
            return

        pos = self.imageGenerator.imagePosToGamePos(self.contextMenuPos)
        pos = QPoint(PixelInfo.getCorrectedColumn(pos), PixelInfo.getCorrectedRow(pos))
        info = PortalInfo(kind, pos.x(), pos.y(), 2)
        command = self.commandFactory.createAddItem(info)
        self.history.push(command)
        self.selectionManager.setSelected(info)

    def addLeftSidePortal(self):
        self.addPortal(PortalEntryKind.LeftSide)

    def addRightSidePortal(self):
        self.addPortal(PortalEntryKind.RightSide)

    def addBothSidesPortal(self):
        self.addPortal(PortalEntryKind.BothSides)

    def addHourglass(self):
        if self.contextMenuPos is None:
            return

        pos = self.imageGenerator.imagePosToGamePos(self.contextMenuPos)
        pos = QPoint(PixelInfo.getCorrectedColumn(pos), PixelInfo.getCorrectedRow(pos))
        info = HourglassInfo(False, pos.x(), pos.y())
        command = self.commandFactory.createAddItem(info)
        self.history.push(command)
        self.selectionManager.setSelected(info)
