from enum import Enum

from PyQt6.QtGui import QUndoStack
from PyQt6.QtWidgets import QWidget, QStackedLayout

from commands.commandFactory import CommandFactory
from data.language import Language
from data.selectionManager import SelectionManager
from gui.disappearingPlatformsToolPanel import DisappearingPlatformsToolPanel
from gui.boardItemsToolPanel import BoardItemsToolPanel
from gui.buttonsToolPanel import ButtonsToolPanel
from gui.enemiesToolPanel import EnemiesToolPanel
from gui.forceFieldsToolPanel import ForceFieldsToolPanel
from gui.generalToolPanel import GeneralToolPanel
from gui.hourglassesToolPanel import HourglassesToolPanel
from gui.iconProvider import IconProvider
from gui.itemListToolPanel import ItemListToolPanel
from gui.lasersToolPanel import LasersToolPanel
from gui.listItemsWidgets import ListItemsWidgets
from gui.movingPlatformsToolPanel import MovingPlatformsToolPanel
from gui.levelPortalToolPanel import LevelPortalInToolPanel, LevelPortalOutToolPanel, LevelPortalToolPanel
from gui.portalsToolPanel import PortalsToolPanel
from gui.toolPanelWidget import ToolPanelWidget
from data.levelInfo import LevelInfo, ItemInfo, LevelPortalInInfo, LevelPortalOutInfo, BoardItemInfo, \
    MovingPlatformInfo, EnemyInfo, LaserInfo, ForceFieldInfo, DisappearingPlatformInfo, ButtonInfo, HourglassInfo, PortalInfo
from image.thumbnailGenerator import ThumbnailGenerator


class ToolPanelKind(int, Enum):
    General = 0
    LevelPortalIn = 1
    LevelPortalOut = 2
    BoardItems = 3
    MovingPlatforms = 4
    Enemies = 5
    Lasers = 6
    ForceFields = 7
    DisappearingPlatforms = 8
    Buttons = 9
    Hourglasses = 10
    Portals = 11


class MainToolPanel(QWidget):

    def __init__(self, levelInfo: LevelInfo, history: QUndoStack, selectionManager: SelectionManager,
                 thumbnailGenerator: ThumbnailGenerator, widgets: ListItemsWidgets, commandFactory: CommandFactory,
                 iconProvider: IconProvider, lang: Language):
        super().__init__()

        self.levelInfo = levelInfo
        self.selectionManager = selectionManager
        self.toolPanelsLayout = QStackedLayout()
        self.toolPanelsLayout.addWidget(
            GeneralToolPanel(levelInfo.general, history, selectionManager, iconProvider, lang, thumbnailGenerator))
        self.toolPanelsLayout.addWidget(
            LevelPortalInToolPanel(levelInfo.levelPortalIn, history, selectionManager, iconProvider, lang))
        self.toolPanelsLayout.addWidget(
            LevelPortalOutToolPanel(levelInfo.levelPortalOut, history, selectionManager, iconProvider, lang))

        panel = BoardItemsToolPanel(levelInfo.boardItems, history, selectionManager, iconProvider, lang)
        self.toolPanelsLayout.addWidget(panel)
        widgets.boardItemsListWidget = panel.itemsList.itemsList

        panel = MovingPlatformsToolPanel(levelInfo.movingPlatforms, history, selectionManager, iconProvider, lang)
        self.toolPanelsLayout.addWidget(panel)
        widgets.movingPlatformsListWidget = panel.itemsList.itemsList

        panel = EnemiesToolPanel(levelInfo.enemies, history, selectionManager, iconProvider, lang)
        self.toolPanelsLayout.addWidget(panel)
        widgets.enemiesListWidget = panel.itemsList.itemsList

        panel = LasersToolPanel(levelInfo.lasers, history, selectionManager, iconProvider, lang)
        self.toolPanelsLayout.addWidget(panel)
        widgets.lasersListWidget = panel.itemsList.itemsList

        panel = ForceFieldsToolPanel(levelInfo.forceFields, history, selectionManager, commandFactory, iconProvider,
                                     lang)
        self.toolPanelsLayout.addWidget(panel)
        widgets.forceFieldsListWidget = panel.itemsList.itemsList

        panel = DisappearingPlatformsToolPanel(levelInfo.disappearingPlatforms, history, selectionManager,
                                               commandFactory, iconProvider, lang)
        self.toolPanelsLayout.addWidget(panel)
        widgets.disappearingPlatformsListWidget = panel.itemsList.itemsList

        panel = ButtonsToolPanel(levelInfo.buttons, history, selectionManager, commandFactory, iconProvider, lang)
        self.toolPanelsLayout.addWidget(panel)
        widgets.buttonsListWidget = panel.itemsList.itemsList

        panel = HourglassesToolPanel(levelInfo.hourglasses, history, selectionManager, iconProvider, lang)
        self.toolPanelsLayout.addWidget(panel)
        widgets.hourglassesListWidget = panel.itemsList.itemsList

        panel = PortalsToolPanel(levelInfo.portals, history, selectionManager, iconProvider, lang)
        self.toolPanelsLayout.addWidget(panel)
        widgets.portalsListWidget = panel.itemsList.itemsList

        self.setLayout(self.toolPanelsLayout)
        self.setContentsMargins(0, 0, 0, 0)

    def updateLanguage(self):
        for i in range(self.toolPanelsLayout.count()):
            widget = self.toolPanelsLayout.widget(i)
            if issubclass(widget.__class__, ToolPanelWidget):
                widget.updateLanguage()

    @staticmethod
    def getToolPanelKindForSelection(item: ItemInfo):
        if issubclass(item.__class__, LevelPortalInInfo):
            return ToolPanelKind.LevelPortalIn
        if issubclass(item.__class__, LevelPortalOutInfo):
            return ToolPanelKind.LevelPortalOut
        if issubclass(item.__class__, BoardItemInfo):
            return ToolPanelKind.BoardItems
        if issubclass(item.__class__, MovingPlatformInfo):
            return ToolPanelKind.MovingPlatforms
        if issubclass(item.__class__, EnemyInfo):
            return ToolPanelKind.Enemies
        if issubclass(item.__class__, LaserInfo):
            return ToolPanelKind.Lasers
        if issubclass(item.__class__, ForceFieldInfo):
            return ToolPanelKind.ForceFields
        if issubclass(item.__class__, DisappearingPlatformInfo):
            return ToolPanelKind.DisappearingPlatforms
        if issubclass(item.__class__, ButtonInfo):
            return ToolPanelKind.Buttons
        if issubclass(item.__class__, HourglassInfo):
            return ToolPanelKind.Hourglasses
        if issubclass(item.__class__, PortalInfo):
            return ToolPanelKind.Portals
        return None

    def updateSelection(self):
        widget = self.toolPanelsLayout.currentWidget()

        if self.selectionManager.selected is None:
            if issubclass(widget.__class__, ItemListToolPanel):
                widget.clearSelection()
            elif issubclass(widget.__class__, LevelPortalToolPanel):
                kind = ToolPanelKind.BoardItems
                widget = self.toolPanelsLayout.widget(kind.value)
                widget.clearSelection()
                self.showTab(kind.value)
                return kind
        else:
            kind = self.getToolPanelKindForSelection(self.selectionManager.selected)
            if not (kind is None):
                widget = self.toolPanelsLayout.widget(kind.value)
                if issubclass(widget.__class__, ItemListToolPanel):
                    widget.setSelection(self.selectionManager.selected)
                self.showTab(kind.value)
                return kind

    def updateControls(self):
        for i in range(self.toolPanelsLayout.count()):
            widget = self.toolPanelsLayout.widget(i)
            if issubclass(widget.__class__, ToolPanelWidget):
                widget.updateControls()

    def reinitControls(self):
        for i in range(self.toolPanelsLayout.count()):
            widget = self.toolPanelsLayout.widget(i)
            if issubclass(widget.__class__, ToolPanelWidget):
                widget.reinitControls()

    def showTab(self, index):
        self.toolPanelsLayout.setCurrentIndex(index)
        widget = self.toolPanelsLayout.widget(index)
        if issubclass(widget.__class__, ToolPanelWidget):
            widget.showTab()

    def showGeneral(self):
        self.showTab(ToolPanelKind.General.value)

    def showPortalIn(self):
        self.showTab(ToolPanelKind.LevelPortalIn.value)

    def showPortalOut(self):
        self.showTab(ToolPanelKind.LevelPortalOut.value)

    def showBoardItems(self):
        self.showTab(ToolPanelKind.BoardItems.value)

    def showMovingPlatforms(self):
        self.showTab(ToolPanelKind.MovingPlatforms.value)

    def showEnemies(self):
        self.showTab(ToolPanelKind.Enemies.value)

    def showLasers(self):
        self.showTab(ToolPanelKind.Lasers.value)

    def showForceFields(self):
        self.showTab(ToolPanelKind.ForceFields.value)

    def showDisappearingPlatforms(self):
        self.showTab(ToolPanelKind.DisappearingPlatforms.value)

    def showButtons(self):
        self.showTab(ToolPanelKind.Buttons.value)

    def showHourglasses(self):
        self.showTab(ToolPanelKind.Hourglasses.value)

    def showPortals(self):
        self.showTab(ToolPanelKind.Portals.value)
