from PyQt6.QtCore import Qt, pyqtSlot
from PyQt6.QtGui import QUndoStack
from PyQt6.QtWidgets import QLabel, QComboBox, QGridLayout, QSpinBox

from commands.generalCommands import BoardColorChangeCommand, BoardColumnsChangeCommand, LevelTimeChangeCommand
from data.language import Language
from data.levelInfo import GeneralInfo, BoardColor
from data.ranges import Ranges
from data.selectionManager import SelectionManager
from gui.headerWidget import HeaderWidget, QHLine
from gui.iconProvider import IconProvider
from gui.levelThumbnail import LevelThumbnail
from gui.toolPanelWidget import ToolPanelWidget
from image.thumbnailGenerator import ThumbnailGenerator


class GeneralToolPanel(ToolPanelWidget):

    def __init__(self, data: GeneralInfo, history: QUndoStack, selectionManager: SelectionManager,
                 iconProvider: IconProvider, lang: Language, thumbnailGenerator: ThumbnailGenerator):
        super().__init__(history, selectionManager, lang)
        self.data = data
        self.generator = thumbnailGenerator

        layout = QGridLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setAlignment(Qt.AlignmentFlag.AlignTop)
        self.header = HeaderWidget(iconProvider, None, ':/icons/view-general')
        layout.addWidget(self.header, 0, 0, 1, 2)
        layout.addWidget(QHLine(), 1, 0, 1, 2)

        self.colorLabel = QLabel(None)
        layout.addWidget(self.colorLabel, 2, 0, 1, 1)
        self.colorComboBox = QComboBox()
        for x in BoardColor:
            self.colorComboBox.addItem(x.name, x)
        self.colorComboBox.currentIndexChanged[int].connect(self.colorChanged)
        layout.addWidget(self.colorComboBox, 2, 1, 1, 1)

        self.columnsLabel = QLabel(None)
        layout.addWidget(self.columnsLabel, 3, 0, 1, 1)
        self.columnsSpinBox = QSpinBox()
        self.columnsSpinBox.setRange(Ranges.levelColumns.min, Ranges.levelColumns.max)
        self.columnsSpinBox.valueChanged[int].connect(self.columnsChanged)
        layout.addWidget(self.columnsSpinBox, 3, 1, 1, 1)

        self.timeLabel = QLabel(None)
        layout.addWidget(self.timeLabel, 4, 0, 1, 1)
        self.timeSpinBox = QSpinBox()
        self.timeSpinBox.setRange(Ranges.seconds.min, Ranges.seconds.max)
        self.timeSpinBox.valueChanged[int].connect(self.timeChanged)
        layout.addWidget(self.timeSpinBox, 4, 1, 1, 1)

        self.levelThumbnail = LevelThumbnail(self.generator)
        layout.addWidget(self.levelThumbnail, 5, 0, 1, 2)

        self.setLayout(layout)
        self.updateControls()

    def updateLanguage(self):
        self.header.setText(self.lang.general())
        self.colorLabel.setText(self.lang.color())
        self.columnsLabel.setText(self.lang.columns())
        self.timeLabel.setText(self.lang.timeSec())
        for i in range(self.colorComboBox.count()):
            self.colorComboBox.setItemText(i, self.lang.boardColorEnum(self.colorComboBox.itemData(i)))

    def reinitControls(self):
        self.updateControls()

    def internalUpdateControls(self):
        self.colorComboBox.setCurrentIndex(list(BoardColor).index(self.data.color))
        self.columnsSpinBox.setValue(self.data.columns)
        self.timeSpinBox.setValue(self.data.seconds)
        self.levelThumbnail.updateThumbnail()

    def showTab(self):
        self.selectionManager.clear()

    @pyqtSlot(int)
    def colorChanged(self):
        if self.updating:
            return

        color = self.colorComboBox.itemData(self.colorComboBox.currentIndex())
        if self.data.color != color:
            command = BoardColorChangeCommand(self.data, self.data.color, color, self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def columnsChanged(self, value):
        if self.updating:
            return

        if self.data.columns != value:
            command = BoardColumnsChangeCommand(self.data, self.data.columns, value, self.lang)
            self.history.push(command)

    @pyqtSlot(int)
    def timeChanged(self, value):
        if self.updating:
            return

        if self.data.seconds != value:
            command = LevelTimeChangeCommand(self.data, self.data.seconds, value, self.lang)
            self.history.push(command)
