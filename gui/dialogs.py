from PyQt6.QtWidgets import QFileDialog, QMessageBox

from data.language import Language


class Dialogs:

    @staticmethod
    def openFile(lang: Language):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.FileMode.ExistingFile)
        dlg.setAcceptMode(QFileDialog.AcceptMode.AcceptOpen)
        dlg.setNameFilter(lang.jsonFiles())
        if not dlg.exec():
            return None
        files = dlg.selectedFiles()
        if len(files) == 0:
            return None
        return files[0]

    @staticmethod
    def saveFile(lang: Language):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.FileMode.AnyFile)
        dlg.setAcceptMode(QFileDialog.AcceptMode.AcceptSave)
        dlg.setNameFilter(lang.jsonFiles())
        if not dlg.exec():
            return None
        files = dlg.selectedFiles()
        if len(files) == 0:
            return None
        return files[0]

    @staticmethod
    def importFile(lang: Language):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.FileMode.ExistingFile)
        dlg.setAcceptMode(QFileDialog.AcceptMode.AcceptOpen)
        dlg.setNameFilter(lang.asmFiles())
        if not dlg.exec():
            return None
        files = dlg.selectedFiles()
        if len(files) == 0:
            return None
        return files[0]

    @staticmethod
    def exportFile(lang: Language):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.FileMode.AnyFile)
        dlg.setAcceptMode(QFileDialog.AcceptMode.AcceptSave)
        dlg.setNameFilter(lang.asmFiles())
        if not dlg.exec():
            return None
        files = dlg.selectedFiles()
        if len(files) == 0:
            return None
        return files[0]

    @staticmethod
    def error(lang: Language, error: str, text: str):
        dlg = QMessageBox()
        dlg.setIcon(QMessageBox.Icon.Critical)
        dlg.setWindowTitle(lang.error())
        dlg.setText(error)
        dlg.setInformativeText(text)
        dlg.exec()

    @staticmethod
    def doNotClose(lang: Language):
        dlg = QMessageBox()
        dlg.setWindowTitle(lang.warning())
        dlg.setText(lang.levelNotSaved())
        dlg.setIcon(QMessageBox.Icon.Warning)
        dlg.setStandardButtons(QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No)
        buttonY = dlg.button(QMessageBox.StandardButton.Yes)
        buttonY.setText(lang.yes())
        buttonN = dlg.button(QMessageBox.StandardButton.No)
        buttonN.setText(lang.no())
        return dlg.exec() == QMessageBox.StandardButton.No

    @staticmethod
    def doNotExport(lang: Language):
        dlg = QMessageBox()
        dlg.setWindowTitle(lang.warning())
        dlg.setText(lang.thereAreProblemsExport())
        dlg.setIcon(QMessageBox.Icon.Warning)
        dlg.setStandardButtons(QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No)
        buttonY = dlg.button(QMessageBox.StandardButton.Yes)
        buttonY.setText(lang.yes())
        buttonN = dlg.button(QMessageBox.StandardButton.No)
        buttonN.setText(lang.no())
        return dlg.exec() == QMessageBox.StandardButton.No

