from PyQt6.QtCore import Qt, QSize, QTimer
from PyQt6.QtGui import QPixmap
from PyQt6.QtWidgets import QDialog, QVBoxLayout, QLabel, QPushButton

from data.language import Language
from gui.headerWidget import QHLine


class AboutWindow(QDialog):

    imageNames = [':/icons/time-wizard-1', ':/icons/time-wizard-2', ':/icons/time-wizard-3', ':/icons/time-wizard-4']

    def __init__(self, parent, version: str, lang: Language):
        super().__init__(parent)
        self.version = version
        self.lang = lang
        self.setFixedSize(QSize(420, 420))
        layout = QVBoxLayout()
        layout.setAlignment(Qt.AlignmentFlag.AlignTop)

        self.animationFrame = 0
        self.images = [QPixmap(name) for name in self.imageNames]
        self.animation = QLabel(None)
        self.animation.setPixmap(self.images[0])
        self.animation.setAlignment(Qt.AlignmentFlag.AlignCenter)
        layout.addWidget(self.animation)

        self.titleLabel = QLabel(None)
        self.titleLabel.setAlignment(Qt.AlignmentFlag.AlignCenter)
        font = self.titleLabel.font()
        font.setBold(True)
        font.setPointSizeF(font.pointSize() * 1.5)
        self.titleLabel.setFont(font)
        layout.addWidget(self.titleLabel)

        authorLabel = QLabel('powered by amarok')
        authorLabel.setAlignment(Qt.AlignmentFlag.AlignCenter)
        layout.addWidget(authorLabel)

        linkLabel = QLabel()
        urlLink = ('<a href=\"https://gitlab.com/amarok8bit/time-wizard-level-editor\">https://gitlab.com/amarok8bit'
                   '/time-wizard-level-editor</a>')
        linkLabel.setText(urlLink)
        linkLabel.setAlignment(Qt.AlignmentFlag.AlignCenter)
        linkLabel.setOpenExternalLinks(True)
        layout.addWidget(linkLabel)

        layout.addWidget(QLabel())
        layout.addWidget(QHLine())

        licenseText = QLabel()
        licenseText.setWordWrap(True)
        licenseText.setText(
            'This program is free software; you can redistribute it and/or modify it under the terms of the GNU '
            'General Public License as published by the Free Software Foundation; either version 3 of the License, '
            'or at your option any later version.')
        layout.addWidget(licenseText)
        font = licenseText.font()
        font.setPointSize(9)
        licenseText.setFont(font)

        license2Text = QLabel()
        license2Text.setWordWrap(True)
        license2Text.setText(
            'This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even '
            'the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public '
            'License for more details.')
        layout.addWidget(license2Text)
        license2Text.setFont(font)

        license3Text = QLabel()
        license3Text.setWordWrap(True)
        license3Text.setText(
            'You should have received a copy of the GNU General Public License along with this program. If not, '
            'see <https://www.gnu.org/licenses/>.')
        layout.addWidget(license3Text)
        license3Text.setFont(font)

        layout.addWidget(QHLine())
        layout.addWidget(QLabel())

        self.closeButton = QPushButton()
        self.closeButton.clicked.connect(self.closeClicked)
        layout.addWidget(self.closeButton)

        self.setLayout(layout)

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.animate)

    def updateLanguage(self):
        self.setWindowTitle(self.lang.about())
        self.titleLabel.setText(f'{self.lang.mainTitle()} {self.version}')
        self.closeButton.setText(self.lang.close())

    def animate(self):
        self.animationFrame += 1
        if self.animationFrame >= len(self.images):
            self.animationFrame = 0
        self.animation.setPixmap(self.images[self.animationFrame])

    def showEvent(self, event):
        self.timer.start(200)

    def hideEvent(self, event):
        self.timer.stop()

    def closeClicked(self):
        self.hide()
