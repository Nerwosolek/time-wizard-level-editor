from PyQt6.QtGui import QPixmap, QPainter, QBrush, QColor

from data.enums import BoardItemKind, EnemyKind, LaserKind, BoardColor
from data.levelInfo import LevelInfo, BoardItemInfo, LevelPortalInfo, MovingPlatformInfo, EnemyInfo, LaserInfo, \
    ForceFieldInfo, DisappearingPlatformInfo, ButtonInfo, HourglassInfo, PortalInfo
from data.positionCalculator import PositionCalculator
from data.ranges import Ranges

import numpy as np


class ThumbnailGenerator:
    COLOR_VOID = 0
    COLOR_AIR = 1
    COLOR_OBJECT = 2
    COLOR_FIELD = 3

    brushes = {
        BoardColor.Gray: {
            COLOR_VOID: QBrush(QColor(0, 0, 0)),
            COLOR_AIR: QBrush(QColor(126, 227, 255)),
            COLOR_OBJECT: QBrush(QColor(107, 107, 107)),
            COLOR_FIELD: QBrush(QColor(255, 245, 104))
        },
        BoardColor.Brown: {
            COLOR_VOID: QBrush(QColor(0, 0, 0)),
            COLOR_AIR: QBrush(QColor(255, 192, 95)),
            COLOR_OBJECT: QBrush(QColor(166, 97, 0)),
            COLOR_FIELD: QBrush(QColor(163, 255, 255))
        },
        BoardColor.Green: {
            COLOR_VOID: QBrush(QColor(0, 0, 0)),
            COLOR_AIR: QBrush(QColor(126, 227, 255)),
            COLOR_OBJECT: QBrush(QColor(31, 132, 174)),
            COLOR_FIELD: QBrush(QColor(255, 245, 104))
        },
        BoardColor.Blue: {
            COLOR_VOID: QBrush(QColor(0, 0, 0)),
            COLOR_AIR: QBrush(QColor(164, 195, 255)),
            COLOR_OBJECT: QBrush(QColor(69, 100, 241)),
            COLOR_FIELD: QBrush(QColor(255, 245, 104))
        },
        BoardColor.Violet: {
            COLOR_VOID: QBrush(QColor(0, 0, 0)),
            COLOR_AIR: QBrush(QColor(164, 195, 255)),
            COLOR_OBJECT: QBrush(QColor(147, 61, 239)),
            COLOR_FIELD: QBrush(QColor(255, 245, 104))
        },
        BoardColor.DarkBlue: {
            COLOR_VOID: QBrush(QColor(0, 0, 0)),
            COLOR_AIR: QBrush(QColor(47, 116, 213)),
            COLOR_OBJECT: QBrush(QColor(0, 46, 143)),
            COLOR_FIELD: QBrush(QColor(255, 245, 104))
        }
    }

    def __init__(self, levelInfo: LevelInfo):
        self.painter = None
        self.levelInfo = levelInfo
        self.columnCount = Ranges.width.max
        self.rowCount = Ranges.height.max
        self.cellSize = 3
        self.image = QPixmap(self.columnCount * self.cellSize, self.rowCount * self.cellSize)
        self.thumbnail = None
        self.leftMargin = 0
        self.rightMargin = 0
        self.blackBrush = QBrush(QColor(0, 0, 0))

    def generate(self):
        bothMargins = self.columnCount - self.levelInfo.general.columns
        self.leftMargin = bothMargins // 2
        self.rightMargin = bothMargins - self.leftMargin
        self.thumbnail = np.zeros((self.rowCount, self.columnCount), np.uint8)
        self.drawAir(BoardItemInfo(BoardItemKind.Air, 0, 0, self.columnCount, self.rowCount))

        for item in self.levelInfo.boardItems:
            self.drawBoardItem(item)

        self.drawLevelPortal(self.levelInfo.levelPortalIn)
        self.drawLevelPortal(self.levelInfo.levelPortalOut)

        for item in self.levelInfo.movingPlatforms:
            self.drawMovingPlatform(item)

        for item in self.levelInfo.enemies:
            if item.kind == EnemyKind.Horizontal:
                self.drawHorizontalEnemy(item)
            else:
                self.drawVerticalEnemy(item)

        for item in self.levelInfo.lasers:
            self.drawLaser(item)

        for item in self.levelInfo.forceFields:
            self.drawForceField(item)

        for item in self.levelInfo.disappearingPlatforms:
            self.drawDisappearingPlatform(item)

        for item in self.levelInfo.buttons:
            self.drawButton(item)

        for item in self.levelInfo.hourglasses:
            self.drawHourglass(item)

        for item in self.levelInfo.portals:
            self.drawPortal(item)

        self.drawMargins()

        self.painter = QPainter(self.image)
        self.painter.fillRect(0, 0, self.image.width(), self.image.height(), self.blackBrush)
        b = self.brushes[self.levelInfo.general.color]
        for y in range(self.rowCount):
            for x in range(self.columnCount):
                brush = b[int(self.thumbnail[y, x])]
                self.painter.fillRect(x * self.cellSize, y * self.cellSize, self.cellSize, self.cellSize, brush)

        self.painter.end()

    def drawBoardItem(self, item: BoardItemInfo):
        match item.kind:
            case BoardItemKind.Void:
                self.drawVoid(item)
            case BoardItemKind.Air:
                self.drawAir(item)
            case BoardItemKind.Wall1x1:
                self.drawWall1x1(item)
            case BoardItemKind.BottomWall1x2:
                self.drawBottomWall1x2(item)
            case BoardItemKind.TopWall1x2:
                self.drawTopWall1x2(item)
            case BoardItemKind.HorizontalWall:
                self.drawHorizontalWall(item)
            case BoardItemKind.VerticalWall:
                self.drawVerticalWall(item)
            case BoardItemKind.LightPlatform:
                self.drawLightPlatform(item)
            case BoardItemKind.Platform:
                self.drawPlatform(item)
            case BoardItemKind.Ladder:
                self.drawLadder(item)
            case BoardItemKind.BottomSpikes:
                self.drawBottomSpikes(item)
            case BoardItemKind.TopSpikes:
                self.drawTopSpikes(item)
            case BoardItemKind.LeftSpikes:
                self.drawLeftSpikes(item)
            case BoardItemKind.RightSpikes:
                self.drawRightSpikes(item)
            case BoardItemKind.Structure:
                self.drawStructure(item)
            case BoardItemKind.BottomWall:
                self.drawBottomWall(item)
            case BoardItemKind.LeftWall:
                self.drawLeftWall(item)
            case BoardItemKind.RightWall:
                self.drawRightWall(item)
            case BoardItemKind.TopWall:
                self.drawTopWall(item)
            case BoardItemKind.LeftTopWall:
                self.drawLeftTopWall(item)
            case BoardItemKind.RightTopWall:
                self.drawRightTopWall(item)
            case BoardItemKind.NarrowStructure:
                self.drawNarrowStructure(item)
            case BoardItemKind.TopWallWithCorners:
                self.drawTopWallWithCorners(item)
            case BoardItemKind.Chain:
                self.drawChain(item)
            case BoardItemKind.Tile:
                self.drawTile(item)
            case BoardItemKind.Pattern4x4_1:
                self.drawPattern4x4_1(item)
            case BoardItemKind.Pattern4x4_2:
                self.drawPattern4x4_2(item)
            case BoardItemKind.Pattern4x4_3:
                self.drawPattern4x4_3(item)
            case BoardItemKind.Pattern3x3_1:
                self.drawPattern3x3_1(item)
            case BoardItemKind.Pattern3x3_2:
                self.drawPattern3x3_2(item)
            case BoardItemKind.Pattern3x3_3:
                self.drawPattern3x3_3(item)

    def drawVoid(self, item: BoardItemInfo):
        self.drawArea(range(item.column, item.column + item.width), range(item.row, item.row + item.height),
                      self.COLOR_VOID)

    def drawAir(self, item: BoardItemInfo):
        self.drawArea(range(item.column, item.column + item.width), range(item.row, item.row + item.height),
                      self.COLOR_AIR)

    def drawWall1x1(self, item: BoardItemInfo):
        self.drawArea(range(item.column, item.column + item.width), range(item.row, item.row + item.height),
                      self.COLOR_OBJECT)
        self.drawArea(range(item.column, item.column + item.width),
                      range(item.row + item.height, item.row + item.height + 1), self.COLOR_AIR)

    def drawBottomWall1x2(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.COLOR_OBJECT)
            self.drawCharacter(item.column + 1, row, self.COLOR_OBJECT)

    def drawTopWall1x2(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.COLOR_OBJECT)
            self.drawCharacter(item.column + 1, row, self.COLOR_OBJECT)
        self.drawCharacter(item.column, item.row + item.height, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row + item.height, self.COLOR_AIR)

    def drawHorizontalWall(self, item: BoardItemInfo):
        for col in range(item.column, item.column + item.width):
            self.drawCharacter(col, item.row, self.COLOR_OBJECT)
            self.drawCharacter(col, item.row + 1, self.COLOR_OBJECT)
            self.drawCharacter(col, item.row + 2, self.COLOR_AIR)

    def drawVerticalWall(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.COLOR_OBJECT)
            self.drawCharacter(item.column + 1, row, self.COLOR_OBJECT)
            self.drawCharacter(item.column + 2, row, self.COLOR_OBJECT)

    def drawLightPlatform(self, item: BoardItemInfo):
        for col in range(item.column, item.column + item.width):
            self.drawCharacter(col, item.row, self.COLOR_OBJECT)
            self.drawCharacter(col, item.row + 1, self.COLOR_AIR)

    def drawPlatform(self, item: BoardItemInfo):
        for col in range(item.column, item.column + item.width):
            self.drawCharacter(col, item.row, self.COLOR_OBJECT)
            self.drawCharacter(col, item.row + 1, self.COLOR_AIR)

    def drawLadder(self, item: BoardItemInfo):
        draw = False
        for row in range(item.row, item.row + item.height):
            if draw:
                self.drawCharacter(item.column, row, self.COLOR_OBJECT)
                self.drawCharacter(item.column + 1, row, self.COLOR_OBJECT)
            else:
                self.drawCharacter(item.column, row, self.COLOR_AIR)
                self.drawCharacter(item.column + 1, row, self.COLOR_AIR)
            draw = not draw

    def drawBottomSpikes(self, item: BoardItemInfo):
        for col in range(item.column, item.column + item.width):
            self.drawCharacter(col, item.row, self.COLOR_FIELD)

    def drawTopSpikes(self, item: BoardItemInfo):
        for col in range(item.column, item.column + item.width):
            self.drawCharacter(col, item.row, self.COLOR_FIELD)

    def drawLeftSpikes(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.COLOR_FIELD)

    def drawRightSpikes(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.COLOR_FIELD)

    def drawStructure(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.COLOR_AIR)
            self.drawCharacter(item.column + 1, row, self.COLOR_AIR)
        return

    def drawBottomWall(self, item: BoardItemInfo):
        for col in range(item.column, item.column + item.width):
            self.drawCharacter(col, item.row, self.COLOR_OBJECT)
            self.drawCharacter(col, item.row + 1, self.COLOR_AIR)

    def drawLeftWall(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.COLOR_OBJECT)

    def drawRightWall(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.COLOR_OBJECT)

    def drawTopWall(self, item: BoardItemInfo):
        for col in range(item.column, item.column + item.width):
            self.drawCharacter(col, item.row, self.COLOR_OBJECT)
            self.drawCharacter(col, item.row + 1, self.COLOR_AIR)

    def drawLeftTopWall(self, item: BoardItemInfo):
        self.drawCharacter(item.column, item.row, self.COLOR_OBJECT)
        self.drawCharacter(item.column, item.row + 1, self.COLOR_AIR)

    def drawRightTopWall(self, item: BoardItemInfo):
        self.drawCharacter(item.column, item.row, self.COLOR_OBJECT)
        self.drawCharacter(item.column, item.row + 1, self.COLOR_AIR)

    def drawNarrowStructure(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.COLOR_AIR)

    def drawChain(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.COLOR_AIR)

    def drawTile(self, item: BoardItemInfo):
        self.drawCharacter(item.column, item.row, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row, self.COLOR_AIR)

    def drawTopWallWithCorners(self, item: BoardItemInfo):
        self.drawTopWall(item)

    def drawPattern4x4_1(self, item: BoardItemInfo):
        self.drawCharacter(item.column, item.row, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row, self.COLOR_AIR)
        self.drawCharacter(item.column + 2, item.row, self.COLOR_AIR)
        self.drawCharacter(item.column + 3, item.row, self.COLOR_AIR)
        self.drawCharacter(item.column, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 2, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 3, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column, item.row + 2, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row + 2, self.COLOR_AIR)
        self.drawCharacter(item.column + 2, item.row + 2, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row + 3, self.COLOR_AIR)
        self.drawCharacter(item.column + 3, item.row + 3, self.COLOR_AIR)

    def drawPattern4x4_2(self, item: BoardItemInfo):
        self.drawCharacter(item.column + 1, item.row, self.COLOR_AIR)
        self.drawCharacter(item.column + 2, item.row, self.COLOR_AIR)
        self.drawCharacter(item.column, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 2, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 3, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row + 2, self.COLOR_AIR)
        self.drawCharacter(item.column + 2, item.row + 2, self.COLOR_AIR)
        self.drawCharacter(item.column + 3, item.row + 2, self.COLOR_AIR)
        self.drawCharacter(item.column + 2, item.row + 3, self.COLOR_AIR)

    def drawPattern4x4_3(self, item: BoardItemInfo):
        self.drawCharacter(item.column + 1, item.row, self.COLOR_AIR)
        self.drawCharacter(item.column + 2, item.row, self.COLOR_AIR)
        self.drawCharacter(item.column, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 2, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 3, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column, item.row + 2, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row + 2, self.COLOR_AIR)
        self.drawCharacter(item.column + 2, item.row + 2, self.COLOR_AIR)
        self.drawCharacter(item.column + 3, item.row + 2, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row + 3, self.COLOR_AIR)
        self.drawCharacter(item.column + 2, item.row + 3, self.COLOR_AIR)

    def drawPattern3x3_1(self, item: BoardItemInfo):
        self.drawCharacter(item.column, item.row, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row, self.COLOR_AIR)
        self.drawCharacter(item.column, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 2, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column, item.row + 2, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row + 2, self.COLOR_AIR)

    def drawPattern3x3_2(self, item: BoardItemInfo):
        self.drawCharacter(item.column, item.row, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row, self.COLOR_AIR)
        self.drawCharacter(item.column, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 2, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row + 2, self.COLOR_AIR)
        self.drawCharacter(item.column + 2, item.row + 2, self.COLOR_AIR)

    def drawPattern3x3_3(self, item: BoardItemInfo):
        self.drawCharacter(item.column, item.row, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row, self.COLOR_AIR)
        self.drawCharacter(item.column, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 2, item.row + 1, self.COLOR_AIR)
        self.drawCharacter(item.column + 1, item.row + 2, self.COLOR_AIR)
        self.drawCharacter(item.column + 2, item.row + 2, self.COLOR_AIR)

    def drawLevelPortal(self, item: LevelPortalInfo):
        self.drawCharacter(item.column, item.row, self.COLOR_OBJECT)
        self.drawCharacter(item.column + 1, item.row, self.COLOR_OBJECT)
        self.drawCharacter(item.column + 2, item.row, self.COLOR_OBJECT)
        self.drawCharacter(item.column + 3, item.row, self.COLOR_OBJECT)
        draw = True
        for row in range(item.row + 1, item.row + item.height):
            if draw:
                self.drawCharacter(item.column + 1, row, self.COLOR_FIELD)
                self.drawCharacter(item.column + 2, row, self.COLOR_FIELD)
            draw = not draw

    def drawMovingPlatform(self, platform: MovingPlatformInfo):
        position = PositionCalculator.calculate(0, platform.moveOffset, platform.range)
        position += platform.xPos
        position //= 4
        self.drawCharacter(position, platform.row, self.COLOR_OBJECT)
        position += 1
        for i in range(1, platform.width - 1):
            self.drawCharacter(position, platform.row, self.COLOR_OBJECT)
            position += 1
        self.drawCharacter(position, platform.row, self.COLOR_OBJECT)

    def drawHorizontalEnemy(self, enemy: EnemyInfo):
        position = PositionCalculator.calculate(0, enemy.moveOffset, enemy.range)
        position += enemy.xPos
        position //= 4
        yPos = enemy.yPos // 8
        self.drawCharacter(position, yPos, self.COLOR_FIELD)
        self.drawCharacter(position + 1, yPos, self.COLOR_FIELD)
        self.drawCharacter(position, yPos + 1, self.COLOR_FIELD)
        self.drawCharacter(position + 1, yPos + 1, self.COLOR_FIELD)

    def drawVerticalEnemy(self, enemy: EnemyInfo):
        position = PositionCalculator.calculate(0, enemy.moveOffset, enemy.range) * 2
        position += enemy.yPos
        position //= 8
        xPos = enemy.xPos // 4
        self.drawCharacter(xPos, position, self.COLOR_FIELD)
        self.drawCharacter(xPos + 1, position, self.COLOR_FIELD)
        self.drawCharacter(xPos, position + 1, self.COLOR_FIELD)
        self.drawCharacter(xPos + 1, position + 1, self.COLOR_FIELD)

    def drawLaser(self, laser: LaserInfo):
        if laser.kind == LaserKind.LeftToRight:
            self.drawCharacter(laser.column, laser.row, self.COLOR_OBJECT)
            draw = True
            for col in range(laser.column + 1, laser.column + laser.width):
                if draw:
                    self.drawCharacter(col, laser.row, self.COLOR_FIELD)
                draw = not draw
        else:
            self.drawCharacter(laser.column + laser.width - 1, laser.row, self.COLOR_OBJECT)
            draw = True
            for col in range(laser.column, laser.column + laser.width - 1):
                if draw:
                    self.drawCharacter(col, laser.row, self.COLOR_FIELD)
                draw = not draw

    def drawForceField(self, item: ForceFieldInfo):
        self.drawCharacter(item.column, item.row, self.COLOR_OBJECT)
        for row in range(item.row + 1, item.row + item.height):
            self.drawCharacter(item.column, row, self.COLOR_FIELD)

    def drawDisappearingPlatform(self, item: DisappearingPlatformInfo):
        self.drawCharacter(item.column, item.row, self.COLOR_OBJECT)
        i = 1
        while i < item.width - 1:
            self.drawCharacter(item.column + i, item.row, self.COLOR_OBJECT)
            i += 1
        self.drawCharacter(item.column + i, item.row, self.COLOR_OBJECT)

    def drawButton(self, button: ButtonInfo):
        self.drawCharacter(button.column, button.row, self.COLOR_OBJECT)
        self.drawCharacter(button.column + 1, button.row, self.COLOR_OBJECT)
        self.drawCharacter(button.column, button.row + 1, self.COLOR_OBJECT)
        self.drawCharacter(button.column + 1, button.row + 1, self.COLOR_OBJECT)

    def drawHourglass(self, hourglass: HourglassInfo):
        self.drawCharacter(hourglass.column, hourglass.row, self.COLOR_OBJECT)
        self.drawCharacter(hourglass.column + 1, hourglass.row, self.COLOR_OBJECT)
        self.drawCharacter(hourglass.column, hourglass.row + 1, self.COLOR_OBJECT)
        self.drawCharacter(hourglass.column + 1, hourglass.row + 1, self.COLOR_OBJECT)

    def drawPortal(self, portal: PortalInfo):
        for row in range(portal.row, portal.row + portal.height):
            self.drawCharacter(portal.column, row, self.COLOR_FIELD)

    def drawMargins(self):
        if self.leftMargin > 0:
            self.drawArea(range(-self.leftMargin, 0), range(self.columnCount), self.COLOR_VOID)
        if self.rightMargin > 0:
            self.drawArea(
                range(self.columnCount - self.leftMargin - self.rightMargin, self.columnCount - self.leftMargin),
                range(self.columnCount), self.COLOR_VOID)

    def drawArea(self, xRange, yRange, color):
        for row in yRange:
            for col in xRange:
                self.drawCharacter(col, row, color)

    def drawCharacter(self, x, y, color):
        x += self.leftMargin
        if (x >= 0) and (x < self.columnCount) and (y >= 0) and (y < self.rowCount):
            self.thumbnail[y, x] = color
