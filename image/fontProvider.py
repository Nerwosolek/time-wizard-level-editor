from enum import Enum

from PyQt6.QtGui import QImage

from data.enums import BoardColor


class ZoomKind(int, Enum):
    Zoom2 = 2
    Zoom3 = 3
    Zoom4 = 4


class FontProvider:

    fontNames = {
        BoardColor.Blue: ':/fonts/font-blue',
        BoardColor.Brown: ':/fonts/font-brown',
        BoardColor.DarkBlue: ':/fonts/font-dark-blue',
        BoardColor.Gray: ':/fonts/font-gray',
        BoardColor.Green: ':/fonts/font-green',
        BoardColor.Violet: ':/fonts/font-violet'
    }

    def __init__(self, color: BoardColor, zoom: ZoomKind):
        font = QImage(self.fontNames[color])
        if zoom == ZoomKind.Zoom4:
            size = 32
            font = font.scaled(font.width() * 2, font.height() * 2)
        elif zoom == ZoomKind.Zoom3:
            size = 24
            font = font.scaled(font.width() * 3 // 2, font.height() * 3 // 2)
        else:
            size = 16

        self.characters = []

        for row in range(8):
            y = row * size
            for col in range(32):
                x = col * size
                self.characters.append(font.copy(x, y, size, size))
