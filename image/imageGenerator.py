from PyQt6.QtCore import QPoint, QRect
from PyQt6.QtGui import QPixmap, QColor, QPainter, QBrush, QPen, QFont

from data.enums import BoardColor, BoardItemKind, EnemyKind, LaserKind, ActivationMethodKind, ButtonKind, \
    PortalEntryKind, ResizeObjectMode
from data.levelInfo import LevelInfo, BoardItemInfo, LevelPortalInfo, MovingPlatformInfo, EnemyInfo, LaserInfo, \
    ForceFieldInfo, DisappearingPlatformInfo, ButtonInfo, HourglassInfo, PortalInfo, ItemInfo
from data.positionCalculator import PositionCalculator
from data.ranges import Ranges
from data.selectionManager import SelectionManager
from image.fontProvider import FontProvider, ZoomKind


class ImageGenerator:
    BOARD_ITEM_AIR = 0x00
    BOARD_ITEM_WALL_1X1 = 0x01
    BOARD_ITEM_BOTTOM_WALL_1X2_L = 0x02
    BOARD_ITEM_BOTTOM_WALL_1X2_R = 0x03
    BOARD_ITEM_TOP_WALL_1X2_L = 0x04
    BOARD_ITEM_TOP_WALL_1X2_R = 0x05
    BOARD_ITEM_WALL_2x3_LT = 0x06
    BOARD_ITEM_WALL_2x3_CT = 0x07
    BOARD_ITEM_WALL_2x3_RT = 0x08
    BOARD_ITEM_WALL_2x3_LB = 0x09
    BOARD_ITEM_WALL_2x3_CB = 0x0A
    BOARD_ITEM_WALL_2x3_RB = 0x0B
    BOARD_ITEM_PLATFORM_L = 0x0C
    BOARD_ITEM_PLATFORM_C = 0x0D
    BOARD_ITEM_PLATFORM_R = 0x0E
    BOARD_ITEM_MOVING_PLATFORM_C = 0x13
    BOARD_ITEM_MOVING_PLATFORM_L0 = 0x14
    BOARD_ITEM_MOVING_PLATFORM_R0 = 0x18
    BOARD_ITEM_BOTTOM_SPIKES = 0x1C
    BOARD_ITEM_TOP_SPIKES = 0x1D
    BOARD_ITEM_RIGHT_SPIKES = 0x1E
    BOARD_ITEM_LEFT_SPIKES = 0x1F
    BOARD_ITEM_SHADOW = 0x20
    BOARD_ITEM_FORCE_FIELD_SOURCE = 0x21
    BOARD_ITEM_FORCE_FIELD = 0x22
    BOARD_ITEM_PORTAL_TOP = 0x23
    BOARD_ITEM_PORTAL_BOTTOM = 0x24
    BOARD_ITEM_BUTTON_UP_LT = 0x26
    BOARD_ITEM_BUTTON_UP_RT = 0x27
    BOARD_ITEM_BUTTON_UP_LB = 0x28
    BOARD_ITEM_BUTTON_UP_RB = 0x29
    BOARD_ITEM_ACTIVABLE_PLATFORM_L = 0x2C
    BOARD_ITEM_ACTIVABLE_PLATFORM_C = 0x2D
    BOARD_ITEM_ACTIVABLE_PLATFORM_R = 0x2E
    BOARD_ITEM_LADDER_L = 0x2F
    BOARD_ITEM_LADDER_R = 0x30
    BOARD_ITEM_VERTICAL_ENEMY_C0R0F0 = 0x31
    BOARD_ITEM_VERTICAL_ENEMY_C1R0F0 = 0x32
    BOARD_ITEM_VERTICAL_ENEMY_C0R1F0 = 0x33
    BOARD_ITEM_VERTICAL_ENEMY_C1R1F0 = 0x34
    BOARD_ITEM_HORIZONTAL_ENEMY_C0R0F0 = 0x35
    BOARD_ITEM_HORIZONTAL_ENEMY_C1R0F0 = 0x36
    BOARD_ITEM_HORIZONTAL_ENEMY_C0R1F0 = 0x37
    BOARD_ITEM_HORIZONTAL_ENEMY_C1R1F0 = 0x38
    BOARD_ITEM_BOTTOM_WALL = 0x49
    BOARD_ITEM_TOP_WALL_SHORT = 0x4A
    BOARD_ITEM_TOP_WALL_LONG = 0x4B
    BOARD_ITEM_LEFT_WALL = 0x4C
    BOARD_ITEM_RIGHT_WALL = 0x4D
    BOARD_ITEM_LEFT_TOP_WALL = 0x4E
    BOARD_ITEM_HOURGLASS_LT = 0x5D
    BOARD_ITEM_HOURGLASS_RT = 0x5E
    BOARD_ITEM_HOURGLASS_LB = 0x5F
    BOARD_ITEM_HOURGLASS_RB = 0x60
    BOARD_ITEM_LASER_LEFT = 0x61
    BOARD_ITEM_LASER_RIGHT = 0x62
    BOARD_ITEM_LASER = 0x63
    BOARD_ITEM_LIGHT_PLATFORM_L = 0x64
    BOARD_ITEM_LIGHT_PLATFORM_C = 0x65
    BOARD_ITEM_LIGHT_PLATFORM_R = 0x66
    TIME_PORTAL_LEFT = 0x68
    TIME_PORTAL_CENTER = 0x69
    TIME_PORTAL_RIGHT = 0x6A
    BOARD_ITEM_TILE = 0x6E
    BOARD_ITEM_STRUCTURE_L = 0x73
    BOARD_ITEM_STRUCTURE_R = 0x74
    BOARD_ITEM_NARROW_STRUCTURE = 0x75
    BOARD_ITEM_CHAIN = 0x76
    BOARD_ITEM_VOID = 0x77
    BOARD_ITEM_TIME_RESIST = 0x80
    TIME_PORTAL_FIELD = 0xE7

    def __init__(self, levelInfo: LevelInfo, selectionManager: SelectionManager, zoom: ZoomKind):
        self.font = None
        self.painter = None
        self.transparentColor = None
        self.hoverItem = None
        self.levelTime = 0
        self.globalTime = 0
        self.levelInfo = levelInfo
        self.selectionManager = selectionManager
        self.columnCount = Ranges.width.max
        self.rowCount = Ranges.height.max
        if zoom == ZoomKind.Zoom4:
            self.xZoomScale = 8
            self.yZoomScale = 4
            self.cellSize = 32
            self.annotationFont = QFont("Arial", 12)
        elif zoom == ZoomKind.Zoom3:
            self.xZoomScale = 6
            self.yZoomScale = 3
            self.cellSize = 24
            self.annotationFont = QFont("Arial", 12)
        else:
            self.xZoomScale = 4
            self.yZoomScale = 2
            self.cellSize = 16
            self.annotationFont = QFont("Arial", 9)
        self.image = QPixmap(self.columnCount * self.cellSize + 1, self.rowCount * self.cellSize + 1)

        self.fonts = dict()
        for color in BoardColor:
            self.fonts[color] = FontProvider(color, zoom)

        self.lightGrayBrush = QBrush(QColor(200, 200, 200))
        self.grayPen = QPen(QColor(150, 150, 150, 128))
        self.annotationPen = QPen(QColor(128, 255, 128))
        self.bluePen = QPen(QColor(0, 0, 255))
        self.blackPen = QPen(QColor(0, 0, 0, 128))
        self.blackPen3 = QPen(QColor(0, 0, 0, 128), 3)
        self.selectionPen = QPen(QColor(255, 255, 0), 3)
        self.relativePen = QPen(QColor(255, 128, 0), 2)
        self.hoverPen = QPen(QColor(255, 255, 0), 1)
        self.marginBrush = QBrush(QColor(255, 255, 255, 192))

    def imagePosToGamePos(self, point: QPoint):
        return QPoint(point.x() // self.xZoomScale, point.y() // self.yZoomScale)

    def scaleRect(self, rect: QRect):
        return QRect(rect.x() * self.xZoomScale, rect.y() * self.yZoomScale, rect.width() * self.xZoomScale,
                     rect.height() * self.yZoomScale)

    def objectAt(self, item: ItemInfo, pos: QPoint):
        rect = item.getRect()
        rect = QRect(rect.x() * self.xZoomScale, rect.y() * self.yZoomScale, rect.width() * self.xZoomScale,
                     rect.height() * self.yZoomScale)
        if rect.contains(pos):
            return item
        else:
            return None

    def objectAtList(self, items: list, pos: QPoint):
        for item in reversed(items):
            item = self.objectAt(item, pos)
            if not (item is None):
                return item
        return None

    def getObjectAt(self, pos: QPoint):
        item = self.objectAtList(self.levelInfo.portals, pos)
        if not (item is None):
            return item

        item = self.objectAtList(self.levelInfo.hourglasses, pos)
        if not (item is None):
            return item

        item = self.objectAtList(self.levelInfo.buttons, pos)
        if not (item is None):
            return item

        item = self.objectAtList(self.levelInfo.disappearingPlatforms, pos)
        if not (item is None):
            return item

        item = self.objectAtList(self.levelInfo.forceFields, pos)
        if not (item is None):
            return item

        item = self.objectAtList(self.levelInfo.lasers, pos)
        if not (item is None):
            return item

        item = self.objectAtList(self.levelInfo.enemies, pos)
        if not (item is None):
            return item

        item = self.objectAtList(self.levelInfo.movingPlatforms, pos)
        if not (item is None):
            return item

        item = self.objectAtList(self.levelInfo.boardItems, pos)
        if not (item is None):
            return item

        item = self.objectAt(self.levelInfo.levelPortalOut, pos)
        if not (item is None):
            return item

        item = self.objectAt(self.levelInfo.levelPortalIn, pos)
        if not (item is None):
            return item

        return None

    def getResizeModeObjectAt(self, pos: QPoint):
        if self.hoverItem is None:
            return ResizeObjectMode.NoResize

        rect = self.hoverItem.getRect()
        rect = QRect(rect.x() * self.xZoomScale, rect.y() * self.yZoomScale, rect.width() * self.xZoomScale,
                     rect.height() * self.yZoomScale)
        if not (pos in rect):
            return ResizeObjectMode.NoResize

        epsilon = 4
        isRight = self.hoverItem.horizontalResize() and (abs(pos.x() - rect.right()) < epsilon)
        isBottom = self.hoverItem.verticalResize() and (abs(pos.y() - rect.bottom()) < epsilon)
        if isRight and isBottom:
            return ResizeObjectMode.ResizeOnRightBottom
        elif isRight:
            return ResizeObjectMode.ResizeOnRight
        elif isBottom:
            return ResizeObjectMode.ResizeOnBottom
        else:
            return ResizeObjectMode.NoResize

    def generate(self):
        self.painter = QPainter(self.image)
        self.painter.fillRect(0, 0, self.image.width(), self.image.height(), self.lightGrayBrush)
        self.painter.setFont(self.annotationFont)

        self.font = self.fonts[self.levelInfo.general.color]
        self.transparentColor = QColor(self.font.characters[self.BOARD_ITEM_AIR].pixel(0, 0))
        self.transparentColor.setAlpha(192)

        self.drawAir(BoardItemInfo(BoardItemKind.Air, 0, 0, self.columnCount, self.rowCount))

        for item in self.levelInfo.boardItems:
            self.drawBoardItem(item)

        self.drawLevelPortalIn(self.levelInfo.levelPortalIn)
        self.drawLevelPortalOut(self.levelInfo.levelPortalOut)

        for item in self.levelInfo.movingPlatforms:
            self.drawMovingPlatform(item)

        for item in self.levelInfo.enemies:
            if item.kind == EnemyKind.Horizontal:
                self.drawHorizontalEnemy(item)
            else:
                self.drawVerticalEnemy(item)

        for item in self.levelInfo.lasers:
            self.drawLaser(item)

        for item in self.levelInfo.forceFields:
            self.drawForceField(item)

        for item in self.levelInfo.disappearingPlatforms:
            self.drawDisappearingPlatform(item)

        for item in self.levelInfo.buttons:
            self.drawButton(item)

        for item in self.levelInfo.hourglasses:
            self.drawHourglass(item)

        for item in self.levelInfo.portals:
            self.drawPortal(item)

        self.drawGrid(self.levelInfo.general.columns)
        self.drawSelection(self.selectionManager.selected)
        self.drawFrame(self.hoverItem, self.hoverPen)
        self.painter.end()

    def drawGrid(self, columns):
        self.painter.setPen(self.grayPen)
        for column in range(self.columnCount + 1):
            self.painter.drawLine(column * self.cellSize, 0, column * self.cellSize, self.image.height() - 1)
        for row in range(self.rowCount + 1):
            self.painter.drawLine(0, row * self.cellSize, self.image.width() - 1, row * self.cellSize)

        if columns < self.columnCount:
            self.painter.fillRect(columns * self.cellSize, 0, (self.columnCount - columns) * self.cellSize,
                                  self.rowCount * self.cellSize, self.marginBrush)
        self.painter.setPen(self.bluePen)
        self.painter.drawLine(columns * self.cellSize, 0, columns * self.cellSize, self.image.height() - 1)

    def drawSelection(self, item):
        if item is None:
            return

        self.drawFrame(self.levelInfo.getRelativePortal(item), self.relativePen)
        for i in self.levelInfo.getObjectsWithTheState(item):
            self.drawFrame(i, self.relativePen)

        self.drawFrame(item, self.selectionPen)

    def drawFrame(self, item: ItemInfo, pen: QPen):
        if item is None:
            return

        rect = item.getRect()
        if rect is None:
            return

        self.painter.setPen(pen)
        self.painter.drawRect(rect.x() * self.xZoomScale, rect.y() * self.yZoomScale,
                              rect.width() * self.xZoomScale, rect.height() * self.yZoomScale)

    def drawBoardItem(self, item: BoardItemInfo):
        match item.kind:
            case BoardItemKind.Void:
                self.drawVoid(item)
            case BoardItemKind.Air:
                self.drawAir(item)
            case BoardItemKind.Wall1x1:
                self.drawWall1x1(item)
            case BoardItemKind.BottomWall1x2:
                self.drawBottomWall1x2(item)
            case BoardItemKind.TopWall1x2:
                self.drawTopWall1x2(item)
            case BoardItemKind.HorizontalWall:
                self.drawHorizontalWall(item)
            case BoardItemKind.VerticalWall:
                self.drawVerticalWall(item)
            case BoardItemKind.LightPlatform:
                self.drawLightPlatform(item)
            case BoardItemKind.Platform:
                self.drawPlatform(item)
            case BoardItemKind.Ladder:
                self.drawLadder(item)
            case BoardItemKind.BottomSpikes:
                self.drawBottomSpikes(item)
            case BoardItemKind.TopSpikes:
                self.drawTopSpikes(item)
            case BoardItemKind.LeftSpikes:
                self.drawLeftSpikes(item)
            case BoardItemKind.RightSpikes:
                self.drawRightSpikes(item)
            case BoardItemKind.Structure:
                self.drawStructure(item)
            case BoardItemKind.BottomWall:
                self.drawBottomWall(item)
            case BoardItemKind.LeftWall:
                self.drawLeftWall(item)
            case BoardItemKind.RightWall:
                self.drawRightWall(item)
            case BoardItemKind.TopWall:
                self.drawTopWall(item)
            case BoardItemKind.LeftTopWall:
                self.drawLeftTopWall(item)
            case BoardItemKind.RightTopWall:
                self.drawRightTopWall(item)
            case BoardItemKind.NarrowStructure:
                self.drawNarrowStructure(item)
            case BoardItemKind.TopWallWithCorners:
                self.drawTopWallWithCorners(item)
            case BoardItemKind.Chain:
                self.drawChain(item)
            case BoardItemKind.Tile:
                self.drawTile(item)
            case BoardItemKind.Pattern4x4_1:
                self.drawPattern4x4_1(item)
            case BoardItemKind.Pattern4x4_2:
                self.drawPattern4x4_2(item)
            case BoardItemKind.Pattern4x4_3:
                self.drawPattern4x4_3(item)
            case BoardItemKind.Pattern3x3_1:
                self.drawPattern3x3_1(item)
            case BoardItemKind.Pattern3x3_2:
                self.drawPattern3x3_2(item)
            case BoardItemKind.Pattern3x3_3:
                self.drawPattern3x3_3(item)

    def drawVoid(self, item: BoardItemInfo):
        self.drawArea(range(item.column, item.column + item.width), range(item.row, item.row + item.height),
                      self.BOARD_ITEM_VOID)

    def drawAir(self, item: BoardItemInfo):
        self.drawArea(range(item.column, item.column + item.width), range(item.row, item.row + item.height),
                      self.BOARD_ITEM_AIR)

    def drawWall1x1(self, item: BoardItemInfo):
        self.drawArea(range(item.column, item.column + item.width), range(item.row, item.row + item.height),
                      self.BOARD_ITEM_WALL_1X1)
        self.drawArea(range(item.column, item.column + item.width),
                      range(item.row + item.height, item.row + item.height + 1),
                      self.BOARD_ITEM_SHADOW)

    def drawBottomWall1x2(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.BOARD_ITEM_BOTTOM_WALL_1X2_L)
            self.drawCharacter(item.column + 1, row, self.BOARD_ITEM_BOTTOM_WALL_1X2_R)

    def drawTopWall1x2(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.BOARD_ITEM_TOP_WALL_1X2_L)
            self.drawCharacter(item.column + 1, row, self.BOARD_ITEM_TOP_WALL_1X2_R)
        self.drawCharacter(item.column, item.row + item.height, self.BOARD_ITEM_SHADOW)
        self.drawCharacter(item.column + 1, item.row + item.height, self.BOARD_ITEM_SHADOW)

    def drawHorizontalWall(self, item: BoardItemInfo):
        c = 0
        for col in range(item.column, item.column + item.width):
            self.drawCharacter(col, item.row, self.BOARD_ITEM_WALL_2x3_LT + c)
            self.drawCharacter(col, item.row + 1, self.BOARD_ITEM_WALL_2x3_LB + c)
            if item.row < self.rowCount - 2:
                self.drawCharacter(col, item.row + 2, self.BOARD_ITEM_SHADOW)
            c += 1
            if c == 3:
                c = 0

    def drawVerticalWall(self, item: BoardItemInfo):
        top = True
        for row in range(item.row, item.row + item.height):
            if top:
                self.drawCharacter(item.column, row, self.BOARD_ITEM_WALL_2x3_LT)
                self.drawCharacter(item.column + 1, row, self.BOARD_ITEM_WALL_2x3_CT)
                self.drawCharacter(item.column + 2, row, self.BOARD_ITEM_WALL_2x3_RT)
            else:
                self.drawCharacter(item.column, row, self.BOARD_ITEM_WALL_2x3_LB)
                self.drawCharacter(item.column + 1, row, self.BOARD_ITEM_WALL_2x3_CB)
                self.drawCharacter(item.column + 2, row, self.BOARD_ITEM_WALL_2x3_RB)
            top = not top

    def drawLightPlatform(self, item: BoardItemInfo):
        c = 0
        for col in range(item.column, item.column + item.width):
            self.drawCharacter(col, item.row, self.BOARD_ITEM_LIGHT_PLATFORM_L + c)
            if item.row < self.rowCount - 1:
                self.drawCharacter(col, item.row + 1, self.BOARD_ITEM_SHADOW)
            c += 1
            if c == 3:
                c = 0

    def drawPlatform(self, item: BoardItemInfo):
        self.drawCharacter(item.column, item.row, self.BOARD_ITEM_PLATFORM_L)
        self.drawCharacter(item.column, item.row + 1, self.BOARD_ITEM_SHADOW)
        i = 1
        while i < item.width - 1:
            self.drawCharacter(item.column + i, item.row, self.BOARD_ITEM_PLATFORM_C)
            self.drawCharacter(item.column + i, item.row + 1, self.BOARD_ITEM_SHADOW)
            i += 1
        self.drawCharacter(item.column + i, item.row, self.BOARD_ITEM_PLATFORM_R)
        self.drawCharacter(item.column + i, item.row + 1, self.BOARD_ITEM_SHADOW)

    def drawLadder(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.BOARD_ITEM_LADDER_L)
            self.drawCharacter(item.column + 1, row, self.BOARD_ITEM_LADDER_R)

    def drawBottomSpikes(self, item: BoardItemInfo):
        for col in range(item.column, item.column + item.width):
            self.drawCharacter(col, item.row, self.BOARD_ITEM_BOTTOM_SPIKES)

    def drawTopSpikes(self, item: BoardItemInfo):
        for col in range(item.column, item.column + item.width):
            self.drawCharacter(col, item.row, self.BOARD_ITEM_TOP_SPIKES)

    def drawLeftSpikes(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.BOARD_ITEM_LEFT_SPIKES)

    def drawRightSpikes(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.BOARD_ITEM_RIGHT_SPIKES)

    def drawStructure(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.BOARD_ITEM_STRUCTURE_L)
            self.drawCharacter(item.column + 1, row, self.BOARD_ITEM_STRUCTURE_R)

    def drawBottomWall(self, item: BoardItemInfo):
        for col in range(item.column, item.column + item.width):
            self.drawCharacter(col, item.row, self.BOARD_ITEM_BOTTOM_WALL)
            if item.row < self.rowCount - 1:
                self.drawCharacter(col, item.row + 1, self.BOARD_ITEM_SHADOW)

    def drawLeftWall(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.BOARD_ITEM_LEFT_WALL)

    def drawRightWall(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.BOARD_ITEM_RIGHT_WALL)

    def drawLeftTopWall(self, item: BoardItemInfo):
        self.drawCharacter(item.column, item.row, self.BOARD_ITEM_LEFT_TOP_WALL)
        if item.row < self.rowCount - 1:
            self.drawCharacter(item.column, item.row + 1, self.BOARD_ITEM_SHADOW)

    def drawRightTopWall(self, item: BoardItemInfo):
        self.drawCharacter(item.column, item.row, self.BOARD_ITEM_TOP_WALL_LONG)
        if item.row < self.rowCount - 1:
            self.drawCharacter(item.column, item.row + 1, self.BOARD_ITEM_SHADOW)

    def drawTopWall(self, item: BoardItemInfo):
        short = False
        for col in range(item.column, item.column + item.width):
            if short:
                self.drawCharacter(col, item.row, self.BOARD_ITEM_TOP_WALL_SHORT)
            else:
                self.drawCharacter(col, item.row, self.BOARD_ITEM_TOP_WALL_LONG)
            if item.row < self.rowCount - 1:
                self.drawCharacter(col, item.row + 1, self.BOARD_ITEM_SHADOW)
            short = not short

    def drawNarrowStructure(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.BOARD_ITEM_NARROW_STRUCTURE)

    def drawTopWallWithCorners(self, item: BoardItemInfo):
        self.drawTopWall(item)
        self.drawRightTopWall(item)
        item.column += item.width - 1
        self.drawLeftTopWall(item)
        item.column -= item.width - 1

    def drawChain(self, item: BoardItemInfo):
        for row in range(item.row, item.row + item.height):
            self.drawCharacter(item.column, row, self.BOARD_ITEM_CHAIN)

    def drawTile(self, item: BoardItemInfo):
        self.drawCharacter(item.column, item.row, self.BOARD_ITEM_TILE)
        self.drawCharacter(item.column + 1, item.row, self.BOARD_ITEM_SHADOW)

    def drawPattern4x4_1(self, item: BoardItemInfo):
        self.drawCharacter(item.column, item.row, 0x52)
        self.drawCharacter(item.column + 1, item.row, 0x53)
        self.drawCharacter(item.column + 2, item.row, 0x56)
        self.drawCharacter(item.column + 3, item.row, 0x57)
        self.drawCharacter(item.column, item.row + 1, 0x53)
        self.drawCharacter(item.column + 1, item.row + 1, 0x56)
        self.drawCharacter(item.column + 2, item.row + 1, 0x4F)
        self.drawCharacter(item.column + 3, item.row + 1, 0x50)
        self.drawCharacter(item.column, item.row + 2, 0x57)
        self.drawCharacter(item.column + 1, item.row + 2, 0x4F)
        self.drawCharacter(item.column + 2, item.row + 2, 0x55)
        self.drawCharacter(item.column + 1, item.row + 3, 0x54)
        self.drawCharacter(item.column + 3, item.row + 3, 0x55)

    def drawPattern4x4_2(self, item: BoardItemInfo):
        self.drawCharacter(item.column + 1, item.row, 0x56)
        self.drawCharacter(item.column + 2, item.row, 0x4F)
        self.drawCharacter(item.column, item.row + 1, 0x52)
        self.drawCharacter(item.column + 1, item.row + 1, 0x4F)
        self.drawCharacter(item.column + 2, item.row + 1, 0x51)
        self.drawCharacter(item.column + 3, item.row + 1, 0x50)
        self.drawCharacter(item.column + 1, item.row + 2, 0x56)
        self.drawCharacter(item.column + 2, item.row + 2, 0x4F)
        self.drawCharacter(item.column + 3, item.row + 2, 0x55)
        self.drawCharacter(item.column + 2, item.row + 3, 0x51)

    def drawPattern4x4_3(self, item: BoardItemInfo):
        self.drawCharacter(item.column + 1, item.row, 0x53)
        self.drawCharacter(item.column + 2, item.row, 0x56)
        self.drawCharacter(item.column, item.row + 1, 0x56)
        self.drawCharacter(item.column + 1, item.row + 1, 0x4F)
        self.drawCharacter(item.column + 2, item.row + 1, 0x55)
        self.drawCharacter(item.column + 3, item.row + 1, 0x53)
        self.drawCharacter(item.column, item.row + 2, 0x50)
        self.drawCharacter(item.column + 1, item.row + 2, 0x56)
        self.drawCharacter(item.column + 2, item.row + 2, 0x4F)
        self.drawCharacter(item.column + 3, item.row + 2, 0x52)
        self.drawCharacter(item.column + 1, item.row + 3, 0x56)
        self.drawCharacter(item.column + 2, item.row + 3, 0x50)

    def drawPattern3x3_1(self, item: BoardItemInfo):
        self.drawCharacter(item.column, item.row, 0x4F)
        self.drawCharacter(item.column + 1, item.row, 0x52)
        self.drawCharacter(item.column, item.row + 1, 0x53)
        self.drawCharacter(item.column + 1, item.row + 1, 0x4F)
        self.drawCharacter(item.column + 2, item.row + 1, 0x56)
        self.drawCharacter(item.column, item.row + 2, 0x52)
        self.drawCharacter(item.column + 1, item.row + 2, 0x55)

    def drawPattern3x3_2(self, item: BoardItemInfo):
        self.drawCharacter(item.column, item.row, 0x52)
        self.drawCharacter(item.column + 1, item.row, 0x56)
        self.drawCharacter(item.column, item.row + 1, 0x53)
        self.drawCharacter(item.column + 1, item.row + 1, 0x52)
        self.drawCharacter(item.column + 2, item.row + 1, 0x4F)
        self.drawCharacter(item.column + 1, item.row + 2, 0x4F)
        self.drawCharacter(item.column + 2, item.row + 2, 0x55)

    def drawPattern3x3_3(self, item: BoardItemInfo):
        self.drawCharacter(item.column, item.row, 0x52)
        self.drawCharacter(item.column + 1, item.row, 0x53)
        self.drawCharacter(item.column, item.row + 1, 0x54)
        self.drawCharacter(item.column + 1, item.row + 1, 0x4F)
        self.drawCharacter(item.column + 2, item.row + 1, 0x4F)
        self.drawCharacter(item.column + 1, item.row + 2, 0x55)
        self.drawCharacter(item.column + 2, item.row + 2, 0x56)

    def drawLevelPortalIn(self, item: LevelPortalInfo):
        self.drawLevelPortal(item)
        self.drawText(item.column * self.cellSize + 1, (item.row + 1) * self.cellSize, 'IN', self.annotationPen)

    def drawLevelPortalOut(self, item: LevelPortalInfo):
        self.drawLevelPortal(item)
        self.drawText(item.column * self.cellSize + 1, (item.row + 1) * self.cellSize, 'OUT', self.annotationPen)

    def drawLevelPortal(self, item: LevelPortalInfo):
        self.drawCharacter(item.column, item.row, self.TIME_PORTAL_LEFT)
        self.drawCharacter(item.column + 1, item.row, self.TIME_PORTAL_CENTER)
        self.drawCharacter(item.column + 2, item.row, self.TIME_PORTAL_CENTER)
        self.drawCharacter(item.column + 3, item.row, self.TIME_PORTAL_RIGHT)
        for row in range(item.row + 1, item.row + item.height):
            self.drawCharacter(item.column + 1, row, self.TIME_PORTAL_FIELD)
            self.drawCharacter(item.column + 2, row, self.TIME_PORTAL_FIELD)

    def drawMovingPlatform(self, platform: MovingPlatformInfo):
        y = int((platform.row + 0.4) * self.cellSize)
        x1 = int(platform.xPos / 4 * self.cellSize)
        x2 = x1 + (platform.width + platform.range.getValue()) * self.cellSize
        self.drawHorizontalLineWithArrowsAndShadow(x1, x2, y, self.annotationPen)

        time = self.levelTime // 2
        if platform.timeResist:
            time += self.globalTime // 2
        position = PositionCalculator.calculate(time, platform.moveOffset, platform.range)
        position += platform.xPos
        position /= 4
        timeResist = self.BOARD_ITEM_TIME_RESIST if platform.timeResist else 0
        self.drawCharacter(position, platform.row, self.BOARD_ITEM_MOVING_PLATFORM_L0 + timeResist)
        position += 1

        for i in range(1, platform.width - 1):
            self.drawCharacter(position, platform.row, self.BOARD_ITEM_MOVING_PLATFORM_C + timeResist)
            position += 1
        self.drawCharacter(position, platform.row, self.BOARD_ITEM_MOVING_PLATFORM_R0 + timeResist)

    def drawHorizontalEnemy(self, enemy: EnemyInfo):
        y = int((enemy.yPos // 8 + 1) * self.cellSize)
        x1 = int(enemy.xPos / 4 * self.cellSize)
        x2 = x1 + (2 + enemy.range.getValue()) * self.cellSize
        self.drawHorizontalLineWithArrowsAndShadow(x1, x2, y, self.annotationPen)

        time = self.levelTime // 2
        if enemy.timeResist:
            time += self.globalTime // 2
        position = PositionCalculator.calculate(time, enemy.moveOffset, enemy.range)
        position += enemy.xPos
        position /= 4
        timeResist = self.BOARD_ITEM_TIME_RESIST if enemy.timeResist else 0
        self.drawCharacter(position, enemy.yPos // 8,
                           self.BOARD_ITEM_HORIZONTAL_ENEMY_C0R0F0 + timeResist)
        self.drawCharacter(position + 1, enemy.yPos // 8,
                           self.BOARD_ITEM_HORIZONTAL_ENEMY_C1R0F0 + timeResist)
        self.drawCharacter(position, enemy.yPos // 8 + 1,
                           self.BOARD_ITEM_HORIZONTAL_ENEMY_C0R1F0 + timeResist)
        self.drawCharacter(position + 1, enemy.yPos // 8 + 1,
                           self.BOARD_ITEM_HORIZONTAL_ENEMY_C1R1F0 + timeResist)

    def drawVerticalEnemy(self, enemy: EnemyInfo):
        x = int((enemy.xPos // 4 + 1) * self.cellSize)
        y1 = int(enemy.yPos / 8 * self.cellSize)
        y2 = y1 + (2 + enemy.range.getValue()) * self.cellSize
        self.drawVerticalLineWithArrowsAndShadow(x, y1, y2, self.annotationPen)

        time = self.levelTime // 2
        if enemy.timeResist:
            time += self.globalTime // 2
        position = PositionCalculator.calculate(time, enemy.moveOffset, enemy.range) * 2
        position += enemy.yPos
        position /= 8
        timeResist = self.BOARD_ITEM_TIME_RESIST if enemy.timeResist else 0
        self.drawCharacter(enemy.xPos // 4, position,
                           self.BOARD_ITEM_VERTICAL_ENEMY_C0R0F0 + timeResist)
        self.drawCharacter(enemy.xPos // 4 + 1, position,
                           self.BOARD_ITEM_VERTICAL_ENEMY_C1R0F0 + timeResist)
        self.drawCharacter(enemy.xPos // 4, position + 1,
                           self.BOARD_ITEM_VERTICAL_ENEMY_C0R1F0 + timeResist)
        self.drawCharacter(enemy.xPos // 4 + 1, position + 1,
                           self.BOARD_ITEM_VERTICAL_ENEMY_C1R1F0 + timeResist)

    def drawLaser(self, item: LaserInfo):
        timeResist = self.BOARD_ITEM_TIME_RESIST if item.timeResist else 0
        time = self.levelTime // 2
        if item.timeResist:
            time += self.globalTime // 2
        counter = time % 256

        if item.kind == LaserKind.LeftToRight:
            self.drawCharacter(item.column, item.row, self.BOARD_ITEM_LASER_LEFT + timeResist)
            for col in range(item.column + 1, item.column + item.width):
                transparent = counter & item.mask != 0
                counter -= 1
                if counter < 0:
                    counter = 255
                self.drawCharacter(col, item.row, self.BOARD_ITEM_LASER + timeResist, transparent)
        else:
            self.drawCharacter(item.column + item.width - 1, item.row, self.BOARD_ITEM_LASER_RIGHT + timeResist)
            for col in reversed(range(item.column, item.column + item.width - 1)):
                transparent = counter & item.mask != 0
                counter -= 1
                if counter < 0:
                    counter = 255
                self.drawCharacter(col, item.row, self.BOARD_ITEM_LASER + timeResist, transparent)

    def drawForceField(self, item: ForceFieldInfo):
        timeResist = self.BOARD_ITEM_TIME_RESIST if item.timeResist else 0
        self.drawCharacter(item.column, item.row, self.BOARD_ITEM_FORCE_FIELD_SOURCE + timeResist)

        transparent = not self.levelInfo.isForceFieldActive(item, self.levelTime, self.globalTime)
        for row in range(item.row + 1, item.row + item.height):
            self.drawCharacter(item.column, row, self.BOARD_ITEM_FORCE_FIELD + timeResist, transparent)

        if item.methodKind == ActivationMethodKind.State:
            text = str(item.state)
            if item.invertedActivation:
                text = '~' + text
            self.drawText(item.column * self.cellSize + 1, (item.row + 1) * self.cellSize, text, self.annotationPen)

    def drawDisappearingPlatform(self, item: DisappearingPlatformInfo):
        timeResist = self.BOARD_ITEM_TIME_RESIST if item.timeResist else 0

        transparent = not self.levelInfo.isDisappearingPlatformActive(item, self.levelTime, self.globalTime)
        self.drawCharacter(item.column, item.row, self.BOARD_ITEM_ACTIVABLE_PLATFORM_L + timeResist, transparent)
        i = 1
        while i < item.width - 1:
            self.drawCharacter(item.column + i, item.row, self.BOARD_ITEM_ACTIVABLE_PLATFORM_C + timeResist,
                               transparent)
            i += 1
        self.drawCharacter(item.column + i, item.row, self.BOARD_ITEM_ACTIVABLE_PLATFORM_R + timeResist, transparent)

        if item.methodKind == ActivationMethodKind.State:
            text = str(item.state)
            if item.invertedActivation:
                text = '~' + text
            self.drawText(item.column * self.cellSize + 1, (item.row + 1) * self.cellSize, text, self.annotationPen)

    def drawButton(self, item: ButtonInfo):
        timeResist = self.BOARD_ITEM_TIME_RESIST if item.timeResist else 0
        self.drawCharacter(item.column, item.row, self.BOARD_ITEM_BUTTON_UP_LT + timeResist)
        self.drawCharacter(item.column + 1, item.row, self.BOARD_ITEM_BUTTON_UP_RT + timeResist)
        self.drawCharacter(item.column, item.row + 1, self.BOARD_ITEM_BUTTON_UP_LB + timeResist)
        self.drawCharacter(item.column + 1, item.row + 1, self.BOARD_ITEM_BUTTON_UP_RB + timeResist)
        text = str(item.state)
        if item.kind == ButtonKind.TurnsOff:
            text = '~' + text
        self.drawText(item.column * self.cellSize + 1, (item.row + 1) * self.cellSize, text, self.annotationPen)

    def drawHourglass(self, hourglass: HourglassInfo):
        timeResist = self.BOARD_ITEM_TIME_RESIST if hourglass.timeResist else 0
        self.drawCharacter(hourglass.column, hourglass.row, self.BOARD_ITEM_HOURGLASS_LT + timeResist)
        self.drawCharacter(hourglass.column + 1, hourglass.row, self.BOARD_ITEM_HOURGLASS_RT + timeResist)
        self.drawCharacter(hourglass.column, hourglass.row + 1, self.BOARD_ITEM_HOURGLASS_LB + timeResist)
        self.drawCharacter(hourglass.column + 1, hourglass.row + 1, self.BOARD_ITEM_HOURGLASS_RB + timeResist)

    def drawPortal(self, item: PortalInfo):
        for row in range(item.row, item.row + item.height // 2):
            self.drawCharacter(item.column, row, self.BOARD_ITEM_PORTAL_TOP)
        for row in range(item.row + item.height // 2, item.row + item.height):
            self.drawCharacter(item.column, row, self.BOARD_ITEM_PORTAL_BOTTOM)
        text = '='
        match item.kind:
            case PortalEntryKind.LeftSide:
                text = '>'
            case PortalEntryKind.RightSide:
                text = '<'
        index = self.levelInfo.portals.index(item)
        text = str(index) + text
        self.drawText(item.column * self.cellSize + 1, (item.row + 1) * self.cellSize, text, self.annotationPen)

    def drawHorizontalLineWithArrowsAndShadow(self, x1, x2, y, pen):
        self.painter.setPen(self.blackPen3)
        self.drawHorizontalLineWithArrows(x1, x2, y)
        self.painter.setPen(pen)
        self.drawHorizontalLineWithArrows(x1, x2, y)

    def drawHorizontalLineWithArrows(self, x1, x2, y):
        dx = 8
        dy = 4
        self.painter.drawLine(x1, y, x2, y)
        self.painter.drawLine(x1, y, x1 + dx, y - dy)
        self.painter.drawLine(x1, y, x1 + dx, y + dy)
        self.painter.drawLine(x2 - dx, y - dy, x2, y)
        self.painter.drawLine(x2 - dx, y + dy + 1, x2, y + 1)

    def drawVerticalLineWithArrowsAndShadow(self, x, y1, y2, pen):
        self.painter.setPen(self.blackPen3)
        self.drawVerticalLineWithArrows(x, y1, y2)
        self.painter.setPen(pen)
        self.drawVerticalLineWithArrows(x, y1, y2)

    def drawVerticalLineWithArrows(self, x, y1, y2):
        dx = 4
        dy = 8
        self.painter.drawLine(x, y1, x, y2)
        self.painter.drawLine(x, y1, x - dx, y1 + dy)
        self.painter.drawLine(x, y1, x + dx, y1 + dy)
        self.painter.drawLine(x - dx, y2 - dy, x, y2)
        self.painter.drawLine(x + dx + 1, y2 - dy, x + 1, y2)

    def drawArea(self, xRange, yRange, item):
        for row in yRange:
            for col in xRange:
                self.drawCharacter(col, row, item)

    def drawCharacter(self, col, row, item, transparent=False):
        x = int(col * self.cellSize)
        y = int(row * self.cellSize)
        self.painter.drawImage(x, y, self.font.characters[item])
        if transparent:
            self.painter.fillRect(x, y, self.cellSize, self.cellSize, self.transparentColor)

    def drawText(self, x, y, text: str, pen: QPen):
        self.painter.setPen(self.blackPen)
        for j in range(y - 1, y + 2):
            for i in range(x - 1, x + 2):
                self.painter.drawText(i, j, text)

        self.painter.setPen(pen)
        self.painter.drawText(x, y, text)
