from PyQt6.QtCore import QObject, pyqtSignal
from PyQt6.QtGui import QUndoStack

from commands.baseCommands import BaseUndoCommand


class History(QUndoStack):

    changed = pyqtSignal()

    def clear(self):
        super().clear()
        self.changed.emit()

    def push(self, cmd):
        super().push(cmd)
        self.changed.emit()

    def redo(self):
        super().redo()
        self.changed.emit()

    def undo(self):
        super().undo()
        self.changed.emit()

    def updateLanguage(self):
        for i in range(self.count()):
            command = self.command(i)
            if issubclass(command.__class__, BaseUndoCommand):
                command.updateLanguage()
