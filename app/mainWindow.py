import copy
import json
import os
import subprocess
import sys
from pathlib import Path

from PyQt6.QtCore import Qt, pyqtSlot
from PyQt6.QtGui import QIcon
from PyQt6.QtWidgets import QMainWindow, QLabel, QHBoxLayout, QSplitter, QWidget, QVBoxLayout

from app.actions import Actions
from app.history import History
from app.settings import Settings
from commands.commandFactory import CommandFactory
from commands.statesCommands import StateOnChangedCommand
from data.asmLevelExporter import AsmLevelExporter
from data.asmLevelImporter import AsmLevelImporter
from data.binLevelExporter import BinLevelExporter
from data.binThumbnailExporter import BinThumbnailExporter
from data.language import LanguageKind, Language
from data.levelInfo import LevelInfo
from data.levelValidator import LevelValidator
from data.selectionManager import SelectionManager
from gui.aboutWindow import AboutWindow
from gui.dialogs import Dialogs
from gui.iconProvider import IconProvider
from gui.levelPanel import LevelPanel
from gui.levelProblemsWindow import LevelProblemsWindow
from gui.listItemsWidgets import ListItemsWidgets
from gui.mainMenu import MainMenu
from gui.mainToolBar import MainToolBar
from gui.mainToolPanel import MainToolPanel, ToolPanelKind
from gui.timeControlPanel import TimeControlPanel
from image.fontProvider import ZoomKind
from image.thumbnailGenerator import ThumbnailGenerator


class MainWindow(QMainWindow):

    version = 'v1.5'
    gameFileName = 'time_wizard_tester.xex'
    showMessageMs = 3000

    def __init__(self):
        super().__init__()

        self.settings = Settings()
        self.settings.load()

        self.lang = Language(self.settings.getLanguage())

        self.fileName = None
        self.history = History()
        self.levelAsmImporter = AsmLevelImporter()
        self.levelAsmExporter = AsmLevelExporter()
        self.levelBinExporter = BinLevelExporter()
        self.thumbnailBinExporter = BinThumbnailExporter()

        self.statusbar = None
        self.wcLabel = None

        self.levelInfo = LevelInfo()
        self.validator = LevelValidator(self.levelInfo)
        self.thumbnailGenerator = ThumbnailGenerator(self.levelInfo)

        self.setWindowIcon(QIcon(':/icons/time-wizard-1'))

        self.resize(1300, 650)

        self.clipboardItem = None
        self.iconProvider = IconProvider()
        self.actions = Actions(self, self.iconProvider, self.lang.lang)
        self.selectionManager = SelectionManager(self.actions)
        self.mainMenu = MainMenu(self, self.actions)
        self.toolBar = MainToolBar(self, self.actions)
        self.createStatusBar()

        self.widgets = ListItemsWidgets()
        self.commandFactory = CommandFactory(self.levelInfo, self.widgets, self.lang)
        self.mainToolPanel = MainToolPanel(self.levelInfo, self.history, self.selectionManager,
                                           self.thumbnailGenerator, self.widgets, self.commandFactory,
                                           self.iconProvider, self.lang)

        self.timeControlPanel = TimeControlPanel()
        self.levelPanel = LevelPanel(self.actions, self.levelInfo, self.history, self.selectionManager,
                                     self.commandFactory, self.lang)

        rightPanel = QWidget()
        rightSideLayout = QVBoxLayout()
        rightSideLayout.addWidget(self.timeControlPanel)
        rightSideLayout.addWidget(self.levelPanel)
        rightPanel.setLayout(rightSideLayout)
        rightPanel.setContentsMargins(0, 0, 0, 0)
        rightSideLayout.setContentsMargins(0, 0, 0, 0)

        mainSplitter = QSplitter()
        mainSplitter.setChildrenCollapsible(False)

        layout = QHBoxLayout()
        mainSplitter.setOrientation(Qt.Orientation.Horizontal)
        mainSplitter.addWidget(self.mainToolPanel)
        mainSplitter.addWidget(rightPanel)
        mainSplitter.setSizes([1, 1000])
        layout.addWidget(mainSplitter)

        self.connectActions()
        self.updateZoom()

        self.centralWidget = QWidget()
        self.centralWidget.setLayout(layout)
        self.setCentralWidget(self.centralWidget)
        self.setFileName(None)

        self.problemsWindow = LevelProblemsWindow(self, self.validator, self.selectionManager, self.lang)
        self.aboutWindow = AboutWindow(self, self.version, self.lang)
        self.updateLanguage()

    def connectActions(self):
        self.actions.newAction.triggered.connect(self.newFile)
        self.actions.openAction.triggered.connect(self.openFile)
        self.actions.saveAction.triggered.connect(self.saveFile)
        self.actions.saveAsAction.triggered.connect(self.saveFileAs)
        self.actions.importAction.triggered.connect(self.importFile)
        self.actions.exportAction.triggered.connect(self.exportFile)
        self.actions.exitAction.triggered.connect(self.close)

        self.actions.undoAction.triggered.connect(self.undoContent)
        self.actions.redoAction.triggered.connect(self.redoContent)
        self.actions.cutAction.triggered.connect(self.cutItem)
        self.actions.copyAction.triggered.connect(self.copyItem)
        self.actions.pasteAction.triggered.connect(self.pasteItem)

        self.actions.aboutAction.triggered.connect(self.about)

        self.actions.testAction.triggered.connect(self.compileAndTest)

        self.actions.zoomOutAction.triggered.connect(self.zoomOut)
        self.actions.zoomInAction.triggered.connect(self.zoomIn)

        self.actions.generalViewAction.triggered.connect(self.mainToolPanel.showGeneral)
        self.actions.levelPortalInViewAction.triggered.connect(self.mainToolPanel.showPortalIn)
        self.actions.levelPortalOutViewAction.triggered.connect(self.mainToolPanel.showPortalOut)
        self.actions.boardItemsViewAction.triggered.connect(self.mainToolPanel.showBoardItems)
        self.actions.movingPlatformsViewAction.triggered.connect(self.mainToolPanel.showMovingPlatforms)
        self.actions.enemiesViewAction.triggered.connect(self.mainToolPanel.showEnemies)
        self.actions.lasersViewAction.triggered.connect(self.mainToolPanel.showLasers)
        self.actions.forceFieldsViewAction.triggered.connect(self.mainToolPanel.showForceFields)
        self.actions.disappearingPlatformsViewAction.triggered.connect(self.mainToolPanel.showDisappearingPlatforms)
        self.actions.buttonsViewAction.triggered.connect(self.mainToolPanel.showButtons)
        self.actions.hourglassesViewAction.triggered.connect(self.mainToolPanel.showHourglasses)
        self.actions.portalsViewAction.triggered.connect(self.mainToolPanel.showPortals)

        self.actions.stateActions[0].triggered.connect(self.state0Action)
        self.actions.stateActions[1].triggered.connect(self.state1Action)
        self.actions.stateActions[2].triggered.connect(self.state2Action)
        self.actions.stateActions[3].triggered.connect(self.state3Action)
        self.actions.stateActions[4].triggered.connect(self.state4Action)
        self.actions.stateActions[5].triggered.connect(self.state5Action)
        self.actions.stateActions[6].triggered.connect(self.state6Action)
        self.actions.stateActions[7].triggered.connect(self.state7Action)

        self.actions.languageEnAction.triggered.connect(self.setToEnglish)
        self.actions.languagePlAction.triggered.connect(self.setToPolish)
        self.actions.languageEsAction.triggered.connect(self.setToSpanish)

        self.history.canUndoChanged[bool].connect(self.canUndoChanged)
        self.history.canRedoChanged[bool].connect(self.canRedoChanged)
        self.history.undoTextChanged[str].connect(self.undoTextChanged)
        self.history.redoTextChanged[str].connect(self.redoTextChanged)
        self.history.cleanChanged.connect(self.cleanChanged)
        self.history.changed.connect(self.historyChanged)

        self.selectionManager.selectionChanged.connect(self.selectionChanged)
        self.timeControlPanel.timeChanged.connect(self.timeChanged)

    def closeEvent(self, event):
        if not self.history.isClean() and Dialogs.doNotClose(self.lang):
            event.ignore()
        else:
            self.history.clear()
            self.settings.save()
            event.accept()

    def updateWindowTitle(self):
        if self.fileName is None:
            self.setWindowTitle(f'{self.lang.mainTitle()} {self.version} - {self.lang.untitled()}')
        else:
            self.setWindowTitle(f'{self.lang.mainTitle()} {self.version} - {self.fileName}')

    def setFileName(self, name):
        self.fileName = name
        self.updateWindowTitle()
        self.updateSaveAction()

    def newFile(self):
        if not self.history.isClean() and Dialogs.doNotClose(self.lang):
            return

        self.levelInfo.clear()
        self.history.clear()
        self.history.setClean()
        self.mainToolPanel.reinitControls()
        self.setFileName(None)

    def internalLoadFile(self):
        with open(self.fileName, 'r') as f:
            self.levelInfo.fromJSON(json.loads(f.read()))

    def openFile(self):
        if not self.history.isClean() and Dialogs.doNotClose(self.lang):
            return

        fileName = Dialogs.openFile(self.lang)
        if fileName is None:
            return

        self.levelInfo.clear()
        self.history.clear()
        self.history.setClean()
        self.selectionManager.clear()

        self.setFileName(fileName)
        try:
            self.internalLoadFile()
        except Exception as e:
            Dialogs.error(self.lang, self.lang.loadError(), str(e))
            self.levelInfo.clear()
            self.setFileName(None)

        self.mainToolPanel.reinitControls()
        self.levelChanged()

    def internalSaveFile(self):
        try:
            with open(self.fileName, 'w') as f:
                f.write(self.levelInfo.toJSON())
            self.history.setClean()
        except Exception as e:
            Dialogs.error(self.lang, self.lang.saveError(), str(e))

    def saveFile(self):
        if self.fileName is None:
            self.saveFileAs()
        else:
            self.internalSaveFile()

    def saveFileAs(self):
        fileName = Dialogs.saveFile(self.lang)
        if fileName is None:
            return

        self.setFileName(fileName)
        self.internalSaveFile()

    def importFile(self):
        if not self.history.isClean() and Dialogs.doNotClose(self.lang):
            return

        fileName = Dialogs.importFile(self.lang)
        if fileName is None:
            return

        self.levelInfo.clear()
        try:
            self.levelAsmImporter.load(self.levelInfo, fileName)
        except Exception as e:
            Dialogs.error(self.lang, self.lang.importError(), str(e))
            self.levelInfo.clear()

        self.history.clear()
        self.history.setClean()
        self.selectionManager.clear()
        self.setFileName(None)
        self.mainToolPanel.reinitControls()
        self.levelChanged()

    def updateStatusBar(self):
        count = len(self.validator.problems)
        if count == 0:
            self.wcLabel.setText('Ok')
            self.wcLabel.setStyleSheet('QLabel { color : green; }')
        else:
            self.wcLabel.setText(f'{self.lang.problems()}: {count}')
            self.wcLabel.setStyleSheet('QLabel { color : red; }')

    def levelChanged(self):
        self.updateStateActions()
        self.levelPanel.updateContent()
        self.validator.validate()
        self.updateStatusBar()
        self.problemsWindow.update()

    def compileAndTest(self):
        if len(self.validator.problems) > 0:
            Dialogs.error(self.lang, self.lang.testError(), self.lang.thereAreProblemsTest())
            self.problemsWindow.show()
            return

        try:
            self.levelBinExporter.exportTest(self.levelInfo, self.gameFileName)
            if sys.platform == "win32":
                os.startfile(self.gameFileName, 'open')
            else:
                opener = "open" if sys.platform == "darwin" else "xdg-open"
                subprocess.call([opener, self.gameFileName])
        except Exception as e:
            Dialogs.error(self.lang, self.lang.testError(), str(e))

    def exportFile(self):
        if (len(self.validator.problems) > 0) and Dialogs.doNotExport(self.lang):
            return

        fileName = Dialogs.exportFile(self.lang)
        if fileName is None:
            return

        try:
            self.levelAsmExporter.save(self.levelInfo, fileName)
            path = Path(fileName)
            path = path.with_suffix('.level')
            self.levelBinExporter.save(self.levelInfo, str(path))
            path = path.with_suffix('.thumb')
            self.thumbnailBinExporter.save(self.thumbnailGenerator.thumbnail, str(path))
        except Exception as e:
            Dialogs.error(self.lang, self.lang.exportError(), str(e))

    def undoContent(self):
        if self.history.canUndo():
            self.statusbar.showMessage(f'{self.lang.undo()} {self.history.undoText()}', self.showMessageMs)
            self.history.undo()

    def redoContent(self):
        if self.history.canRedo():
            self.statusbar.showMessage(f'{self.lang.redo()} {self.history.redoText()}', self.showMessageMs)
            self.history.redo()

    def cutItem(self):
        item = self.selectionManager.selected
        if (item is None) or (not item.isCopyable):
            return
        self.clipboardItem = copy.deepcopy(item)
        command = self.commandFactory.createRemoveItem(item)
        if not (command is None):
            self.history.push(command)
        self.actions.pasteAction.setEnabled(True)

    def copyItem(self):
        item = self.selectionManager.selected
        if (item is None) or (not item.isCopyable):
            return
        self.clipboardItem = copy.deepcopy(item)
        self.actions.pasteAction.setEnabled(True)

    def pasteItem(self):
        if self.clipboardItem is None:
            return
        item = copy.deepcopy(self.clipboardItem)
        command = self.commandFactory.createAddItem(item)
        if not (command is None):
            self.history.push(command)

    def about(self):
        self.aboutWindow.show()

    def zoomOut(self):
        if self.levelPanel.zoom == ZoomKind.Zoom4:
            self.levelPanel.setZoom(ZoomKind.Zoom3)
        elif self.levelPanel.zoom == ZoomKind.Zoom3:
            self.levelPanel.setZoom(ZoomKind.Zoom2)
        self.updateZoom()

    def zoomIn(self):
        if self.levelPanel.zoom == ZoomKind.Zoom2:
            self.levelPanel.setZoom(ZoomKind.Zoom3)
        elif self.levelPanel.zoom == ZoomKind.Zoom3:
            self.levelPanel.setZoom(ZoomKind.Zoom4)
        self.updateZoom()

    def updateZoom(self):
        self.actions.zoomOutAction.setEnabled(self.levelPanel.zoom != ZoomKind.Zoom2)
        self.actions.zoomInAction.setEnabled(self.levelPanel.zoom != ZoomKind.Zoom4)

    def state0Action(self, value: bool):
        self.stateAction(0, value)

    def state1Action(self, value: bool):
        self.stateAction(1, value)

    def state2Action(self, value: bool):
        self.stateAction(2, value)

    def state3Action(self, value: bool):
        self.stateAction(3, value)

    def state4Action(self, value: bool):
        self.stateAction(4, value)

    def state5Action(self, value: bool):
        self.stateAction(5, value)

    def state6Action(self, value: bool):
        self.stateAction(6, value)

    def state7Action(self, value: bool):
        self.stateAction(7, value)

    def stateAction(self, index: int, value: bool):
        command = StateOnChangedCommand(self.levelInfo.states[index], not value, value, self.lang)
        self.history.push(command)

    def setToEnglish(self):
        self.lang.lang = LanguageKind.en
        self.updateLanguage()

    def setToPolish(self):
        self.lang.lang = LanguageKind.pl
        self.updateLanguage()

    def setToSpanish(self):
        self.lang.lang = LanguageKind.es
        self.updateLanguage()

    def updateLanguage(self):
        self.settings.setLanguage(self.lang.lang)
        self.updateWindowTitle()
        self.actions.updateLanguage(self.lang)
        self.mainMenu.updateLanguage(self.lang)
        self.history.updateLanguage()
        self.undoTextChanged(self.history.undoText())
        self.redoTextChanged(self.history.redoText())
        self.mainToolPanel.updateLanguage()
        self.levelPanel.updateLanguage()
        self.problemsWindow.updateLanguage()
        self.aboutWindow.updateLanguage()
        self.levelChanged()

    @pyqtSlot(bool)
    def canUndoChanged(self, can):
        self.actions.undoAction.setEnabled(can)

    @pyqtSlot(bool)
    def canRedoChanged(self, can):
        self.actions.redoAction.setEnabled(can)

    @pyqtSlot(str)
    def undoTextChanged(self, text):
        self.actions.undoAction.setText(f'{self.lang.undo()} {text}')

    @pyqtSlot(str)
    def redoTextChanged(self, text):
        self.actions.redoAction.setText(f'{self.lang.redo()} {text}')

    @pyqtSlot()
    def cleanChanged(self):
        self.updateSaveAction()

    def updateSaveAction(self):
        self.actions.saveAction.setEnabled((not self.history.isClean()) or (self.fileName is None))

    def updateStateActions(self):
        for i, state in enumerate(self.levelInfo.states):
            self.actions.stateActions[i].setChecked(state.on)
            self.actions.stateActions[i].setEnabled(self.levelInfo.isObjectWithState(i))

    @pyqtSlot()
    def historyChanged(self):
        self.levelChanged()
        self.mainToolPanel.updateControls()

    @pyqtSlot()
    def selectionChanged(self):
        tabKind = self.mainToolPanel.updateSelection()
        match tabKind:
            case ToolPanelKind.General:
                self.actions.generalViewAction.setChecked(True)
            case ToolPanelKind.LevelPortalIn:
                self.actions.levelPortalInViewAction.setChecked(True)
            case ToolPanelKind.LevelPortalOut:
                self.actions.levelPortalOutViewAction.setChecked(True)
            case ToolPanelKind.BoardItems:
                self.actions.boardItemsViewAction.setChecked(True)
            case ToolPanelKind.MovingPlatforms:
                self.actions.movingPlatformsViewAction.setChecked(True)
            case ToolPanelKind.Enemies:
                self.actions.enemiesViewAction.setChecked(True)
            case ToolPanelKind.Lasers:
                self.actions.lasersViewAction.setChecked(True)
            case ToolPanelKind.Buttons:
                self.actions.buttonsViewAction.setChecked(True)
            case ToolPanelKind.ForceFields:
                self.actions.forceFieldsViewAction.setChecked(True)
            case ToolPanelKind.DisappearingPlatforms:
                self.actions.disappearingPlatformsViewAction.setChecked(True)
            case ToolPanelKind.Hourglasses:
                self.actions.hourglassesViewAction.setChecked(True)
            case ToolPanelKind.Portals:
                self.actions.portalsViewAction.setChecked(True)

        self.levelPanel.updateContent()

    def timeChanged(self):
        self.levelPanel.updateTime(
            self.timeControlPanel.levelTimeSlider.value(),
            self.timeControlPanel.globalTimeSlider.value())

    @staticmethod
    def getWordCount():
        return 42

    def createStatusBar(self):
        self.statusbar = self.statusBar()
        self.statusbar.showMessage("Ready", self.showMessageMs)

        self.wcLabel = QLabel()
        self.statusbar.addPermanentWidget(self.wcLabel)
        self.wcLabel.mousePressEvent = self.problemsMousePressEvent

    def problemsMousePressEvent(self, event):
        self.problemsWindow.show()
