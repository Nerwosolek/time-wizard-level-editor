import json

from data.language import LanguageKind


class Settings:

    fileName = 'settings.json'

    languages = {
        LanguageKind.en: 'en',
        LanguageKind.pl: 'pl',
        LanguageKind.es: 'es'
    }

    def __init__(self):
        self.language = 'en'

    def getLanguage(self):
        keyList = list(self.languages.keys())
        valList = list(self.languages.values())

        index = valList.index(self.language)
        if index > 0:
            return keyList[index]
        else:
            return LanguageKind.en

    def setLanguage(self, lang: LanguageKind):
        self.language = self.languages[lang]

    def save(self):
        try:
            with open(self.fileName, 'w') as f:
                data = json.dumps(self, default=lambda o: o.__dict__, sort_keys=False, indent=4)
                f.write(data)
        except Exception as e:
            pass

    def load(self):
        try:
            with open(self.fileName, 'r') as f:
                data = json.loads(f.read())
                self.language = data['language']
        except Exception as e:
            return
