import json
from abc import abstractmethod, ABC

from PyQt6.QtCore import QRect, QPoint, QSize

from data.enums import (BoardColor, BoardItemKind, RangeKind, EnemyKind, LaserKind, ActivationMethodKind, ButtonKind,
                        PortalEntryKind)
from data.language import Language
from data.ranges import Ranges


class PixelInfo:

    pixelsInColumn = 4
    pixelsInRow = 8

    @staticmethod
    def columnToPix(column: int):
        return column * PixelInfo.pixelsInColumn

    @staticmethod
    def rowToPix(row: int):
        return row * PixelInfo.pixelsInRow

    @staticmethod
    def getCorrectXPos(pos: QPoint):
        x = pos.x()
        if x < Ranges.xPos.min:
            x = Ranges.xPos.min
        if x > Ranges.xPos.max:
            x = Ranges.xPos.max
        return x

    @staticmethod
    def getCorrectYPos(pos: QPoint):
        y = pos.y()
        if y < Ranges.yPos.min:
            y = Ranges.yPos.min
        if y > Ranges.yPos.max:
            y = Ranges.yPos.max
        return y

    @staticmethod
    def getCorrectedColumn(pos: QPoint):
        column = pos.x() // PixelInfo.pixelsInColumn
        if column < Ranges.column.min:
            column = Ranges.column.min
        if column > Ranges.column.max:
            column = Ranges.column.max
        return column

    @staticmethod
    def getCorrectedRow(pos: QPoint):
        row = pos.y() // PixelInfo.pixelsInRow
        if row < Ranges.row.min:
            row = Ranges.row.min
        if row > Ranges.row.max:
            row = Ranges.row.max
        return row

    @staticmethod
    def roundXPos(xPos: int):
        return (xPos // PixelInfo.pixelsInColumn) * PixelInfo.pixelsInColumn

    @staticmethod
    def roundYPos(yPos: int):
        return (yPos // PixelInfo.pixelsInRow) * PixelInfo.pixelsInRow


class ItemInfo:
    @abstractmethod
    def getItemName(self, lang: Language):
        pass

    @abstractmethod
    def getRect(self):
        pass

    @abstractmethod
    def getRects(self):
        pass

    @abstractmethod
    def getPos(self):
        pass

    @abstractmethod
    def getSize(self):
        pass

    def getCorrectedSize(self, size: QSize):
        pass

    @abstractmethod
    def langStr(self, lang: Language):
        pass

    @abstractmethod
    def horizontalShift(self):
        pass

    @abstractmethod
    def verticalShift(self):
        pass

    def horizontalResize(self):
        return False

    def verticalResize(self):
        return False

    def isCopyable(self):
        return False


class GeneralInfo(ItemInfo):
    def __init__(self, color: BoardColor, columns: int, seconds: int):
        self.color = color
        self.columns = columns
        self.seconds = seconds

    def getItemName(self, lang: Language):
        return lang.general().lower()

    def langStr(self, lang: Language):
        pass

    def getRect(self):
        return None

    def getRects(self):
        return None

    def getPos(self):
        return None

    def getSize(self):
        return None

    def getCorrectedSize(self, size: QSize):
        return None

    def horizontalShift(self):
        return None

    def verticalShift(self):
        return None


class LevelPortalInfo(ItemInfo, ABC):
    def __init__(self, column, row, height):
        self.column = column
        self.row = row
        self.height = height

    def langStr(self, lang: Language):
        pass

    def getRect(self):
        return QRect(PixelInfo.columnToPix(self.column), PixelInfo.rowToPix(self.row), PixelInfo.columnToPix(4),
                     PixelInfo.rowToPix(self.height))

    def getRects(self):
        return [self.getTopRect(), self.getBottomRect()]

    def getTopRect(self):
        return QRect(PixelInfo.columnToPix(self.column), PixelInfo.rowToPix(self.row), PixelInfo.columnToPix(4),
                     PixelInfo.rowToPix(1))

    def getBottomRect(self):
        return QRect(PixelInfo.columnToPix(self.column + 1), PixelInfo.rowToPix(self.row + 1), PixelInfo.columnToPix(2),
                     PixelInfo.rowToPix(self.height - 1))

    def getPos(self):
        return QPoint(self.column, self.row)

    def getSize(self):
        return QSize(4, self.height)

    def getCorrectedSize(self, size: QSize):
        return QSize(4, max(1, min(Ranges.height.max, size.height())))

    def horizontalShift(self):
        return PixelInfo.pixelsInColumn

    def verticalShift(self):
        return PixelInfo.pixelsInRow

    def verticalResize(self):
        return True


class LevelPortalInInfo(LevelPortalInfo):

    def getItemName(self, lang: Language):
        return lang.levelPortalIn().lower()


class LevelPortalOutInfo(LevelPortalInfo):

    def getItemName(self, lang: Language):
        return lang.levelPortalOut()


class BoardItemInfo(ItemInfo):
    def __init__(self, kind: BoardItemKind, column: int, row: int, width: int, height: int):
        self.kind = kind
        self.column = column
        self.row = row
        self.width = max(width, kind.minWidth())
        self.height = max(height, kind.minHeight())

    def getItemName(self, lang: Language):
        return lang.boardItemKindEnum(self.kind).lower()

    def getRect(self):
        return QRect(PixelInfo.columnToPix(self.column), PixelInfo.rowToPix(self.row),
                     PixelInfo.columnToPix(self.getColumns()), PixelInfo.rowToPix(self.getRows()))

    def getRects(self):
        return [self.getRect()]

    def getPos(self):
        return QPoint(self.column, self.row)

    def getSize(self):
        return QSize(self.getColumns(), self.getRows())

    def getCorrectedSize(self, size: QSize):
        return QSize(max(self.getMinWidth(), min(self.getMaxWidth(), size.width())),
                     max(self.getMinHeight(), min(self.getMaxHeight(), size.height())))

    def getMinWidth(self):
        match self.kind:
            case BoardItemKind.BottomWall1x2 | BoardItemKind.TopWall1x2 | BoardItemKind.Platform | \
                 BoardItemKind.Ladder | BoardItemKind.Structure | BoardItemKind.Tile:
                return 2
            case BoardItemKind.VerticalWall | BoardItemKind.Pattern3x3_1 | BoardItemKind.Pattern3x3_2 | \
                 BoardItemKind.Pattern3x3_3:
                return 3
            case BoardItemKind.Pattern4x4_1 | BoardItemKind.Pattern4x4_2 | BoardItemKind.Pattern4x4_3:
                return 4
        return 1

    def getMaxWidth(self):
        if self.kind.containsWidth():
            return Ranges.width.max
        else:
            return self.getMinWidth()

    def getMinHeight(self):
        match self.kind:
            case BoardItemKind.HorizontalWall:
                return 2
            case BoardItemKind.Pattern3x3_1 | BoardItemKind.Pattern3x3_2 | BoardItemKind.Pattern3x3_3:
                return 3
            case BoardItemKind.Pattern4x4_1 | BoardItemKind.Pattern4x4_2 | BoardItemKind.Pattern4x4_3:
                return 4
        return 1

    def getMaxHeight(self):
        if self.kind.containsHeight():
            return Ranges.height.max
        else:
            return self.getMinHeight()

    def getColumns(self):
        if self.kind.containsWidth():
            return self.width
        else:
            return self.getMinWidth()

    def getRows(self):
        if self.kind.containsHeight():
            return self.height
        else:
            return self.getMinHeight()

    def langStr(self, lang: Language):
        return f'{lang.boardItemKindEnum(self.kind)}, x:{self.column}, y:{self.row}{self.widthStr()}{self.heightStr()}'

    def widthStr(self):
        if self.kind.containsWidth():
            return f', w:{self.width}'
        else:
            return ''

    def heightStr(self):
        if self.kind.containsHeight():
            return f', h:{self.height}'
        else:
            return ''

    def horizontalShift(self):
        return PixelInfo.pixelsInColumn

    def verticalShift(self):
        return PixelInfo.pixelsInRow

    def horizontalResize(self):
        return self.kind.containsWidth()

    def verticalResize(self):
        return self.kind.containsHeight()

    def isCopyable(self):
        return True


class MovingPlatformInfo(ItemInfo):
    def __init__(self, timeResist: bool, xPos: int, row: int, width: int, moveRange: RangeKind, moveOffset: int):
        self.timeResist = timeResist
        self.xPos = xPos
        self.row = row
        self.width = width
        self.range = moveRange
        self.moveOffset = moveOffset

    def getItemName(self, lang: Language):
        return lang.movingPlatform().lower()

    def langStr(self, lang: Language):
        return (f'tr:{lang.bool(self.timeResist)}, x:{self.xPos}, y:{self.row}, w:{self.width}, r:{self.range.value}, '
                f'o:{self.moveOffset}')

    def getRect(self):
        return QRect(self.xPos, PixelInfo.rowToPix(self.row), PixelInfo.columnToPix(self.width + self.range.getValue()),
                     PixelInfo.rowToPix(1))

    def getRects(self):
        return [self.getRect()]

    def getPos(self):
        return QPoint(self.xPos, self.row)

    def getSize(self):
        return QSize(self.width, 1)

    def getCorrectedSize(self, size: QSize):
        return QSize(max(2, min(Ranges.width.max, size.width())), 1)

    def horizontalShift(self):
        return 1

    def verticalShift(self):
        return PixelInfo.pixelsInRow

    def horizontalResize(self):
        return True

    def isCopyable(self):
        return True


class EnemyInfo(ItemInfo):
    def __init__(self, kind: EnemyKind, timeResist: bool, xPos: int, yPos: int, moveRange: RangeKind, moveOffset: int):
        self.kind = kind
        self.timeResist = timeResist
        self.xPos = xPos
        self.yPos = yPos
        self.range = moveRange
        self.moveOffset = moveOffset

    def getItemName(self, lang: Language):
        return lang.enemy()

    def langStr(self, lang: Language):
        return (f'{lang.enemyKindEnum(self.kind)}, tr:{lang.bool(self.timeResist)}, x:{self.xPos}, y:{self.yPos}, '
                f'r:{self.range.value}, o:{self.moveOffset}')

    def getRect(self):
        return QRect(self.getXPos(), self.getYPos(), self.getWidthPix(), self.getHeightPix())

    def getRects(self):
        return [self.getRect()]

    def getXPos(self):
        if self.kind == EnemyKind.Horizontal:
            return self.xPos
        else:
            return (self.xPos // PixelInfo.pixelsInColumn) * PixelInfo.pixelsInColumn

    def getYPos(self):
        if self.kind == EnemyKind.Horizontal:
            return (self.yPos // PixelInfo.pixelsInRow) * PixelInfo.pixelsInRow
        else:
            return self.yPos

    def getPos(self):
        return QPoint(self.xPos, self.yPos)

    def getSize(self):
        return QSize(2, 2)

    def getCorrectedSize(self, size: QSize):
        return QSize(2, 2)

    def getWidthPix(self):
        if self.kind == EnemyKind.Horizontal:
            return PixelInfo.columnToPix(2 + self.range.getValue())
        else:
            return PixelInfo.columnToPix(2)

    def getHeightPix(self):
        if self.kind == EnemyKind.Horizontal:
            return PixelInfo.rowToPix(2)
        else:
            return PixelInfo.rowToPix(2 + self.range.getValue())

    def horizontalShift(self):
        if self.kind == EnemyKind.Horizontal:
            return 1
        else:
            return PixelInfo.pixelsInColumn

    def verticalShift(self):
        if self.kind == EnemyKind.Horizontal:
            return PixelInfo.pixelsInRow
        else:
            return 1

    def isCopyable(self):
        return True


class LaserInfo(ItemInfo):
    def __init__(self, kind: LaserKind, timeResist: bool, column: int, row: int, width: int, mask: int):
        self.kind = kind
        self.timeResist = timeResist
        self.column = column
        self.row = row
        self.width = width
        self.mask = mask

    def getItemName(self, lang: Language):
        return lang.laser().lower()

    def langStr(self, lang: Language):
        return (f'{lang.laserKindEnum(self.kind)}, tr:{lang.bool(self.timeResist)}, x:{self.column}, y:{self.row}, '
                f'w:{self.width}, m:{self.mask:08b}')

    def getRect(self):
        return QRect(PixelInfo.columnToPix(self.column), PixelInfo.rowToPix(self.row),
                     PixelInfo.columnToPix(self.width), PixelInfo.rowToPix(1))

    def getRects(self):
        return [self.getRect()]

    def getPos(self):
        return QPoint(self.column, self.row)

    def getSize(self):
        return QSize(self.width, 1)

    def getCorrectedSize(self, size: QSize):
        return QSize(max(2, min(Ranges.width.max, size.width())), 1)

    def horizontalShift(self):
        return PixelInfo.pixelsInColumn

    def verticalShift(self):
        return PixelInfo.pixelsInRow

    def horizontalResize(self):
        return True

    def isCopyable(self):
        return True


class StateInfo(ItemInfo):
    def __init__(self, timeResist: bool, on: bool):
        self.timeResist = timeResist
        self.on = on

    def getItemName(self, lang: Language):
        return lang.state().lower()

    def langStr(self, lang: Language):
        return f'tr:{lang.bool(self.timeResist)}, on:{lang.bool(self.on)}'

    def getRect(self):
        return None

    def getRects(self):
        return [self.getRect()]

    def getPos(self):
        return None

    def getSize(self):
        return None

    def getCorrectedSize(self, size: QSize):
        return None

    def horizontalShift(self):
        return None

    def verticalShift(self):
        return None


class ForceFieldInfo(ItemInfo):
    def __init__(self, methodKind: ActivationMethodKind, invertedActivation: bool,
                 timeResist: bool, column: int, row: int, height: int, mask: int, state: int):
        self.methodKind = methodKind
        self.invertedActivation = invertedActivation
        self.timeResist = timeResist
        self.column = column
        self.row = row
        self.height = height
        self.mask = mask
        self.state = state

    def getItemName(self, lang: Language):
        return lang.forceField().lower()

    def langStr(self, lang: Language):
        return (f'{lang.activationMethodKindEnum(self.methodKind)}, inv:{lang.bool(self.invertedActivation)}, '
                f'tr:{lang.bool(self.timeResist)}, x:{self.column}, y:{self.row}, h:{self.height}, '
                f'{self.maskOrStateStr()}')

    def maskOrStateStr(self):
        if self.methodKind == ActivationMethodKind.Time:
            return f'm:{self.mask:08b}'
        else:
            return f's:{self.state}'

    def getRect(self):
        return QRect(PixelInfo.columnToPix(self.column), PixelInfo.rowToPix(self.row),
                     PixelInfo.columnToPix(1), PixelInfo.rowToPix(self.height))

    def getRects(self):
        return [self.getRect()]

    def getPos(self):
        return QPoint(self.column, self.row)

    def getSize(self):
        return QSize(1, self.height)

    def getCorrectedSize(self, size: QSize):
        return QSize(1, max(2, min(Ranges.height.max, size.height())))

    def horizontalShift(self):
        return PixelInfo.pixelsInColumn

    def verticalShift(self):
        return PixelInfo.pixelsInRow

    def verticalResize(self):
        return True

    def isCopyable(self):
        return True


class DisappearingPlatformInfo(ItemInfo):
    def __init__(self, methodKind: ActivationMethodKind, invertedActivation: bool,
                 timeResist: bool, column: int, row: int, width: int, mask: int, state: int):
        self.methodKind = methodKind
        self.invertedActivation = invertedActivation
        self.timeResist = timeResist
        self.column = column
        self.row = row
        self.width = width
        self.mask = mask
        self.state = state

    def getItemName(self, lang: Language):
        return lang.disappearingPlatform().lower()

    def langStr(self, lang: Language):
        return (f'{lang.activationMethodKindEnum(self.methodKind)}, inv:{lang.bool(self.invertedActivation)}, '
                f'tr:{lang.bool(self.timeResist)}, x:{self.column}, y:{self.row}, w:{self.width},'
                f'{self.maskOrStateStr()}')

    def maskOrStateStr(self):
        if self.methodKind == ActivationMethodKind.Time:
            return f'm:{self.mask:08b}'
        else:
            return f's:{self.state}'

    def getRect(self):
        return QRect(PixelInfo.columnToPix(self.column), PixelInfo.rowToPix(self.row),
                     PixelInfo.columnToPix(self.width), PixelInfo.rowToPix(1))

    def getRects(self):
        return [self.getRect()]

    def getPos(self):
        return QPoint(self.column, self.row)

    def getSize(self):
        return QSize(self.width, 1)

    def getCorrectedSize(self, size: QSize):
        return QSize(max(2, min(Ranges.width.max, size.width())), 1)

    def horizontalShift(self):
        return PixelInfo.pixelsInColumn

    def verticalShift(self):
        return PixelInfo.pixelsInRow

    def horizontalResize(self):
        return True

    def isCopyable(self):
        return True


class ButtonInfo(ItemInfo):
    def __init__(self, kind: ButtonKind, timeResist: bool, column: int, row: int, state: int):
        self.kind = kind
        self.timeResist = timeResist
        self.column = column
        self.row = row
        self.state = state

    def getItemName(self, lang: Language):
        return lang.button().lower()

    def langStr(self, lang: Language):
        return (f'{lang.buttonKindEnum(self.kind)}, tr:{lang.bool(self.timeResist)}, x:{self.column}, y:{self.row}, '
                f's:{self.state}')

    def getRect(self):
        return QRect(PixelInfo.columnToPix(self.column), PixelInfo.rowToPix(self.row), PixelInfo.columnToPix(2),
                     PixelInfo.rowToPix(2))

    def getRects(self):
        return [self.getRect()]

    def getPos(self):
        return QPoint(self.column, self.row)

    def getSize(self):
        return QSize(2, 2)

    def getCorrectedSize(self, size: QSize):
        return QSize(2, 2)

    def horizontalShift(self):
        return PixelInfo.pixelsInColumn

    def verticalShift(self):
        return PixelInfo.pixelsInRow

    def isCopyable(self):
        return True


class HourglassInfo(ItemInfo):
    def __init__(self, timeResist: bool, column: int, row: int):
        self.timeResist = timeResist
        self.column = column
        self.row = row

    def getItemName(self, lang: Language):
        return lang.hourglass().lower()

    def langStr(self, lang: Language):
        return f'tr:{lang.bool(self.timeResist)}, x:{self.column}, y:{self.row}'

    def getRect(self):
        return QRect(PixelInfo.columnToPix(self.column), PixelInfo.rowToPix(self.row), PixelInfo.columnToPix(2),
                     PixelInfo.rowToPix(2))

    def getRects(self):
        return [self.getRect()]

    def getPos(self):
        return QPoint(self.column, self.row)

    def getSize(self):
        return QSize(2, 2)

    def getCorrectedSize(self, size: QSize):
        return QSize(2, 2)

    def horizontalShift(self):
        return PixelInfo.pixelsInColumn

    def verticalShift(self):
        return PixelInfo.pixelsInRow

    def isCopyable(self):
        return True


class PortalInfo(ItemInfo):
    def __init__(self, kind: PortalEntryKind, column: int, row: int, height: int):
        self.kind = kind
        self.column = column
        self.row = row
        self.height = height

    def getItemName(self, lang: Language):
        return lang.portal().lower()

    def langStr(self, lang: Language):
        return f'{lang.portalEntryKindEnum(self.kind)}, x:{self.column}, y:{self.row}, h:{self.height}'

    def getRect(self):
        return QRect(PixelInfo.columnToPix(self.column), PixelInfo.rowToPix(self.row), PixelInfo.columnToPix(1),
                     PixelInfo.rowToPix(self.height))

    def getRects(self):
        return [self.getRect()]

    def getPos(self):
        return QPoint(self.column, self.row)

    def getSize(self):
        return QSize(1, self.height)

    def getCorrectedSize(self, size: QSize):
        return QSize(1, max(2, min(Ranges.height.max, size.height())))

    def horizontalShift(self):
        return PixelInfo.pixelsInColumn

    def verticalShift(self):
        return PixelInfo.pixelsInRow

    def verticalResize(self):
        return True

    def isCopyable(self):
        return True


class LevelInfo:
    latestVersion = 1
    defaultBoardColor = BoardColor.Blue
    defaultColumns = 64
    defaultSeconds = 120
    defaultPortalInColumn = 0
    defaultPortalOutColumn = 4
    defaultPortalRow = 0
    defaultPortalHeight = 10

    def __init__(self):
        self.version = self.latestVersion
        self.general = GeneralInfo(self.defaultBoardColor, self.defaultColumns, self.defaultSeconds)
        self.levelPortalIn = LevelPortalInInfo(self.defaultPortalInColumn, self.defaultPortalRow,
                                               self.defaultPortalHeight)
        self.levelPortalOut = LevelPortalOutInfo(self.defaultPortalOutColumn, self.defaultPortalRow,
                                                 self.defaultPortalHeight)
        self.boardItems = []
        self.movingPlatforms = []
        self.enemies = []
        self.lasers = []
        self.states = []
        self.forceFields = []
        self.disappearingPlatforms = []
        self.buttons = []
        self.hourglasses = []
        self.portals = []
        self.fixProblems()

    def clear(self):
        self.version = self.latestVersion
        self.general.color = self.defaultBoardColor
        self.general.columns = self.defaultColumns
        self.general.seconds = self.defaultSeconds
        self.levelPortalIn.column = self.defaultPortalInColumn
        self.levelPortalIn.row = self.defaultPortalRow
        self.levelPortalIn.height = self.defaultPortalHeight
        self.levelPortalOut.column = self.defaultPortalOutColumn
        self.levelPortalOut.row = self.defaultPortalRow
        self.levelPortalOut.height = self.defaultPortalHeight
        self.boardItems.clear()
        self.movingPlatforms.clear()
        self.enemies.clear()
        self.lasers.clear()
        self.states.clear()
        self.forceFields.clear()
        self.disappearingPlatforms.clear()
        self.buttons.clear()
        self.hourglasses.clear()
        self.portals.clear()
        self.fixProblems()

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=False, indent=4)

    def fillGeneral(self, data):
        self.general.color = BoardColor(data['color'])
        self.general.columns = data['columns']
        self.general.seconds = data['seconds']

    def fillLevelPortalIn(self, data):
        self.levelPortalIn.column = data['column']
        self.levelPortalIn.row = data['row']
        self.levelPortalIn.height = data['height']

    def fillLevelPortalOut(self, data):
        self.levelPortalOut.column = data['column']
        self.levelPortalOut.row = data['row']
        self.levelPortalOut.height = data['height']

    def fillBoardItems(self, data):
        for entry in data:
            self.boardItems.append(
                BoardItemInfo(
                    BoardItemKind(entry['kind']),
                    entry['column'],
                    entry['row'],
                    entry['width'],
                    entry['height']))

    def fillMovingPlatforms(self, data):
        for entry in data:
            self.movingPlatforms.append(
                MovingPlatformInfo(
                    entry['timeResist'],
                    entry['xPos'],
                    entry['row'],
                    entry['width'],
                    RangeKind(entry['range']),
                    entry['moveOffset']))

    def fillEnemies(self, data):
        for entry in data:
            self.enemies.append(
                EnemyInfo(
                    EnemyKind(entry['kind']),
                    entry['timeResist'],
                    entry['xPos'],
                    entry['yPos'],
                    RangeKind(entry['range']),
                    entry['moveOffset']))

    def fillLasers(self, data):
        for entry in data:
            self.lasers.append(
                LaserInfo(
                    LaserKind(entry['kind']),
                    entry['timeResist'],
                    entry['column'],
                    entry['row'],
                    entry['width'],
                    entry['mask']))

    def fillStates(self, data):
        self.states.clear()
        for entry in data:
            self.states.append(
                StateInfo(
                    entry['timeResist'],
                    entry['on']))

    def fillForceFields(self, data):
        for entry in data:
            self.forceFields.append(
                ForceFieldInfo(
                    ActivationMethodKind(entry['methodKind']),
                    entry['invertedActivation'],
                    entry['timeResist'],
                    entry['column'],
                    entry['row'],
                    entry['height'],
                    entry['mask'],
                    entry['state']))

    def fillDisappearingPlatforms(self, data):
        for entry in data:
            self.disappearingPlatforms.append(
                DisappearingPlatformInfo(
                    ActivationMethodKind(entry['methodKind']),
                    entry['invertedActivation'],
                    entry['timeResist'],
                    entry['column'],
                    entry['row'],
                    entry['width'],
                    entry['mask'],
                    entry['state']))

    def fillButtons(self, data):
        for entry in data:
            self.buttons.append(
                ButtonInfo(
                    ButtonKind(entry['kind']),
                    entry['timeResist'],
                    entry['column'],
                    entry['row'],
                    entry['state']))

    def fillHourglasses(self, data):
        for entry in data:
            self.hourglasses.append(
                HourglassInfo(
                    entry['timeResist'],
                    entry['column'],
                    entry['row']))

    def fillPortals(self, data):
        for entry in data:
            self.portals.append(
                PortalInfo(
                    PortalEntryKind(entry['kind']),
                    entry['column'],
                    entry['row'],
                    entry['height']))

    def fromJSON(self, data):
        self.clear()
        self.version = data['version']

        self.fillGeneral(data['general'])
        self.fillLevelPortalIn(data['levelPortalIn'])
        self.fillLevelPortalOut(data['levelPortalOut'])
        self.fillBoardItems(data['boardItems'])
        self.fillMovingPlatforms(data['movingPlatforms'])
        self.fillEnemies(data['enemies'])
        self.fillLasers(data['lasers'])
        self.fillStates(data['states'])
        self.fillForceFields(data['forceFields'])
        self.fillDisappearingPlatforms(data['disappearingPlatforms'])
        self.fillButtons(data['buttons'])
        self.fillHourglasses(data['hourglasses'])
        self.fillPortals(data['portals'])
        self.fixProblems()

    def fixProblems(self):
        while len(self.states) < Ranges.state.count():
            self.states.append(StateInfo(True, False))
        for i, state in enumerate(self.states):
            self.fixStateTimeResist(i, state.timeResist)

    def fixStateTimeResist(self, index: int, timeResist: bool):
        for button in self.buttons:
            if button.state == index:
                button.timeResist = timeResist
        for forceField in self.forceFields:
            if (forceField.methodKind == ActivationMethodKind.State) and (forceField.state == index):
                forceField.timeResist = timeResist
        for platform in self.disappearingPlatforms:
            if (platform.methodKind == ActivationMethodKind.State) and (platform.state == index):
                platform.timeResist = timeResist

    def isObjectWithState(self, index: int):
        for button in self.buttons:
            if button.state == index:
                return True
        for forceField in self.forceFields:
            if (forceField.methodKind == ActivationMethodKind.State) and (forceField.state == index):
                return True
        for platform in self.disappearingPlatforms:
            if (platform.methodKind == ActivationMethodKind.State) and (platform.state == index):
                return True
        return False

    def getObjectWithDifferentTimeResistForState(self, index: int, timeResist: bool):
        result = []
        if self.states[index].timeResist != timeResist:
            result.append(self.states[index])
        for button in self.buttons:
            if (button.state == index) and (button.timeResist != timeResist):
                result.append(button)
        for forceField in self.forceFields:
            if ((forceField.methodKind == ActivationMethodKind.State) and (forceField.state == index) and
                    (forceField.timeResist != timeResist)):
                result.append(forceField)
        for platform in self.disappearingPlatforms:
            if ((platform.methodKind == ActivationMethodKind.State) and (platform.state == index) and
                    (platform.timeResist != timeResist)):
                result.append(platform)
        return result

    def getRelativePortal(self, item: PortalInfo):
        if item is None:
            return None

        if not (item in self.portals):
            return None

        index = self.portals.index(item)
        if index % 2 == 0:
            index += 1
        else:
            index -= 1

        if (index < 0) or (index >= len(self.portals)):
            return None

        return self.portals[index]

    def getObjectsWithTheState(self, item: ItemInfo):
        result = []

        if item is None:
            return result

        if issubclass(item.__class__, StateInfo):
            if not (item in self.states):
                return result
            state = self.states.index(item)
        elif issubclass(item.__class__, ButtonInfo):
            state = item.state
        elif issubclass(item.__class__, ForceFieldInfo) and (item.methodKind == ActivationMethodKind.State):
            state = item.state
        elif issubclass(item.__class__, DisappearingPlatformInfo) and (item.methodKind == ActivationMethodKind.State):
            state = item.state
        else:
            return result

        for b in self.buttons:
            if (b != item) and (b.state == state):
                result.append(b)

        for p in self.forceFields:
            if (p != item) and (p.methodKind == ActivationMethodKind.State) and (p.state == state):
                result.append(p)

        for p in self.disappearingPlatforms:
            if (p != item) and (p.methodKind == ActivationMethodKind.State) and (p.state == state):
                result.append(p)

        return result

    def getStateValue(self, index: int):
        if (index < 0) or (index >= len(self.states)):
            return True
        else:
            return self.states[index].on

    def isForceFieldActive(self, item: ForceFieldInfo, levelTime: int, globalTime: int):
        if item.methodKind == ActivationMethodKind.State:
            active = self.getStateValue(item.state)
        else:
            time = levelTime // 8
            if item.timeResist:
                time += globalTime // 8
            active = time & item.mask
        if item.invertedActivation:
            active = not active
        return active

    def isDisappearingPlatformActive(self, item: DisappearingPlatformInfo, levelTime: int, globalTime: int):
        if item.methodKind == ActivationMethodKind.State:
            active = self.getStateValue(item.state)
        else:
            time = levelTime // 8
            if item.timeResist:
                time += globalTime // 8
            active = time & item.mask
        if item.invertedActivation:
            active = not active
        return active
