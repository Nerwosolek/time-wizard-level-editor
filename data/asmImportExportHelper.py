from data.enums import BoardColor, BoardItemKind, RangeKind, EnemyKind, LaserKind, ActivationMethodKind, ButtonKind, \
    PortalEntryKind


class AsmImportExportHelper:

    generalHeader = '; columns, board color, seconds'
    levelPortalsHeader = '; timePortals: colIn, rowIn, heightIn, colOut, rowOut, heightOut'
    boardItemsHeader = '; board items: kind, col, row, optional width, optional height'
    movingPlatformsHeader = '; moving platforms: rangeBegin, row, width, range, moveOffset, timeResist'
    enemiesHeader = '; enemies: rangeBegin, secondCoordinate, range, moveOffset, kind'
    lasersHeader = '; lasers: leftCol, rightCol, row, mask, kind'
    statesHeader = '; states: value, timeResist'
    activableItemsHeader = '; activable items: col, row, size, active, activationMaskOrState, kind'
    buttonsHeader = '; buttons: col, row, state, kind'
    hourglassesHeader = '; hourglasses: col, row, timeResist'
    portalsHeader = '; portals: col, row, height, kind'
    end = 'bidkEnd'

    boardColor = {
        'bcGray': BoardColor.Gray,
        'bcBrown': BoardColor.Brown,
        'bcGreen': BoardColor.Green,
        'bcBlue': BoardColor.Blue,
        'bcViolet': BoardColor.Violet,
        'bcDarkBlue': BoardColor.DarkBlue
    }

    boardItemKind = {
        'bidkVoid': BoardItemKind.Void,
        'bidkAir': BoardItemKind.Air,
        'bidkWall1x1': BoardItemKind.Wall1x1,
        'bidkBottomWall1x2': BoardItemKind.BottomWall1x2,
        'bidkTopWall1x2': BoardItemKind.TopWall1x2,
        'bidkHorizontalWall': BoardItemKind.HorizontalWall,
        'bidkVerticalWall': BoardItemKind.VerticalWall,
        'bidkLightPlatform': BoardItemKind.LightPlatform,
        'bidkPlatform': BoardItemKind.Platform,
        'bidkLadder': BoardItemKind.Ladder,
        'bidkBottomSpikes': BoardItemKind.BottomSpikes,
        'bidkTopSpikes': BoardItemKind.TopSpikes,
        'bidkLeftSpikes': BoardItemKind.LeftSpikes,
        'bidkRightSpikes': BoardItemKind.RightSpikes,
        'bidkStructure': BoardItemKind.Structure,
        'bidkBottomWall': BoardItemKind.BottomWall,
        'bidkLeftWall': BoardItemKind.LeftWall,
        'bidkRightWall': BoardItemKind.RightWall,
        'bidkTopWall': BoardItemKind.TopWall,
        'bidkLeftTopWall': BoardItemKind.LeftTopWall,
        'bidkRightTopWall': BoardItemKind.RightTopWall,
        'bidkNarrowStructure': BoardItemKind.NarrowStructure,
        'bidkTopWallWithCorners': BoardItemKind.TopWallWithCorners,
        'bidkChain': BoardItemKind.Chain,
        'bidkTile': BoardItemKind.Tile,
        'bidkPattern4x4_1': BoardItemKind.Pattern4x4_1,
        'bidkPattern4x4_2': BoardItemKind.Pattern4x4_2,
        'bidkPattern4x4_3': BoardItemKind.Pattern4x4_3,
        'bidkPattern3x3_1': BoardItemKind.Pattern3x3_1,
        'bidkPattern3x3_2': BoardItemKind.Pattern3x3_2,
        'bidkPattern3x3_3': BoardItemKind.Pattern3x3_3
    }

    rangeKind = {
        'range1': RangeKind.Range1,
        'range2': RangeKind.Range2,
        'range4': RangeKind.Range4,
        'range8': RangeKind.Range8,
        'range16': RangeKind.Range16,
        'range32': RangeKind.Range32,
        'range3': RangeKind.Range3,
        'range5': RangeKind.Range5
    }

    enemyKind = {
        'ekVertical': (EnemyKind.Vertical, False),
        'ekHorizontal': (EnemyKind.Horizontal, False),
        'ekVerticalTimeResist': (EnemyKind.Vertical, True),
        'ekHorizontalTimeResist': (EnemyKind.Horizontal, True)
    }

    laserKind = {
        'lkLeftToRight': (LaserKind.LeftToRight, False),
        'lkRightToLeft': (LaserKind.RightToLeft, False),
        'lkLeftToRightTimeResist': (LaserKind.LeftToRight, True),
        'lkRightToLeftTimeResist': (LaserKind.RightToLeft, True)
    }

    activableItemKind = {
        'aikTimePlatform': (ActivationMethodKind.Time, False, False, False),
        'aikTimeForceField': (ActivationMethodKind.Time, False, False, True),
        'aikStatePlatform': (ActivationMethodKind.State, False, False, False),
        'aikStateForceField': (ActivationMethodKind.State, False, False, True),
        'aikTimeInversePlatform': (ActivationMethodKind.Time, True, False, False),
        'aikTimeInverseForceField': (ActivationMethodKind.Time, True, False, True),
        'aikStateInversePlatform': (ActivationMethodKind.State, True, False, False),
        'aikStateInverseForceField': (ActivationMethodKind.State, True, False, True),
        'aikTimeTimeResistPlatform': (ActivationMethodKind.Time, False, True, False),
        'aikTimeTimeResistForceField': (ActivationMethodKind.Time, False, True, True),
        'aikStateTimeResistPlatform': (ActivationMethodKind.State, False, True, False),
        'aikStateTimeResistForceField': (ActivationMethodKind.State, False, True, True),
        'aikTimeInverseTimeResistPlatform': (ActivationMethodKind.Time, True, True, False),
        'aikTimeInverseTimeResistForceField': (ActivationMethodKind.Time, True, True, True),
        'aikStateInverseTimeResistPlatform': (ActivationMethodKind.State, True, True, False),
        'aikStateInverseTimeResistForceField': (ActivationMethodKind.State, True, True, True)
    }

    buttonKind = {
        'bkStateOn': (ButtonKind.TurnsOn, False),
        'bkStateOff': (ButtonKind.TurnsOff, False),
        'bkStateOnTimeResist': (ButtonKind.TurnsOn, True),
        'bkStateOffTimeResist': (ButtonKind.TurnsOff, True)
    }

    portalKind = {
        'pkLeftEntry': PortalEntryKind.LeftSide,
        'pkRightEntry': PortalEntryKind.RightSide,
        'pkBothEntries': PortalEntryKind.BothSides
    }

    def parseBoardColor(self, text: str):
        return self.boardColor[text]

    def encodeBoardColor(self, color: BoardColor):
        return [x for x in self.boardColor if self.boardColor[x] == color][0]

    def parseBoardItemKind(self, text: str):
        return self.boardItemKind[text]

    def encodeBoardItemKind(self, kind: BoardItemKind):
        return [x for x in self.boardItemKind if self.boardItemKind[x] == kind][0]

    def parseRangeKind(self, text: str):
        return self.rangeKind[text]

    def encodeRangeKind(self, kind: RangeKind):
        return [x for x in self.rangeKind if self.rangeKind[x] == kind][0]

    def parseEnemyKind(self, text: str):
        return self.enemyKind[text]

    def encodeEnemyKind(self, kind: EnemyKind, timeResist: bool):
        return [x for x in self.enemyKind if self.enemyKind[x] == (kind, timeResist)][0]

    def parseLaserKind(self, text: str):
        return self.laserKind[text]

    def encodeLaserKind(self, kind: LaserKind, timeResist: bool):
        return [x for x in self.laserKind if self.laserKind[x] == (kind, timeResist)][0]

    def parseActivableItemKind(self, text: str):
        return self.activableItemKind[text]

    def encodeActivableItemKind(self, methodKind: ActivationMethodKind, inverted: bool, timeResist: bool,
                                forceField: bool):
        return [x for x in self.activableItemKind if self.activableItemKind[x] ==
                (methodKind, inverted, timeResist, forceField)][0]

    def parseButtonKind(self, text: str):
        return self.buttonKind[text]

    def encodeButtonKind(self, kind: ButtonKind, timeResist: bool):
        return [x for x in self.buttonKind if self.buttonKind[x] == (kind, timeResist)][0]

    def parsePortalKind(self, text: str):
        return self.portalKind[text]

    def encodePortalKind(self, kind: PortalEntryKind):
        return [x for x in self.portalKind if self.portalKind[x] == kind][0]

    @staticmethod
    def parseBool(text: str):
        match text:
            case 'False':
                return False
            case 'True':
                return True
        raise Exception('parseBool error')

    @staticmethod
    def parseBinary(text: str):
        if len(text) != 9 or text[0] != '%':
            raise Exception('parseBinary error')
        return int(text[1:], 2)

    @staticmethod
    def encodeBinary(val: int):
        return f'%{val:08b}'
