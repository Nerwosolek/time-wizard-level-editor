class MinMax:
    def __init__(self, minimum: int, maximum: int):
        self.min = minimum
        self.max = maximum

    def count(self):
        return self.max - self.min + 1


class Ranges:
    levelColumns = MinMax(40, 64)
    seconds = MinMax(10, 120)
    column = MinMax(0, 63)
    row = MinMax(0, 17)
    width = MinMax(1, 64)
    height = MinMax(1, 18)
    xPos = MinMax(0, 255)
    yPos = MinMax(0, 143)
    moveOffset = MinMax(0, 255)
    mask = MinMax(0, 255)
    state = MinMax(0, 7)
