from PyQt6.QtCore import pyqtSignal, QObject

from app.actions import Actions


class SelectionManager(QObject):

    selectionChanged = pyqtSignal()

    def __init__(self, actions: Actions):
        super().__init__()
        self.actions = actions
        self.selected = None

    def clear(self):
        self.setSelected(None)

    def setSelected(self, item):
        if self.selected != item:
            self.selected = item
            copyable = not (self.selected is None) and self.selected.isCopyable()
            self.actions.cutAction.setEnabled(copyable)
            self.actions.copyAction.setEnabled(copyable)
            self.selectionChanged.emit()