import re

from data.asmImportExportHelper import AsmImportExportHelper
from data.enums import BoardItemKind, EnemyKind, ActivationMethodKind
from data.levelInfo import LevelInfo, GeneralInfo, LevelPortalInfo, BoardItemInfo, MovingPlatformInfo, EnemyInfo, \
    LaserInfo, StateInfo, ForceFieldInfo, DisappearingPlatformInfo, ButtonInfo, HourglassInfo, PortalInfo
from data.ranges import Ranges


class AsmLevelImporter:

    def __init__(self):
        self.ranges = Ranges()
        numberListPattern = r'^\s+dta\s+(([0-9A-Za-z\%_]+,\s+)*[0-9A-Za-z\%_]+)$'
        self.numberListComp = re.compile(numberListPattern)
        self.helper = AsmImportExportHelper()

    def load(self, levelInfo: LevelInfo, fileName: str):
        with open(fileName) as f:
            self.parse(levelInfo, f.read())

    def parse(self, levelInfo: LevelInfo, text: str):
        lines = text.splitlines()
        self.parseGeneral(levelInfo.general, lines)
        self.parseLevelPortals(levelInfo.levelPortalIn, levelInfo.levelPortalOut, lines)
        self.parseBoardItems(levelInfo.boardItems, lines)
        self.parseMovingPlatforms(levelInfo.movingPlatforms, lines)
        self.parseEnemies(levelInfo.enemies, lines)
        self.parseLasers(levelInfo.lasers, lines)
        self.parseStates(levelInfo.states, lines)
        self.parseActivableItems(levelInfo.forceFields, levelInfo.disappearingPlatforms, lines)
        self.parseButtons(levelInfo.buttons, lines)
        self.parseHourglasses(levelInfo.hourglasses, lines)
        self.parsePortals(levelInfo.portals, lines)
        levelInfo.fixProblems()

    def parseGeneral(self, data: GeneralInfo, lines: list):
        index = self.getIndex(lines, self.helper.generalHeader)
        items = self.parseList(lines[index + 1])
        if len(items) != 3:
            raise Exception('Invalid length of general info')

        data.columns = int(items[0])
        if self.ranges.levelColumns.min > data.columns > self.ranges.levelColumns.max:
            raise Exception('Invalid number of columns')

        data.color = self.helper.parseBoardColor(items[1])
        data.seconds = int(items[2])
        if self.ranges.seconds.min > data.seconds > self.ranges.seconds.max:
            raise Exception('Invalid number of seconds')

    def parseLevelPortals(self, portalIn: LevelPortalInfo, portalOut: LevelPortalInfo, lines: list):
        index = self.getIndex(lines, self.helper.levelPortalsHeader)
        items = self.parseList(lines[index + 1])
        if len(items) != 6:
            raise Exception('Invalid length of level portals')

        portalIn.column = int(items[0])
        portalIn.row = int(items[1])
        portalIn.height = int(items[2])
        portalOut.column = int(items[3])
        portalOut.row = int(items[4])
        portalOut.height = int(items[5])

    def parseBoardItems(self, data: list, lines: list):
        self.parseItems(lines, self.helper.boardItemsHeader,
                        lambda items: data.append(self.parseBoardItem(items)))

    def parseBoardItem(self, items: list):
        kind = self.helper.parseBoardItemKind(items[0])
        column = int(items[1])
        row = int(items[2])
        width = 1
        height = 1

        match kind:
            case BoardItemKind.Void | BoardItemKind.Air | BoardItemKind.Wall1x1:
                width = int(items[3])
                height = int(items[4])
                if len(items) != 5:
                    raise Exception('Invalid length of board item')

            case BoardItemKind.BottomWall1x2 | BoardItemKind.TopWall1x2 | BoardItemKind.VerticalWall | \
                    BoardItemKind.Ladder | BoardItemKind.Structure:
                width = 2
                height = int(items[3])
                if len(items) != 4:
                    raise Exception('Invalid length of board item')

            case BoardItemKind.LeftSpikes | BoardItemKind.RightSpikes | BoardItemKind.LeftWall | \
                    BoardItemKind.RightWall | BoardItemKind.NarrowStructure | BoardItemKind.Chain:
                height = int(items[3])
                if len(items) != 4:
                    raise Exception('Invalid length of board item')

            case BoardItemKind.HorizontalWall:
                width = int(items[3])
                height = 2
                if len(items) != 4:
                    raise Exception('Invalid length of board item')

            case BoardItemKind.LightPlatform | BoardItemKind.Platform | \
                    BoardItemKind.BottomSpikes | BoardItemKind.TopSpikes | \
                    BoardItemKind.BottomWall | BoardItemKind.TopWall | BoardItemKind.TopWallWithCorners:
                width = int(items[3])
                if len(items) != 4:
                    raise Exception('Invalid length of board item')

            case BoardItemKind.Pattern4x4_1, BoardItemKind.Pattern4x4_2, BoardItemKind.Pattern4x4_3:
                width = 4
                height = 4

            case BoardItemKind.Pattern3x3_1, BoardItemKind.Pattern3x3_2, BoardItemKind.Pattern3x3_3:
                width = 3
                height = 3

        return BoardItemInfo(kind, column, row, width, height)

    def parseMovingPlatforms(self, data: list, lines: list):
        self.parseItems(lines, self.helper.movingPlatformsHeader,
                        lambda items: data.append(self.parseMovingPlatform(items)))

    def parseMovingPlatform(self, items: list):
        if len(items) != 6:
            raise Exception('Invalid length of moving platform')

        xPos = int(items[0])
        row = int(items[1])
        width = int(items[2])
        moveRange = self.helper.parseRangeKind(items[3])
        moveOffset = int(items[4])
        timeResist = self.helper.parseBool(items[5])

        return MovingPlatformInfo(timeResist, xPos, row, width, moveRange, moveOffset)

    def parseEnemies(self, data: list, lines: list):
        self.parseItems(lines, self.helper.enemiesHeader,
                        lambda items: data.append(self.parseEnemy(items)))

    def parseEnemy(self, items: list):
        if len(items) != 5:
            raise Exception('Invalid length of enemy')

        kind, timeResist = self.helper.parseEnemyKind(items[4])
        if kind == EnemyKind.Horizontal:
            xPos = int(items[0])
            yPos = int(items[1])
        else:
            xPos = int(items[1])
            yPos = int(items[0])

        moveRange = self.helper.parseRangeKind(items[2])
        moveOffset = int(items[3])

        return EnemyInfo(kind, timeResist, xPos, yPos, moveRange, moveOffset)

    def parseLasers(self, data: list, lines: list):
        self.parseItems(lines, self.helper.lasersHeader,
                        lambda items: data.append(self.parseLaser(items)))

    def parseLaser(self, items: list):
        if len(items) != 5:
            raise Exception('Invalid length of laser')

        leftCol = int(items[0])
        rightCol = int(items[1])
        width = rightCol - leftCol + 1
        row = int(items[2])
        mask = self.helper.parseBinary(items[3])
        kind, timeResist = self.helper.parseLaserKind(items[4])

        return LaserInfo(kind, timeResist, leftCol, row, width, mask)

    def parseStates(self, data: list, lines: list):
        data.clear()
        self.parseItems(lines, self.helper.statesHeader,
                        lambda items: data.append(self.parseState(items)))

    def parseState(self, items: list):
        if len(items) != 2:
            raise Exception('Invalid length of laser')

        on = self.helper.parseBool(items[0])
        timeResist = self.helper.parseBool(items[1])

        return StateInfo(timeResist, on)

    def parseActivableItems(self, forceFields: list, disappearingPlatforms: list, lines: list):
        data = []
        self.parseItems(lines, self.helper.activableItemsHeader,
                        lambda items: data.append(self.parseActivableItem(items)))

        for item in data:
            if isinstance(item, ForceFieldInfo):
                forceFields.append(item)
            elif isinstance(item, DisappearingPlatformInfo):
                disappearingPlatforms.append(item)

    def parseActivableItem(self, items: list):
        if len(items) != 6:
            raise Exception('Invalid length of activable item')

        column = int(items[0])
        row = int(items[1])
        size = int(items[2])
        methodKind, inverted, timeResist, forceField = self.helper.parseActivableItemKind(items[5])

        if methodKind == ActivationMethodKind.Time:
            mask = self.helper.parseBinary(items[4])
            state = 0
        else:
            mask = 0
            state = int(items[4])

        if forceField:
            return ForceFieldInfo(methodKind, inverted, timeResist, column, row, size, mask, state)
        else:
            return DisappearingPlatformInfo(methodKind, inverted, timeResist, column, row, size, mask, state)

    def parseButtons(self, data: list, lines: list):
        self.parseItems(lines, self.helper.buttonsHeader,
                        lambda items: data.append(self.parseButton(items)))

    def parseButton(self, items: list):
        if len(items) != 4:
            raise Exception('Invalid length of button')

        column = int(items[0])
        row = int(items[1])
        state = int(items[2])
        kind, timeResist = self.helper.parseButtonKind(items[3])

        return ButtonInfo(kind, timeResist, column, row, state)

    def parseHourglasses(self, data: list, lines: list):
        self.parseItems(lines, self.helper.hourglassesHeader,
                        lambda items: data.append(self.parseHourglass(items)))

    def parseHourglass(self, items: list):
        if len(items) != 3:
            raise Exception('Invalid length of button')

        column = int(items[0])
        row = int(items[1])
        timeResist = self.helper.parseBool(items[2])

        return HourglassInfo(timeResist, column, row)

    def parsePortals(self, data: list, lines: list):
        self.parseItems(lines, self.helper.portalsHeader,
                        lambda items: data.append(self.parsePortal(items)))

    def parsePortal(self, items: list):
        if len(items) != 4:
            raise Exception('Invalid length of button')

        column = int(items[0])
        row = int(items[1])
        height = int(items[2])
        kind = self.helper.parsePortalKind(items[3])

        return PortalInfo(kind, column, row, height)

    @staticmethod
    def getIndex(lines, text):
        index = lines.index(text)
        if index < 0:
            raise Exception(f'{text} not found')
        return index

    def parseList(self, line):
        match = self.numberListComp.match(line)
        if match:
            return [x for x in match.group(1).split(', ')]
        else:
            raise Exception('List parse error')

    def isEnd(self, items):
        val = len(items) == 1 and items[0] == self.helper.end
        return val

    def parseItems(self, lines, text, fun):
        index = self.getIndex(lines, text)
        while True:
            index += 1
            items = self.parseList(lines[index])
            if self.isEnd(items):
                return
            fun(items)
