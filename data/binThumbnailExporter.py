import numpy as np


class BinThumbnailExporter:

    remap = [0, 1, 3, 2]

    def save(self, image, fileName: str):
        height, width = image.shape[:2]
        quadWidth = width // 4
        result = np.empty((height, quadWidth), np.uint8)
        for y in range(height):
            for x in range(quadWidth):
                result[y, x] = self.encode4pixels(image[y, (4 * x):(4 * x + 4)])
        result.tofile(fileName)

    def encode4pixels(self, pixels):
        return (self.remap[pixels[0]] * 64 + self.remap[pixels[1]] * 16 + self.remap[pixels[2]] * 4 +
                self.remap[pixels[3]])
