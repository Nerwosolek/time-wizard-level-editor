from data.asmImportExportHelper import AsmImportExportHelper
from data.enums import EnemyKind, ActivationMethodKind
from data.levelInfo import LevelInfo, GeneralInfo, LevelPortalInInfo, LevelPortalOutInfo, PixelInfo


class AsmLevelExporter:

    dta = '  dta '

    def __init__(self):
        self.helper = AsmImportExportHelper()

    def save(self, levelInfo: LevelInfo, fileName: str):
        lines = list()
        lines.extend(self.encodeGeneral(levelInfo.general))
        lines.extend(self.encodeBoardItems(levelInfo.boardItems))
        lines.extend(self.encodeLevelPortals(levelInfo.levelPortalIn, levelInfo.levelPortalOut))
        lines.extend(self.encodeMovingPlatforms(levelInfo.movingPlatforms))
        lines.extend(self.encodeEnemies(levelInfo.enemies))
        lines.extend(self.encodeLasers(levelInfo.lasers))
        lines.extend(self.encodeStates(levelInfo.states))
        lines.extend(self.encodeActivableItems(levelInfo))
        lines.extend(self.encodeButtons(levelInfo.buttons))
        lines.extend(self.encodeHourglasses(levelInfo.hourglasses))
        lines.extend(self.encodePortals(levelInfo.portals))

        with open(fileName, 'w') as f:
            for line in lines:
                f.write(f'{line}\n')

    def encodeGeneral(self, data: GeneralInfo):
        lines = list()
        lines.append(self.helper.generalHeader)
        lines.append(f'{self.dta}{data.columns}, {self.helper.encodeBoardColor(data.color)}, {data.seconds}')
        lines.append('')
        return lines

    def encodeBoardItems(self, data: list):
        lines = list()
        lines.append(self.helper.boardItemsHeader)
        for item in data:
            if item.kind.containsWidth():
                wStr = f', {item.width}'
            else:
                wStr = ''
            if item.kind.containsHeight():
                hStr = f', {item.height}'
            else:
                hStr = ''
            lines.append(f'{self.dta}{self.helper.encodeBoardItemKind(item.kind)}, {item.column}, {item.row}'
                         f'{wStr}{hStr}')
        lines.append(f'{self.dta}{self.helper.end}')
        lines.append('')
        return lines

    def encodeLevelPortals(self, portalIn: LevelPortalInInfo, portalOut: LevelPortalOutInfo):
        lines = list()
        lines.append(self.helper.levelPortalsHeader)
        lines.append(f'{self.dta}{portalIn.column}, {portalIn.row}, {portalIn.height}, {portalOut.column}, '
                     f'{portalOut.row}, {portalOut.height}')
        lines.append('')
        return lines

    def encodeMovingPlatforms(self, data: list):
        lines = list()
        lines.append(self.helper.movingPlatformsHeader)
        for item in data:
            lines.append(f'{self.dta}{item.xPos}, {item.row}, {item.width}, {self.helper.encodeRangeKind(item.range)}, '
                         f'{item.moveOffset}, {item.timeResist}')
        lines.append(f'{self.dta}{self.helper.end}')
        lines.append('')
        return lines

    def encodeEnemies(self, data: list):
        lines = list()
        lines.append(self.helper.enemiesHeader)
        for item in data:
            if item.kind == EnemyKind.Horizontal:
                rangeBegin = item.xPos
                secondCoordinate = PixelInfo.roundYPos(item.yPos)
            else:
                rangeBegin = item.yPos
                secondCoordinate = PixelInfo.roundXPos(item.xPos)
            lines.append(f'{self.dta}{rangeBegin}, {secondCoordinate}, {self.helper.encodeRangeKind(item.range)}, '
                         f'{item.moveOffset}, {self.helper.encodeEnemyKind(item.kind, item.timeResist)}')
        lines.append(f'{self.dta}{self.helper.end}')
        lines.append('')
        return lines

    def encodeLasers(self, data: list):
        lines = list()
        lines.append(self.helper.lasersHeader)
        for item in data:
            rightColumn = item.column + item.width - 1
            lines.append(f'{self.dta}{item.column}, {rightColumn}, {item.row}, {self.helper.encodeBinary(item.mask)}, '
                         f'{self.helper.encodeLaserKind(item.kind, item.timeResist)}')
        lines.append(f'{self.dta}{self.helper.end}')
        lines.append('')
        return lines

    def encodeStates(self, data: list):
        lines = list()
        lines.append(self.helper.statesHeader)
        for item in data:
            lines.append(f'{self.dta}{item.on}, {item.timeResist}')
        lines.append(f'{self.dta}{self.helper.end}')
        lines.append('')
        return lines

    def encodeActivableItems(self, levelInfo: LevelInfo):
        lines = list()
        lines.append(self.helper.activableItemsHeader)
        for item in levelInfo.forceFields:
            if item.methodKind == ActivationMethodKind.Time:
                maskOrState = self.helper.encodeBinary(item.mask)
            else:
                maskOrState = item.state
            lines.append(f'{self.dta}{item.column}, {item.row}, {item.height}, '
                         f'{not levelInfo.isForceFieldActive(item, 0, 0)}, {maskOrState}, '
                         f'{self.helper.encodeActivableItemKind(item.methodKind, item.invertedActivation, item.timeResist,True)}')
        for item in levelInfo.disappearingPlatforms:
            if item.methodKind == ActivationMethodKind.Time:
                maskOrState = self.helper.encodeBinary(item.mask)
            else:
                maskOrState = item.state
            lines.append(f'{self.dta}{item.column}, {item.row}, {item.width}, '
                         f'{not levelInfo.isDisappearingPlatformActive(item, 0, 0)}, {maskOrState}, '
                         f'{self.helper.encodeActivableItemKind(item.methodKind, item.invertedActivation, item.timeResist,False)}')
        lines.append(f'{self.dta}{self.helper.end}')
        lines.append('')
        return lines

    def encodeButtons(self, data: list):
        lines = list()
        lines.append(self.helper.buttonsHeader)
        for item in data:
            lines.append(f'{self.dta}{item.column}, {item.row}, {item.state}, '
                         f'{self.helper.encodeButtonKind(item.kind, item.timeResist)}')
        lines.append(f'{self.dta}{self.helper.end}')
        lines.append('')
        return lines

    def encodeHourglasses(self, data: list):
        lines = list()
        lines.append(self.helper.hourglassesHeader)
        for item in data:
            lines.append(f'{self.dta}{item.column}, {item.row}, {item.timeResist}')
        lines.append(f'{self.dta}{self.helper.end}')
        lines.append('')
        return lines

    def encodePortals(self, data: list):
        lines = list()
        lines.append(self.helper.portalsHeader)
        for item in data:
            lines.append(f'{self.dta}{item.column}, {item.row}, {item.height}, '
                         f'{self.helper.encodePortalKind(item.kind)}')
        lines.append(f'{self.dta}{self.helper.end}')
        lines.append('')
        return lines

