from data.enums import BoardColor, BoardItemKind, RangeKind, EnemyKind, LaserKind, ActivationMethodKind, ButtonKind, \
    PortalEntryKind


class BinImportExportHelper:
    
    boardColor = {
        BoardColor.Gray: 0,
        BoardColor.Brown: 1,
        BoardColor.Green: 2,
        BoardColor.Blue: 3,
        BoardColor.Violet: 4,
        BoardColor.DarkBlue: 5
    }

    boardItemKind = {
        BoardItemKind.Void: 0,
        BoardItemKind.Air: 1,
        BoardItemKind.Wall1x1: 2,
        BoardItemKind.BottomWall1x2: 3,
        BoardItemKind.TopWall1x2: 4,
        BoardItemKind.HorizontalWall: 5,
        BoardItemKind.VerticalWall: 6,
        BoardItemKind.LightPlatform: 7,
        BoardItemKind.Platform: 8,
        BoardItemKind.Ladder: 9,
        BoardItemKind.BottomSpikes: 10,
        BoardItemKind.TopSpikes: 11,
        BoardItemKind.LeftSpikes: 12,
        BoardItemKind.RightSpikes: 13,
        BoardItemKind.Structure: 14,
        BoardItemKind.BottomWall: 15,
        BoardItemKind.LeftWall: 16,
        BoardItemKind.RightWall: 17,
        BoardItemKind.TopWall: 18,
        BoardItemKind.LeftTopWall: 19,
        BoardItemKind.RightTopWall: 20,
        BoardItemKind.NarrowStructure: 21,
        BoardItemKind.TopWallWithCorners: 22,
        BoardItemKind.Chain: 23,
        BoardItemKind.Tile: 24,
        BoardItemKind.Pattern4x4_1: 25,
        BoardItemKind.Pattern4x4_2: 26,
        BoardItemKind.Pattern4x4_3: 27,
        BoardItemKind.Pattern3x3_1: 28,
        BoardItemKind.Pattern3x3_2: 29,
        BoardItemKind.Pattern3x3_3: 30
    }

    rangeKind = {
        RangeKind.Range1: 0,
        RangeKind.Range2: 1,
        RangeKind.Range4: 2,
        RangeKind.Range8: 3,
        RangeKind.Range16: 4,
        RangeKind.Range32: 5,
        RangeKind.Range3: 6,
        RangeKind.Range5: 7
    }

    enemyKind = {
        (EnemyKind.Vertical, False): 0,
        (EnemyKind.Horizontal, False): 1,
        (EnemyKind.Vertical, True): 2,
        (EnemyKind.Horizontal, True): 3
    }

    laserKind = {
        (LaserKind.LeftToRight, False): 0,
        (LaserKind.RightToLeft, False): 1,
        (LaserKind.LeftToRight, True): 2,
        (LaserKind.RightToLeft, True): 3
    }

    activableItemKind = {
        (ActivationMethodKind.Time, False, False, False): 0,
        (ActivationMethodKind.Time, False, False, True): 1,
        (ActivationMethodKind.State, False, False, False): 2,
        (ActivationMethodKind.State, False, False, True): 3,
        (ActivationMethodKind.Time, True, False, False): 4,
        (ActivationMethodKind.Time, True, False, True): 5,
        (ActivationMethodKind.State, True, False, False): 6,
        (ActivationMethodKind.State, True, False, True): 7,
        (ActivationMethodKind.Time, False, True, False): 8,
        (ActivationMethodKind.Time, False, True, True): 9,
        (ActivationMethodKind.State, False, True, False): 10,
        (ActivationMethodKind.State, False, True, True): 11,
        (ActivationMethodKind.Time, True, True, False): 12,
        (ActivationMethodKind.Time, True, True, True): 13,
        (ActivationMethodKind.State, True, True, False): 14,
        (ActivationMethodKind.State, True, True, True): 15
    }

    buttonKind = {
        (ButtonKind.TurnsOn, False): 0,
        (ButtonKind.TurnsOff, False): 1,
        (ButtonKind.TurnsOn, True): 2,
        (ButtonKind.TurnsOff, True): 3
    }

    portalKind = {
        PortalEntryKind.LeftSide: 0,
        PortalEntryKind.RightSide: 1,
        PortalEntryKind.BothSides: 2
    }

    def encodeBoardColor(self, color: BoardColor):
        return self.boardColor[color]

    def encodeBoardItemKind(self, kind: BoardItemKind):
        return self.boardItemKind[kind]

    def encodeRangeKind(self, kind: RangeKind):
        return self.rangeKind[kind]

    def encodeEnemyKind(self, kind: EnemyKind, timeResist: bool):
        return self.enemyKind[kind, timeResist]

    def encodeLaserKind(self, kind: LaserKind, timeResist: bool):
        return self.laserKind[kind, timeResist]

    def encodeActivableItemKind(self, methodKind: ActivationMethodKind, inverted: bool, timeResist: bool,
                                forceField: bool):
        return self.activableItemKind[methodKind, inverted, timeResist, forceField]

    def encodeButtonKind(self, kind: ButtonKind, timeResist: bool):
        return self.buttonKind[kind, timeResist]

    def encodePortalKind(self, kind: PortalEntryKind):
        return self.portalKind[kind]

    @staticmethod
    def encodeBool(value: bool):
        if value:
            return 1
        else:
            return 0