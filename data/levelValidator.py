from data.enums import ActivationMethodKind, ProblemKind
from data.levelInfo import LevelInfo, ItemInfo, StateInfo
from data.ranges import Ranges


class LevelProblem:

    def __init__(self, kind: ProblemKind, item: ItemInfo):
        self.kind = kind
        self.item = item

    def __str__(self):
        if self.item is None:
            return self.kind.value
        else:
            return f'{self.kind.value} - {self.item.getItemName()}'


class LevelValidator:

    def __init__(self, levelInfo: LevelInfo):
        self.levelInfo = levelInfo
        self.problems = []
        self.maxX = Ranges.xPos.max
        self.maxY = Ranges.yPos.max

    def validate(self):
        self.problems.clear()

        self.checkForProblemOutsideBoard(self.levelInfo.levelPortalIn)
        self.checkForProblemOutsideBoard(self.levelInfo.levelPortalOut)

        for item in self.levelInfo.boardItems:
            self.checkForProblemOutsideBoard(item)

        for item in self.levelInfo.movingPlatforms:
            self.checkForProblemOutsideBoard(item)

        for item in self.levelInfo.enemies:
            self.checkForProblemOutsideBoard(item)

        for item in self.levelInfo.lasers:
            self.checkForProblemOutsideBoard(item)

        for item in self.levelInfo.buttons:
            self.checkForProblemOutsideBoard(item)

        for item in self.levelInfo.forceFields:
            self.checkForProblemOutsideBoard(item)

        for item in self.levelInfo.disappearingPlatforms:
            self.checkForProblemOutsideBoard(item)

        for item in self.levelInfo.hourglasses:
            self.checkForProblemOutsideBoard(item)

        for item in self.levelInfo.portals:
            self.checkForProblemOutsideBoard(item)

        if len(self.levelInfo.hourglasses) == 0:
            self.problems.append(LevelProblem(ProblemKind.LackOfHourglasses, None))

        self.checkForTooManyButtons(self.levelInfo.buttons)
        self.checkForTooManyStates(self.levelInfo.states)

        if len(self.levelInfo.portals) % 2 == 1:
            self.problems.append(LevelProblem(ProblemKind.PortalWithoutPair, self.levelInfo.portals[-1]))

        self.checkForProblemOverlap(self.levelInfo.levelPortalIn)
        self.checkForProblemOverlap(self.levelInfo.levelPortalOut)

        for item in self.levelInfo.movingPlatforms:
            self.checkForProblemOverlap(item)

        for item in self.levelInfo.enemies:
            self.checkForProblemOverlap(item)

        for item in self.levelInfo.lasers:
            self.checkForProblemOverlap(item)

        for item in self.levelInfo.buttons:
            self.checkForProblemOverlap(item)

        for item in self.levelInfo.forceFields:
            self.checkForProblemOverlap(item)

        for item in self.levelInfo.disappearingPlatforms:
            self.checkForProblemOverlap(item)

        for item in self.levelInfo.hourglasses:
            self.checkForProblemOverlap(item)

        for item in self.levelInfo.portals:
            self.checkForProblemOverlap(item)

        for item in self.levelInfo.buttons:
            self.checkForProblemState(item)

        for item in self.levelInfo.forceFields:
            if item.methodKind == ActivationMethodKind.State:
                self.checkForProblemState(item)

        for item in self.levelInfo.disappearingPlatforms:
            if item.methodKind == ActivationMethodKind.State:
                self.checkForProblemState(item)

        for index, item in enumerate(self.levelInfo.states):
            self.checkForWrongTimeResist(index, item.timeResist)

    def checkForProblemOutsideBoard(self, item: ItemInfo):
        rect = item.getRect()
        if (rect.x() < 0) or (rect.right() > self.maxX) or (rect.y() < 0) or (rect.bottom() > self.maxY):
            self.problems.append(LevelProblem(ProblemKind.OutsideBoard, item))

    def checkForProblemOverlap(self, item: ItemInfo):
        rects = item.getRects()

        self.checkForProblemOverlapPair(item, rects, self.levelInfo.levelPortalIn)
        self.checkForProblemOverlapPair(item, rects, self.levelInfo.levelPortalOut)

        for another in self.levelInfo.movingPlatforms:
            self.checkForProblemOverlapPair(item, rects, another)

        for another in self.levelInfo.enemies:
            self.checkForProblemOverlapPair(item, rects, another)

        for another in self.levelInfo.lasers:
            self.checkForProblemOverlapPair(item, rects, another)

        for another in self.levelInfo.buttons:
            self.checkForProblemOverlapPair(item, rects, another)

        for another in self.levelInfo.forceFields:
            self.checkForProblemOverlapPair(item, rects, another)

        for another in self.levelInfo.disappearingPlatforms:
            self.checkForProblemOverlapPair(item, rects, another)

        for another in self.levelInfo.hourglasses:
            self.checkForProblemOverlapPair(item, rects, another)

        for another in self.levelInfo.portals:
            self.checkForProblemOverlapPair(item, rects, another)

    def checkForProblemOverlapPair(self, item: ItemInfo, rects: list, another: ItemInfo):
        if item == another:
            return

        anotherRects = another.getRects()
        for rect in rects:
            for anotherRect in anotherRects:
                if rect.intersects(anotherRect):
                    self.problems.append(LevelProblem(ProblemKind.ObjectOverlap, item))
                    return

    def checkForTooManyButtons(self, buttons: list):
        count = 0
        for button in buttons:
            if not button.timeResist:
                count += 1
                if count > 8:
                    self.problems.append(LevelProblem(ProblemKind.TooManyButtons, button))

    def checkForTooManyStates(self, states: list):
        count = 0
        for state in states:
            if not state.timeResist:
                count += 1
                if count > 8:
                    self.problems.append(LevelProblem(ProblemKind.TooManyStates, state))

    def checkForProblemState(self, item: ItemInfo):
        if item.state >= len(self.levelInfo.states):
            self.problems.append(LevelProblem(ProblemKind.StateDoesNotExist, item))

    def checkForWrongTimeResist(self, index: int, timeResist: bool):
        for button in self.levelInfo.buttons:
            if (button.state == index) and (button.timeResist != timeResist):
                self.problems.append(LevelProblem(ProblemKind.InvalidTimeResist, button))

        for forceField in self.levelInfo.forceFields:
            if ((forceField.methodKind == ActivationMethodKind.State) and (forceField.state == index) and
                    (forceField.timeResist != timeResist)):
                self.problems.append(LevelProblem(ProblemKind.InvalidTimeResist, forceField))

        for platform in self.levelInfo.disappearingPlatforms:
            if ((platform.methodKind == ActivationMethodKind.State) and (platform.state == index) and
                    (platform.timeResist != timeResist)):
                self.problems.append(LevelProblem(ProblemKind.InvalidTimeResist, platform))
