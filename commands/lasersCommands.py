from commands.baseCommands import BaseUndoChangeCommand
from commands.commandKind import CommandKind


class LaserKindChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.kind = self.newValue

    def undo(self):
        self.data.kind = self.oldValue

    def getCommandKind(self):
        return CommandKind.LaserKind


class LaserTimeResistChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.timeResist = self.newValue

    def undo(self):
        self.data.timeResist = self.oldValue

    def getCommandKind(self):
        return CommandKind.LaserTimeResist


class LaserPosChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.column = self.newValue.x()
        self.data.row = self.newValue.y()

    def undo(self):
        self.data.column = self.oldValue.x()
        self.data.row = self.oldValue.y()

    def getCommandKind(self):
        return CommandKind.LaserPosition


class LaserSizeChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.width = self.newValue.width()

    def undo(self):
        self.data.width = self.oldValue.width()

    def getCommandKind(self):
        return CommandKind.LaserSize


class LaserMaskChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.mask = self.newValue

    def undo(self):
        self.data.mask = self.oldValue

    def getCommandKind(self):
        return CommandKind.LaserMask
