from commands.baseCommands import BaseUndoChangeCommand
from commands.commandKind import CommandKind


class MovingPlatformTimeResistChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.timeResist = self.newValue

    def undo(self):
        self.data.timeResist = self.oldValue

    def getCommandKind(self):
        return CommandKind.MovingPlatformTimeResist


class MovingPlatformPosChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.xPos = self.newValue.x()
        self.data.row = self.newValue.y()

    def undo(self):
        self.data.xPos = self.oldValue.x()
        self.data.row = self.oldValue.y()

    def getCommandKind(self):
        return CommandKind.MovingPlatformPosition


class MovingPlatformSizeChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.width = self.newValue.width()

    def undo(self):
        self.data.width = self.oldValue.width()

    def getCommandKind(self):
        return CommandKind.MovingPlatformSize


class MovingPlatformRangeChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.range = self.newValue

    def undo(self):
        self.data.range = self.oldValue

    def getCommandKind(self):
        return CommandKind.MovingPlatformRange


class MovingPlatformMoveOffsetChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.moveOffset = self.newValue

    def undo(self):
        self.data.moveOffset = self.oldValue

    def getCommandKind(self):
        return CommandKind.MovingPlatformMoveOffset
