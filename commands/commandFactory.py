from commands.groupCommands import GroupTimeResistChangedCommand
from commands.itemListCommands import AddItemCommand, RemoveItemCommand, MoveItemCommand
from data.language import Language
from data.levelInfo import HourglassInfo, BoardItemInfo, MovingPlatformInfo, EnemyInfo, LaserInfo, ButtonInfo, \
    ForceFieldInfo, DisappearingPlatformInfo, PortalInfo, LevelInfo, ItemInfo
from gui.listItemsWidgets import ListItemsWidgets


class CommandFactory:

    def __init__(self, levelInfo: LevelInfo, listItemWidgets: ListItemsWidgets, lang: Language):
        self.levelInfo = levelInfo
        self.listItemWidgets = listItemWidgets
        self.lang = lang

    def getContext(self, item: ItemInfo):
        if issubclass(item.__class__, BoardItemInfo):
            return self.levelInfo.boardItems, self.listItemWidgets.boardItemsListWidget
        elif issubclass(item.__class__, MovingPlatformInfo):
            return self.levelInfo.movingPlatforms, self.listItemWidgets.movingPlatformsListWidget
        elif issubclass(item.__class__, EnemyInfo):
            return self.levelInfo.enemies, self.listItemWidgets.enemiesListWidget
        elif issubclass(item.__class__, LaserInfo):
            return self.levelInfo.lasers, self.listItemWidgets.lasersListWidget
        elif issubclass(item.__class__, ButtonInfo):
            return self.levelInfo.buttons,self.listItemWidgets.buttonsListWidget
        elif issubclass(item.__class__, ForceFieldInfo):
            return self.levelInfo.forceFields, self.listItemWidgets.forceFieldsListWidget
        elif issubclass(item.__class__, DisappearingPlatformInfo):
            return self.levelInfo.disappearingPlatforms, self.listItemWidgets.disappearingPlatformsListWidget
        elif issubclass(item.__class__, HourglassInfo):
            return self.levelInfo.hourglasses, self.listItemWidgets.hourglassesListWidget
        elif issubclass(item.__class__, PortalInfo):
            return self.levelInfo.portals, self.listItemWidgets.portalsListWidget
        else:
            return None, None

    def createAddItem(self, item: ItemInfo):
        itemList, widgetList = self.getContext(item)
        if itemList is None:
            return None
        return AddItemCommand(itemList, item, len(itemList), widgetList, self.lang)

    def createRemoveItem(self, item: ItemInfo):
        itemList, widgetList = self.getContext(item)
        if itemList is None:
            return None
        return RemoveItemCommand(itemList, item, itemList.index(item), widgetList, self.lang)

    def createMoveItemTop(self, item: ItemInfo):
        itemList, widgetList = self.getContext(item)
        if itemList is None:
            return None
        oldIndex = itemList.index(item)
        if oldIndex == 0:
            return None
        return MoveItemCommand(itemList, item, oldIndex, 0, widgetList, self.lang)

    def createMoveItemUp(self, item: ItemInfo):
        itemList, widgetList = self.getContext(item)
        if itemList is None:
            return None
        oldIndex = itemList.index(item)
        if oldIndex == 0:
            return None
        return MoveItemCommand(itemList, item, oldIndex, oldIndex - 1, widgetList, self.lang)

    def createMoveItemDown(self, item: ItemInfo):
        itemList, widgetList = self.getContext(item)
        if itemList is None:
            return None
        oldIndex = itemList.index(item)
        if oldIndex == len(itemList) - 1:
            return None
        return MoveItemCommand(itemList, item, oldIndex, oldIndex + 1, widgetList, self.lang)

    def createMoveItemBottom(self, item: ItemInfo):
        itemList, widgetList = self.getContext(item)
        if itemList is None:
            return None
        oldIndex = itemList.index(item)
        newIndex = len(itemList) - 1
        if oldIndex == newIndex:
            return None
        return MoveItemCommand(itemList, item, oldIndex, newIndex, widgetList, self.lang)

    def createGroupTimeResistChangedCommand(self, state: int, value: bool):
        items = self.levelInfo.getObjectWithDifferentTimeResistForState(state, value)
        if len(items) == 0:
            return None
        return GroupTimeResistChangedCommand(items, not value, value, self.lang)
