from commands.baseCommands import BaseUndoChangeCommand
from commands.commandKind import CommandKind


class EnemyKindChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.kind = self.newValue

    def undo(self):
        self.data.kind = self.oldValue

    def getCommandKind(self):
        return CommandKind.EnemyKind


class EnemyTimeResistChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.timeResist = self.newValue

    def undo(self):
        self.data.timeResist = self.oldValue

    def getCommandKind(self):
        return CommandKind.EnemyTimeResist


class EnemyPosChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.xPos = self.newValue.x()
        self.data.yPos = self.newValue.y()

    def undo(self):
        self.data.xPos = self.oldValue.x()
        self.data.yPos = self.oldValue.y()

    def getCommandKind(self):
        return CommandKind.EnemyPosition


class EnemyRangeChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.range = self.newValue

    def undo(self):
        self.data.range = self.oldValue

    def getCommandKind(self):
        return CommandKind.EnemyRange


class EnemyMoveOffsetChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.moveOffset = self.newValue

    def undo(self):
        self.data.moveOffset = self.oldValue

    def getCommandKind(self):
        return CommandKind.EnemyMoveOffset
