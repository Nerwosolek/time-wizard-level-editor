from commands.baseCommands import BaseUndoChangeCommand
from commands.commandKind import CommandKind


class BoardItemKindChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.kind = self.newValue

    def undo(self):
        self.data.kind = self.oldValue

    def getCommandKind(self):
        return CommandKind.BoardItemKind


class BoardItemPosChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.column = self.newValue.x()
        self.data.row = self.newValue.y()

    def undo(self):
        self.data.column = self.oldValue.x()
        self.data.row = self.oldValue.y()

    def getCommandKind(self):
        return CommandKind.BoardItemPosition


class BoardItemSizeChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.width = self.newValue.width()
        self.data.height = self.newValue.height()

    def undo(self):
        self.data.width = self.oldValue.width()
        self.data.height = self.oldValue.height()

    def getCommandKind(self):
        return CommandKind.BoardItemSize
