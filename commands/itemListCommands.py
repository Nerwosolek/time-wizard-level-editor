from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QListWidget, QListWidgetItem

from commands.baseCommands import BaseUndoCommand
from commands.commandKind import CommandKind
from data.language import Language
from data.levelInfo import ItemInfo


class AddItemCommand(BaseUndoCommand):

    def __init__(self, data: list, item: ItemInfo, index: int, widget: QListWidget, lang: Language):
        self.data = data
        self.item = item
        self.index = index
        self.widget = widget
        self.lang = lang
        super().__init__(lang)

    def redo(self):
        self.data.insert(self.index, self.item)
        listItem = QListWidgetItem()
        listItem.setText(str(self.item))
        listItem.setData(Qt.ItemDataRole.UserRole, self.item)
        self.widget.insertItem(self.index, listItem)
        self.widget.setCurrentRow(self.index)

    def undo(self):
        del self.data[self.index]
        self.widget.takeItem(self.index)

    def getCommandKind(self):
        return CommandKind.ListAddItem

    def id(self):
        return -1

    def updateLanguage(self):
        self.setText(f'{self.lang.commandKindEnum(self.getCommandKind())} {self.item.getItemName(self.lang).lower()}')


class RemoveItemCommand(BaseUndoCommand):

    def __init__(self, data: list, item: ItemInfo, index: int, widget: QListWidget, lang: Language):
        self.data = data
        self.item = item
        self.index = index
        self.widget = widget
        super().__init__(lang)

    def redo(self):
        del self.data[self.index]
        self.widget.takeItem(self.index)
        self.widget.setCurrentRow(-1)
        if self.index >= self.widget.count():
            self.widget.setCurrentRow(self.index - 1)
        else:
            self.widget.setCurrentRow(self.index)

    def undo(self):
        self.data.insert(self.index, self.item)
        listItem = QListWidgetItem()
        listItem.setText(str(self.item))
        listItem.setData(Qt.ItemDataRole.UserRole, self.item)
        self.widget.insertItem(self.index, listItem)
        self.widget.setCurrentRow(self.index)

    def getCommandKind(self):
        return CommandKind.ListRemoveItem

    def id(self):
        return -1

    def updateLanguage(self):
        self.setText(f'{self.lang.commandKindEnum(self.getCommandKind())} {self.item.getItemName(self.lang).lower()}')


class MoveItemCommand(BaseUndoCommand):

    def __init__(self, data: list, item: ItemInfo, oldIndex: int, newIndex: int, widget: QListWidget, lang: Language):
        self.data = data
        self.item = item
        self.oldIndex = oldIndex
        self.newIndex = newIndex
        self.widget = widget
        super().__init__(lang)

    def redo(self):
        del self.data[self.oldIndex]
        self.data.insert(self.newIndex, self.item)
        self.widget.takeItem(self.oldIndex)
        listItem = QListWidgetItem()
        listItem.setText(str(self.item))
        listItem.setData(Qt.ItemDataRole.UserRole, self.item)
        self.widget.insertItem(self.newIndex, listItem)
        self.widget.setCurrentRow(self.newIndex)

    def undo(self):
        del self.data[self.newIndex]
        self.data.insert(self.oldIndex, self.item)
        self.widget.takeItem(self.newIndex)
        listItem = QListWidgetItem()
        listItem.setText(str(self.item))
        listItem.setData(Qt.ItemDataRole.UserRole, self.item)
        self.widget.insertItem(self.oldIndex, listItem)
        self.widget.setCurrentRow(self.oldIndex)

    def mergeWith(self, other):
        if self.item != other.item:
            return False

        self.newIndex = other.newIndex
        if self.newIndex == self.oldIndex:
            self.setObsolete(True)
        return True

    def getCommandKind(self):
        return CommandKind.ListMoveItem
