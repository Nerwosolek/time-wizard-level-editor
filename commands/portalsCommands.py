from commands.baseCommands import BaseUndoChangeCommand
from commands.commandKind import CommandKind


class PortalKindChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.kind = self.newValue

    def undo(self):
        self.data.kind = self.oldValue

    def getCommandKind(self):
        return CommandKind.PortalKind


class PortalPosChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.column = self.newValue.x()
        self.data.row = self.newValue.y()

    def undo(self):
        self.data.column = self.oldValue.x()
        self.data.row = self.oldValue.y()

    def getCommandKind(self):
        return CommandKind.PortalPosition


class PortalSizeChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.height = self.newValue.height()

    def undo(self):
        self.data.height = self.oldValue.height()

    def getCommandKind(self):
        return CommandKind.PortalSize
