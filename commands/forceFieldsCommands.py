from commands.baseCommands import BaseUndoChangeCommand
from commands.commandKind import CommandKind


class ForceFieldActivationMethodChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.methodKind = self.newValue

    def undo(self):
        self.data.methodKind = self.oldValue

    def getCommandKind(self):
        return CommandKind.ForceFieldActivationMethod


class ForceFieldInvertedChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.invertedActivation = self.newValue

    def undo(self):
        self.data.inverted = self.oldValue

    def getCommandKind(self):
        return CommandKind.ForceFieldInverted


class ForceFieldTimeResistChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.timeResist = self.newValue

    def undo(self):
        self.data.timeResist = self.oldValue

    def getCommandKind(self):
        return CommandKind.ForceFieldTimeResist


class ForceFieldPosChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.column = self.newValue.x()
        self.data.row = self.newValue.y()

    def undo(self):
        self.data.column = self.oldValue.x()
        self.data.row = self.oldValue.y()

    def getCommandKind(self):
        return CommandKind.ForceFieldPosition


class ForceFieldSizeChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.height = self.newValue.height()

    def undo(self):
        self.data.height = self.oldValue.height()

    def getCommandKind(self):
        return CommandKind.ForceFieldSize


class ForceFieldMaskChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.mask = self.newValue

    def undo(self):
        self.data.mask = self.oldValue

    def getCommandKind(self):
        return CommandKind.ForceFieldMask


class ForceFieldStateChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.state = self.newValue

    def undo(self):
        self.data.state = self.oldValue

    def getCommandKind(self):
        return CommandKind.ForceFieldState
