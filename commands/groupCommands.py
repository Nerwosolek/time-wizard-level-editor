from commands.baseCommands import BaseUndoCommand
from commands.commandKind import CommandKind
from data.language import Language


class BaseGroupUndoChangeCommand(BaseUndoCommand):

    def __init__(self, items: list, oldValue, newValue, lang: Language):
        self.items = items
        self.oldValue = oldValue
        self.newValue = newValue
        super().__init__(lang)

    def mergeWith(self, other):
        if self.items != other.items:
            return False

        self.newValue = other.newValue

        if self.newValue == self.oldValue:
            self.setObsolete(True)
        return True


class GroupTimeResistChangedCommand(BaseGroupUndoChangeCommand):

    def redo(self):
        for item in self.items:
            item.timeResist = self.newValue

    def undo(self):
        for item in self.items:
            item.timeResist = self.oldValue

    def getCommandKind(self):
        return CommandKind.GroupTimeResist
