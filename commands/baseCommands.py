from abc import abstractmethod

from PyQt6.QtGui import QUndoCommand

from data.language import Language
from data.levelInfo import ItemInfo


class BaseUndoCommand(QUndoCommand):

    def __init__(self, lang: Language):
        super().__init__()
        self.lang = lang
        self.updateLanguage()

    def updateLanguage(self):
        self.setText(self.lang.commandKindEnum(self.getCommandKind()))

    @abstractmethod
    def getCommandKind(self):
        pass

    def id(self):
        return self.getCommandKind().value


class BaseUndoChangeCommand(BaseUndoCommand):

    def __init__(self, data: ItemInfo, oldValue, newValue, lang: Language):
        self.data = data
        self.oldValue = oldValue
        self.newValue = newValue
        super().__init__(lang)

    def mergeWith(self, other):
        if self.data != other.data:
            return False

        self.newValue = other.newValue

        if self.newValue == self.oldValue:
            self.setObsolete(True)
        return True
