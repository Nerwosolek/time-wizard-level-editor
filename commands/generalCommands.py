from commands.baseCommands import BaseUndoChangeCommand
from commands.commandKind import CommandKind


class BoardColorChangeCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.color = self.newValue

    def undo(self):
        self.data.color = self.oldValue

    def getCommandKind(self):
        return CommandKind.GeneralBoardColor


class BoardColumnsChangeCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.columns = self.newValue

    def undo(self):
        self.data.columns = self.oldValue

    def getCommandKind(self):
        return CommandKind.GeneralBoardColumns


class LevelTimeChangeCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.seconds = self.newValue

    def undo(self):
        self.data.seconds = self.oldValue

    def getCommandKind(self):
        return CommandKind.GeneralLevelTime
