from commands.baseCommands import BaseUndoChangeCommand
from commands.commandKind import CommandKind


class ButtonKindChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.kind = self.newValue

    def undo(self):
        self.data.kind = self.oldValue

    def getCommandKind(self):
        return CommandKind.ButtonKind


class ButtonTimeResistChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.timeResist = self.newValue

    def undo(self):
        self.data.timeResist = self.oldValue

    def getCommandKind(self):
        return CommandKind.ButtonTimeResist


class ButtonPosChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.column = self.newValue.x()
        self.data.row = self.newValue.y()

    def undo(self):
        self.data.column = self.oldValue.x()
        self.data.row = self.oldValue.y()

    def getCommandKind(self):
        return CommandKind.ButtonPosition


class ButtonStateChangedCommand(BaseUndoChangeCommand):

    def redo(self):
        self.data.state = self.newValue

    def undo(self):
        self.data.state = self.oldValue

    def getCommandKind(self):
        return CommandKind.ButtonState
