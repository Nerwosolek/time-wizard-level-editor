from enum import Enum


class CommandKind(int, Enum):

    GeneralBoardColor = 100
    GeneralBoardColumns = 101
    GeneralLevelTime = 102

    LevelPortalInPosition = 200
    LevelPortalInSize = 201

    LevelPortalOutPosition = 300
    LevelPortalOutSize = 301

    BoardItemKind = 400
    BoardItemPosition = 401
    BoardItemSize = 402

    MovingPlatformTimeResist = 500
    MovingPlatformPosition = 501
    MovingPlatformSize = 502
    MovingPlatformRange = 503
    MovingPlatformMoveOffset = 504

    EnemyKind = 600
    EnemyTimeResist = 601
    EnemyPosition = 602
    EnemyRange = 603
    EnemyMoveOffset = 604

    LaserKind = 700
    LaserTimeResist = 701
    LaserPosition = 702
    LaserSize = 703
    LaserMask = 704

    StateTimeResist = 800
    StateOn = 801

    ButtonKind = 900
    ButtonTimeResist = 901
    ButtonPosition = 902
    ButtonState = 903

    ForceFieldActivationMethod = 1000
    ForceFieldInverted = 1001
    ForceFieldTimeResist = 1002
    ForceFieldPosition = 1003
    ForceFieldSize = 1004
    ForceFieldMask = 1005
    ForceFieldState = 1006

    DisappearingPlatformActivationMethod = 1100
    DisappearingPlatformInverted = 1101
    DisappearingPlatformTimeResist = 1102
    DisappearingPlatformPosition = 1103
    DisappearingPlatformSize = 1104
    DisappearingPlatformMask = 1105
    DisappearingPlatformState = 1106

    HourglassTimeResist = 1200
    HourglassPosition = 1201

    PortalKind = 1300
    PortalPosition = 1301
    PortalSize = 1302

    ListAddItem = 1400
    ListRemoveItem = 1401
    ListMoveItem = 1402

    GroupTimeResist = 1500
